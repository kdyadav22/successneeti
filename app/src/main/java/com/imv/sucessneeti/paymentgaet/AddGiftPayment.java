package com.imv.sucessneeti.paymentgaet;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.imv.sucessneeti.R;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.activities.BaseActivity;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AddGiftPayment extends BaseActivity implements View.OnClickListener {


    public static final String TAG = "MyActivity : ";
    private boolean isDisableExitConfirmation = false;
    private SharedPreferences settings;
    private AppPreference mAppPreference;
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    String PREFS_NAME="auth_info";
    Intent intent;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppPreference = new AppPreference();
        intent=getIntent();
        settings = getSharedPreferences("settings", MODE_PRIVATE);

         sharedPreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

      try {

      }catch (Exception e){
         e.printStackTrace();
      }

        if (settings.getBoolean("is_prod_env", true)) {

            ((AppController) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);

            ((AppController) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);
        } else {
            ((AppController) getApplication()).setAppEnvironment(AppEnvironment.SANDBOX);

        }
        setupCitrusConfigs();

        launchPayUMoneyFlow();
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (PayUmoneyFlowManager.isUserLoggedIn(getApplicationContext())) {

        } else {

        }
    }

    @Override
    protected int a() {
        return R.layout.activity_my;
    }
    private void setupCitrusConfigs() {

        AppEnvironment appEnvironment = ((AppController) getApplication()).getAppEnvironment();
        if (appEnvironment == AppEnvironment.PRODUCTION) {
            //Toast.makeText(MyActivity.this, "Environment Set to Production", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(MyActivity.this, "Environment Set to SandBox", Toast.LENGTH_SHORT).show();
        }
    }







    @Override
    public void onClick(View v) {

    }


    private void launchPayUMoneyFlow() {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        double amount = 100;
        try {
            amount = Double.parseDouble(intent.getStringExtra("amount"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String txnId = System.currentTimeMillis() + "";
        String phone = "8888888888";
        String productName = "Hippy";
        String firstName = "Abhinav";
        String email = "ksbmabhinavshukla@gmail.com";
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = ((AppController) getApplication()).getAppEnvironment();
        builder.setAmount(String.valueOf(amount))
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();


            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);

            if (AppPreference.selectedTheme != -1) {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, AddGiftPayment.this, AppPreference.selectedTheme,mAppPreference.isOverrideResultScreen());
            } else {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, AddGiftPayment.this, R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
            }

        } catch (Exception e) {

            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }


    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
      try {


        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = ((AppController) getApplication()).getAppEnvironment();
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);
      }catch (Exception e){

      }
        return paymentParam;
    }


    @Override
    public void processAndShowResult(ResultModel resultModel, boolean b) {

    }



    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        Log.d("MyActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);
            Log.d("getTransactionDetails",transactionResponse.getTransactionDetails());
            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    Log.d("Success-","Success");
                    Toast.makeText(this, "Order Placed Successfully", Toast.LENGTH_SHORT).show();
                    try {
                        String details=transactionResponse.getPayuResponse();
                        Log.d("details",details);
                        JSONObject jsonObject=new JSONObject(transactionResponse.getPayuResponse());
                        JSONObject jsonObject1=jsonObject.getJSONObject("result");
                        String txnID=jsonObject1.getString("txnid");
                        String amount=jsonObject1.getString("amount");
                        //updateWallet(txnID,amount,intent.getStringExtra("diamond"),details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Failure Transaction
                    Log.d("failed-","failed");
                    Toast.makeText(this, "Failed Payments...", Toast.LENGTH_SHORT).show();
                }

                // Response from Payumoney
                String payuResponse = transactionResponse.getPayuResponse();
                Log.d("payuResponse-",payuResponse);

                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();
                Log.d("merchantResponse- ",merchantResponse);


            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(TAG, "Both objects are null!");
            }
        }
    }

//    private void updateWallet(String  txn_id,String amount,String diamond,String details) {
//
//        final JSONObject parameters = new JSONObject();
//        try {
//            Log.d("userid=",Variables.sharedPreferences.getString(Variables.u_id,""));
//            parameters.put("fb_id", Variables.sharedPreferences.getString(Variables.u_id,""));
//            parameters.put("pay_u_tx_id", txn_id);
//            parameters.put("price", amount);
//            parameters.put("diamond", diamond);
//            parameters.put("data", details);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Log.d("resp-home",parameters.toString());
//
//        RequestQueue rq = Volley.newRequestQueue(this);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
//                (Request.Method.POST, Variables.update_my_diamond, parameters, new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        String respo=response.toString();
//                        Log.d("responspayment",respo);
//                        finish();
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        Toast.makeText(getApplicationContext(), "Something wrong with Api", Toast.LENGTH_SHORT).show();
//                        //  Log.d("respo",error.toString());
//                    }
//                });
//        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(120000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.getCache().clear();
//        rq.add(jsonObjectRequest);
//
//    }
}
