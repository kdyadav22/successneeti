package com.imv.sucessneeti.paymentgaet;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.payumoney.sdkui.ui.activities.BaseActivity;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class MyActivity extends BaseActivity {


    //    public static final String TAG = "MyActivity : ";
    private boolean isDisableExitConfirmation = false;
    private SharedPreferences settings;
    private AppPreference mAppPreference;
//    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    String PREFS_NAME="auth_info";
//    Intent intent;
    SharedPreferences sharedPreferences;
    SharedPreferences sharedPreferencesfim;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editorfim;
    String phoneno,username,useremail,subject_id,subject_list,user_id,typedata;
    ProgressDialog progressDialog;
    private ArrayList<String> post_val = new ArrayList<String>();
    private String post_Data = "";
    WebView webView;
    final Activity activity = this;
    private String tag = "MyActivity";
    private String hash, hashSequence;
//    ProgressDialog progressDialog;

    String merchant_key = "jvEjiqhB";
    String salt = "b3sFnUEIb7";
    String action1 = "";
    String base_url = "https://secure.payu.in";//
    int error = 0;
    String hashString = "";
    Map<String, String> params;
    String txnid = "";
    String SUCCESS_URL = "https://www.payumoney.com/mobileapp/payumoney/success.php";
    String FAILED_URL = "https://www.payumoney.com/mobileapp/payumoney/failure.php";
    Handler mHandler = new Handler();
    static String getFirstName, getNumber, getEmailAddress, getRechargeAmt;
    ProgressDialog pDialog;
Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(activity);
      //  getWindow().requestFeature(Window.FEATURE_PROGRESS);
        webView = new WebView(this);
        setContentView(webView);
        intent=getIntent();
//        settings = getSharedPreferences("settings", MODE_PRIVATE);
//
         sharedPreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);

        sharedPreferencesfim = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = sharedPreferencesfim.edit();
        typedata = intent.getStringExtra("typedata");
        subject_list = intent.getStringExtra("type");
   phoneno = sharedPreferencesfim.getString("mobile",null);
   username = sharedPreferencesfim.getString("name",null);
   useremail = sharedPreferencesfim.getString("email",null);
   subject_id = intent.getStringExtra("jobid");

        getFirstName =username;//= oIntent.getExtras().getString("FIRST_NAME");
        getNumber =phoneno;//= oIntent.getExtras().getString("PHONE_NUMBER");
        getEmailAddress =useremail;//= oIntent.getExtras().getString("EMAIL_ADDRESS");
        getRechargeAmt = typedata;//Intent.getExtras().getString("RECHARGE_AMT");
        //post_val = getIntent().getStringArrayListExtra("post_val");
        //Log.d(tag, "post_val: "+post_val);
        params = new HashMap<String, String>();
        params.put("key", merchant_key);
        params.put("amount", getRechargeAmt);
        params.put("firstname", getFirstName);
        params.put("email", getEmailAddress);
        params.put("phone", getNumber);
        params.put("productinfo", "Recharge Wallet");
        params.put("surl", SUCCESS_URL);
        params.put("furl", FAILED_URL);
        params.put("service_provider", "payu_paisa");
        params.put("lastname", "");
        params.put("address1", "");
        params.put("address2", "");
        params.put("city", "");
        params.put("state", "");
        params.put("country", "");
        params.put("zipcode", "");
        params.put("udf1", "");
        params.put("udf2", "");
        params.put("udf3", "");
        params.put("udf4", "");
        params.put("udf5", "");
        params.put("pg", "");

    /*for(int i = 0;i<post_val.size();){
        params.put(post_val.get(i), post_val.get(i+1));
    i+=2;
    }*/
        if (empty(params.get("txnid"))) {
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
            params.put("txnid", txnid);
        } else
            txnid = params.get("txnid");
        //String udf2 = txnid;
        String txn = "abcd";
        hash = "";
        String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty(params.get("hash")) && params.size() > 0) {
            if (empty(params.get("key"))
                    || empty(params.get("txnid"))
                    || empty(params.get("amount"))
                    || empty(params.get("firstname"))
                    || empty(params.get("email"))
                    || empty(params.get("phone"))
                    || empty(params.get("productinfo"))
                    || empty(params.get("surl"))
                    || empty(params.get("furl"))
                    || empty(params.get("service_provider"))

            ) {
                error = 1;
            } else {
                String[] hashVarSeq = hashSequence.split("\\|");

                for (String part : hashVarSeq) {
                    hashString = (empty(params.get(part))) ? hashString.concat("") : hashString.concat(params.get(part));
                    hashString = hashString.concat("|");
                }
                hashString = hashString.concat(salt);


                hash = hashCal("SHA-512", hashString);
                action1 = base_url.concat("/_payment");
            }
        } else if (!empty(params.get("hash"))) {
            hash = params.get("hash");
            action1 = base_url.concat("/_payment");
        }

        webView.setWebViewClient(new MyWebViewClient() {

            public void onPageFinished(WebView view, final String url) {
                progressDialog.dismiss();
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //make sure dialog is showing
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }

        });


        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setCacheMode(2);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearHistory();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setLoadWithOverviewMode(false);

        //webView.addJavascriptInterface(new PayUJavaScriptInterface(getApplicationContext()), "PayUMoney");
        webView.addJavascriptInterface(new PayUJavaScriptInterface(), "PayUMoney");
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("key", merchant_key);
        mapParams.put("hash", MyActivity.this.hash);
        mapParams.put("txnid", (empty(MyActivity.this.params.get("txnid"))) ? "" : MyActivity.this.params.get("txnid"));
        Log.d(tag, "txnid: " + MyActivity.this.params.get("txnid"));
        mapParams.put("service_provider", "payu_paisa");

        mapParams.put("amount", (empty(MyActivity.this.params.get("amount"))) ? "" : MyActivity.this.params.get("amount"));
        mapParams.put("firstname", (empty(MyActivity.this.params.get("firstname"))) ? "" : MyActivity.this.params.get("firstname"));
        mapParams.put("email", (empty(MyActivity.this.params.get("email"))) ? "" : MyActivity.this.params.get("email"));
        mapParams.put("phone", (empty(MyActivity.this.params.get("phone"))) ? "" : MyActivity.this.params.get("phone"));

        mapParams.put("productinfo", (empty(MyActivity.this.params.get("productinfo"))) ? "" : MyActivity.this.params.get("productinfo"));
        mapParams.put("surl", (empty(MyActivity.this.params.get("surl"))) ? "" : MyActivity.this.params.get("surl"));
        mapParams.put("furl", (empty(MyActivity.this.params.get("furl"))) ? "" : MyActivity.this.params.get("furl"));
        mapParams.put("lastname", (empty(MyActivity.this.params.get("lastname"))) ? "" : MyActivity.this.params.get("lastname"));

        mapParams.put("address1", (empty(MyActivity.this.params.get("address1"))) ? "" : MyActivity.this.params.get("address1"));
        mapParams.put("address2", (empty(MyActivity.this.params.get("address2"))) ? "" : MyActivity.this.params.get("address2"));
        mapParams.put("city", (empty(MyActivity.this.params.get("city"))) ? "" : MyActivity.this.params.get("city"));
        mapParams.put("state", (empty(MyActivity.this.params.get("state"))) ? "" : MyActivity.this.params.get("state"));

        mapParams.put("country", (empty(MyActivity.this.params.get("country"))) ? "" : MyActivity.this.params.get("country"));
        mapParams.put("zipcode", (empty(MyActivity.this.params.get("zipcode"))) ? "" : MyActivity.this.params.get("zipcode"));
        mapParams.put("udf1", (empty(MyActivity.this.params.get("udf1"))) ? "" : MyActivity.this.params.get("udf1"));
        mapParams.put("udf2", (empty(MyActivity.this.params.get("udf2"))) ? "" : MyActivity.this.params.get("udf2"));

        mapParams.put("udf3", (empty(MyActivity.this.params.get("udf3"))) ? "" : MyActivity.this.params.get("udf3"));
        mapParams.put("udf4", (empty(MyActivity.this.params.get("udf4"))) ? "" : MyActivity.this.params.get("udf4"));
        mapParams.put("udf5", (empty(MyActivity.this.params.get("udf5"))) ? "" : MyActivity.this.params.get("udf5"));
        mapParams.put("pg", (empty(MyActivity.this.params.get("pg"))) ? "" : MyActivity.this.params.get("pg"));
        webview_ClientPost(webView, action1, mapParams.entrySet());

    }

    @Override
    protected int a() {
        return R.layout.activity_review_order;
    }

    @Override
    public void processAndShowResult(ResultModel resultModel, boolean b) {

    }

    private final class PayUJavaScriptInterface {

        PayUJavaScriptInterface() {
        }

        /**
         * This is not called on the UI thread. Post a runnable to invoke
         * loadUrl on the UI thread.
         */
        @JavascriptInterface
        public void success(long id, final String paymentId) {
            mHandler.post(new Runnable() {
                public void run() {
                    mHandler = null;
                  /*Intent intent = new Intent();
                 intent.putExtra(Constants.RESULT, "success");
                 intent.putExtra(Constants.PAYMENT_ID, paymentId);
                 setResult(RESULT_OK, intent);
                 finish();*/
                    // new PostRechargeData().execute();
                    Intent intent = new Intent(MyActivity.this, MainActivity.class);
                    intent.putExtra("test", getFirstName);
                    startActivity(intent);

                    Toast.makeText(getApplicationContext(), "Successfully payment", Toast.LENGTH_LONG).show();
                    try {
                        updateWallet(subject_id,paymentId, "sucess");
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }
            });
        }

        @JavascriptInterface
        public void failure(final String id, String error) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
//                    try {
//                        updateWallet(subject_id,id, "error");
//                    } catch (JSONException e) {
//
//                        e.printStackTrace();
//                    }
                    //cancelPayment();
                    Toast.makeText(getApplicationContext(), "Cancel payment", Toast.LENGTH_LONG).show();
                }
            });
        }

        @JavascriptInterface
        public void failure() {
            failure("");
        }

        @JavascriptInterface
        public void failure(final String params) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                  /*Intent intent = new Intent();
                 intent.putExtra(Constants.RESULT, params);
                 setResult(RESULT_CANCELED, intent);
                 finish();*/
                    try {
                        updateWallet(subject_id,params, "failed");
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(), "Failed payment", Toast.LENGTH_LONG).show();
                }
            });
        }

    }


    public void webview_ClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();

        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d(tag, "webview_ClientPost called");

        //setup and load the progress bar
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading. Please wait...");
        webView.loadData(sb.toString(), "text/html", "utf-8");
    }


    public void success(long id, final String paymentId) {

        mHandler.post(new Runnable() {
            public void run() {
                mHandler = null;
                //  new PostRechargeData().execute();

                Toast.makeText(getApplicationContext(), "Successfully payment\n redirect from Success Function", Toast.LENGTH_LONG).show();

            }
        });
    }


    public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }

    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();


            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) hexString.append("0");
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();


    }

    //String SUCCESS_URL = "https://pay.in/sccussful" ; // failed
//String FAILED_URL = "https://pay.in/failed" ;
//override the override loading method for the webview client
    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

     /*if(url.contains("response.php") || url.equalsIgnoreCase(SUCCESS_URL)){

      new PostRechargeData().execute();

      Toast.makeText(getApplicationContext(),"Successfully payment\n redirect from webview" ,Toast.LENGTH_LONG).show();

            return false;
     }else  */
            if (url.startsWith("http")) {
                //Toast.makeText(getApplicationContext(),url ,Toast.LENGTH_LONG).show();
                progressDialog.show();
                view.loadUrl(url);
                System.out.println("myresult " + url);
                //return true;
            } else {
                return false;
            }

            return true;
        }
    }


//        mAppPreference = new AppPreference();
//        intent=getIntent();
//        settings = getSharedPreferences("settings", MODE_PRIVATE);
//
//         sharedPreferences=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
//
//        sharedPreferencesfim = getSharedPreferences("TEMP", MODE_PRIVATE);
//        editor = sharedPreferencesfim.edit();
//        typedata = intent.getStringExtra("typedata");
//        subject_list = intent.getStringExtra("type");
//   phoneno = sharedPreferencesfim.getString("mobile",null);
//   username = sharedPreferencesfim.getString("name",null);
//   useremail = sharedPreferencesfim.getString("email",null);
//   subject_id = intent.getStringExtra("jobid");
////        subject_id = sharedPreferencesfim.getString("subject_id",null);
////        subject_list = sharedPreferencesfim.getString("subject_list",null);
////        user_id = sharedPreferencesfim.getString("user_id",null);
//      try {
//
//      }catch (Exception e){
//         e.printStackTrace();
//      }
//
//        if (settings.getBoolean("is_prod_env", true)) {
//
//            ((MyApplication) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);
//
//            ((MyApplication) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);
//        } else {
//            ((MyApplication) getApplication()).setAppEnvironment(AppEnvironment.SANDBOX);
//
//        }
//        setupCitrusConfigs();
//
//        launchPayUMoneyFlow();
//    }
//
//    public static String hashCal(String str) {
//        byte[] hashseq = str.getBytes();
//        StringBuilder hexString = new StringBuilder();
//        try {
//            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
//            algorithm.reset();
//            algorithm.update(hashseq);
//            byte messageDigest[] = algorithm.digest();
//            for (byte aMessageDigest : messageDigest) {
//                String hex = Integer.toHexString(0xFF & aMessageDigest);
//                if (hex.length() == 1) {
//                    hexString.append("0");
//                }
//                hexString.append(hex);
//            }
//        } catch (NoSuchAlgorithmException ignored) {
//        }
//        return hexString.toString();
//    }
//
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if (PayUmoneyFlowManager.isUserLoggedIn(getApplicationContext())) {
//finish();
//        } else {
//finish();
//        }
//    }
//
//    @Override
//    protected int a() {
//        return R.layout.activity_my;
//    }
//    private void setupCitrusConfigs() {
//
//        AppEnvironment appEnvironment = ((MyApplication) getApplication()).getAppEnvironment();
//        if (appEnvironment == AppEnvironment.PRODUCTION) {
//            //Toast.makeText(MyActivity.this, "Environment Set to Production", Toast.LENGTH_SHORT).show();
//        } else {
//            //Toast.makeText(MyActivity.this, "Environment Set to SandBox", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//
//
//
//
//
//
//    @Override
//    public void onClick(View v) {
//
//    }
//
//
//    private void launchPayUMoneyFlow() {
//
//        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
//
//        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);
//
//        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
//
//        double amount = 1;
//        try {
//            amount = Double.parseDouble(intent.getStringExtra("typedata"));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        String txnId = System.currentTimeMillis() + "";
//        String phone = phoneno;
//        String productName = "Sucess Neeti";
//        String firstName = username;
//        String email = useremail;
//        String udf1 = "";
//        String udf2 = "";
//        String udf3 = "";
//        String udf4 = "";
//        String udf5 = "";
//        String udf6 = "";
//        String udf7 = "";
//        String udf8 = "";
//        String udf9 = "";
//        String udf10 = "";
//
//        AppEnvironment appEnvironment = ((MyApplication) getApplication()).getAppEnvironment();
//        builder.setAmount(String.valueOf(amount))
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl(appEnvironment.surl())
//                .setfUrl(appEnvironment.furl())
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setUdf6(udf6)
//                .setUdf7(udf7)
//                .setUdf8(udf8)
//                .setUdf9(udf9)
//                .setUdf10(udf10)
//                .setIsDebug(appEnvironment.debug())
//                .setKey(appEnvironment.merchant_Key())
//                .setMerchantId(appEnvironment.merchant_ID());
//
//        try {
//            mPaymentParams = builder.build();
//
//
//            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
//
//            if (AppPreference.selectedTheme != -1) {
//                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MyActivity.this, AppPreference.selectedTheme,mAppPreference.isOverrideResultScreen());
//            } else {
//                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MyActivity.this, R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
//            }
//
//        } catch (Exception e) {
//
//            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
//
//        }
//    }
//
//
//    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
//      try {
//
//
//        StringBuilder stringBuilder = new StringBuilder();
//        HashMap<String, String> params = paymentParam.getParams();
//        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
//
//        AppEnvironment appEnvironment = ((MyApplication) getApplication()).getAppEnvironment();
//        stringBuilder.append(appEnvironment.salt());
//
//        String hash = hashCal(stringBuilder.toString());
//        paymentParam.setMerchantHash(hash);
//      }catch (Exception e){
//
//      }
//        return paymentParam;
//    }
//
//
//    @Override
//    public void processAndShowResult(ResultModel resultModel, boolean b) {
//
//    }
//
//
//
//    public class DecimalDigitsInputFilter implements InputFilter {
//
//        Pattern mPattern;
//
//        public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
//            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
//        }
//
//        @Override
//        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//
//            Matcher matcher = mPattern.matcher(dest);
//            if (!matcher.matches())
//                return "";
//            return null;
//        }
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result Code is -1 send from Payumoney activity
//        Log.d("MyActivity", "request code " + requestCode + " resultcode " + resultCode);
//
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
//                null) {
//            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                    .INTENT_EXTRA_TRANSACTION_RESPONSE);
//            Log.d("getTransactionDetails",transactionResponse.getTransactionDetails());
//            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
//
//            // Check which object is non-null
//            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                    //Success Transaction
//                    Log.d("Success-","Success");
//                    Toast.makeText(this, "Package Buy Successfully", Toast.LENGTH_SHORT).show();
////                    try {
////                        String details=transactionResponse.getPayuResponse();
////                        Log.d("details",details);
////                        JSONObject jsonObject=new JSONObject(transactionResponse.getPayuResponse());
////                        JSONObject jsonObject1=jsonObject.getJSONObject("result");
////                        String txnID=jsonObject1.getString("txnid");
////                        String amount=jsonObject1.getString("amount");
//                    try {
//                        updateWallet(transactionResponse.getTransactionDetails(), intent.getStringExtra("typedata"), "true");
//                    } catch (JSONException e) {
//
//                        e.printStackTrace();
//                    }
////
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
//                } else {  try {
//                    updateWallet(transactionResponse.getTransactionDetails(), intent.getStringExtra("typedata"), "false");
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//                }
//
//
//                }
//                try {
//                    updateWallet(transactionResponse.getTransactionDetails(), intent.getStringExtra("typedata"), "false");
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//                }
//                //Failure Transaction
//                    Log.d("failed-","failed");
//                    Toast.makeText(this, "Failed Payments...", Toast.LENGTH_SHORT).show();
//                }
//
//                // Response from Payumoney
//                String payuResponse = transactionResponse.getPayuResponse();
//                Log.d("payuResponse-",payuResponse);
//
//                // Response from SURl and FURL
//                String merchantResponse = transactionResponse.getTransactionDetails();
//                Log.d("merchantResponse- ",merchantResponse);
//
//
//               } else {
//                Log.d(TAG, "Both objects are null!");
//            }
//        }


    private void updateWallet(String txnID, String amount, String details) throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url ="http://new.successneeti.com/api/user/signup";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        JSONObject obj=new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
String iddd = sharedPreferencesfim.getString("id",null);
            JSONObject obj1=new JSONObject();
            obj1.put("user_id",iddd);
            obj1.put("job_id",subject_id);
            obj1.put("type",subject_list);
            obj1.put("fee",typedata);
            obj1.put("status",details);
            obj1.put("txn_id",txnID);
            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/jobs/updatepayment",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {

                        if(subject_list.matches("1")) {
                            editor.putString("subscription", "sucess");
                            editor.apply();
                        }else{
//                            editor.putString("subscription", "sucess");
//                            editor.apply();

                        }

                        Intent intent = new Intent(MyActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);


                    }

                    else{
                        Toast.makeText(MyActivity.this, response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(MyActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferencesm = getSharedPreferences("TEMP", MODE_PRIVATE);
                String apiuyt = sharedPreferencesm.getString("deviceid",null);

                headers.put("Token", apiuyt);
                return headers;
            }
            /*@Override
            public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }

            }*/
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }


        };

        requestQueue.add(jsonObjectRequest);
    }




//    private void updateWallet(final String txn_id, final String amount, final String details) {
//        progressDialog = new ProgressDialog(MyActivity.this);
//        progressDialog.setMessage("Loading..."); // Setting Message
//        progressDialog.setTitle("Please wait..."); // Setting Title
//        progressDialog.show(); // Display Progress Dialog
//        progressDialog.setCancelable(false);
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        final StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.PAYMENTDATASEND, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i(TAG, "onResponse: "+response);
//                JSONObject jsonObject = null;
//
//                try{
//                    progressDialog.dismiss();
//                     jsonObject = new JSONObject(response);
//                    String status = jsonObject.getString("status");
//                    if (status.matches("1")){
//                        Intent intent = new Intent(MyActivity.this, MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        Toast.makeText(MyActivity.this, "Thankyou", Toast.LENGTH_SHORT).show();
//
//                    }
//                    else{
//                        Toast.makeText(MyActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
//                    }
//
////                    Intent intent = new Intent(Mobileno.this, OtpVerify.class);
////                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                    intent.putExtra("mobile_no",mobileno.getText().toString());
////                    intent.putExtra("otp_first",otp);
////                    intent.putExtra("status",success);
////                    editor.putString("status",jsonObject.getString("status"));
////
////                    editor.putString("mobileno_v",mobileno.getText().toString().trim());
////
////                    editor.apply();
////
////                    startActivity(intent);
//
//                 }catch (Exception e){
//                    progressDialog.dismiss();
//                    e.printStackTrace();
//                }
//
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Log.d(TAG, "onErrorResponse: "+error);
//                Toast.makeText(MyActivity.this, ""+error, Toast.LENGTH_SHORT).show();
//            }
//        }){
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                return super.getHeaders();
//            }
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> param = new HashMap<>();
//
//                param.put("p_id",txn_id);
//                param.put("amount",amount);
//                param.put("data",details);
//                param.put("subject_id",subject_id);
//                param.put("course_id",subject_list);
//                param.put("type",typedata);
//                param.put("user_id",user_id);
//                Log.d(TAG, "getParams: "+param);
//                return param;
//            }
//
//
//        };
//        requestQueue.add(stringRequest);
//    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

