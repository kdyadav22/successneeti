package com.imv.sucessneeti.Model;

public class EducationModel {
String education_name;
String stream_name;
String board_name;
String sujbjectmark;
String obtainmar;
String perctengae_name;
String yearofpassding;
String id;
String educationid;
String streamid;

    public EducationModel(String id,String educationid,String education_name,String streamid ,String stream_name, String board_name,String sujbjectmark,String obtainmar, String perctengae_name, String yearofpassding) {
        this.id = id;
        this.educationid = educationid;
        this.education_name = education_name;
        this.streamid = streamid;
        this.stream_name = stream_name;
        this.board_name = board_name;
        this.sujbjectmark = sujbjectmark;
        this.obtainmar = obtainmar;
        this.perctengae_name = perctengae_name;
        this.yearofpassding = yearofpassding;
    }

    public String getEducation_name() {
        return education_name;
    }

    public void setEducation_name(String education_name) {
        this.education_name = education_name;
    }

    public String getStream_name() {
        return stream_name;
    }

    public void setStream_name(String stream_name) {
        this.stream_name = stream_name;
    }

    public String getBoard_name() {
        return board_name;
    }

    public void setBoard_name(String board_name) {
        this.board_name = board_name;
    }

    public String getPerctengae_name() {
        return perctengae_name;
    }

    public void setPerctengae_name(String perctengae_name) {
        this.perctengae_name = perctengae_name;
    }

    public String getYearofpassding() {
        return yearofpassding;
    }

    public void setYearofpassding(String yearofpassding) {
        this.yearofpassding = yearofpassding;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSujbjectmark() {
        return sujbjectmark;
    }

    public void setSujbjectmark(String sujbjectmark) {
        this.sujbjectmark = sujbjectmark;
    }

    public String getObtainmar() {
        return obtainmar;
    }

    public void setObtainmar(String obtainmar) {
        this.obtainmar = obtainmar;
    }

    public String getEducationid() {
        return educationid;
    }

    public void setEducationid(String educationid) {
        this.educationid = educationid;
    }

    public String getStreamid() {
        return streamid;
    }

    public void setStreamid(String streamid) {
        this.streamid = streamid;
    }
}
