package com.imv.sucessneeti.Model;

public class AppliedjobMode {
    String id;
    String title;
    String job_state;
    String last_date;
    String fee;
    String admit_card;
    String result;
    String fee_rec;
    String reg_rec;

    public AppliedjobMode(String id, String title, String job_state, String last_date, String fee, String admit_card, String result, String fee_rec, String reg_rec) {
        this.id = id;
        this.title = title;
        this.job_state = job_state;
        this.last_date = last_date;
        this.fee = fee;
        this.admit_card = admit_card;
        this.result = result;
        this.fee_rec = fee_rec;
        this.reg_rec = reg_rec;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJob_state() {
        return job_state;
    }

    public void setJob_state(String job_state) {
        this.job_state = job_state;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getAdmit_card() {
        return admit_card;
    }

    public void setAdmit_card(String admit_card) {
        this.admit_card = admit_card;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFee_rec() {
        return fee_rec;
    }

    public void setFee_rec(String fee_rec) {
        this.fee_rec = fee_rec;
    }

    public String getReg_rec() {
        return reg_rec;
    }

    public void setReg_rec(String reg_rec) {
        this.reg_rec = reg_rec;
    }
}
