package com.imv.sucessneeti.Model;

public class MainPhoto {
    String name;
    String id;
    String images;

    public MainPhoto(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
