package com.imv.sucessneeti.Model;

public class DocumentList {
    String id;
    String title;
    String date;
    String  namei;

    public DocumentList(String id, String title, String date, String namei) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.namei = namei;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNamei() {
        return namei;
    }

    public void setNamei(String namei) {
        this.namei = namei;
    }
}
