package com.imv.sucessneeti.Model;

public class ExperienceModel {
    String id;
    String companyname;
    String jobtitle;
    String industry;
    String firstday;
    String lasday;
    String industry_id;
    String function_id;
    String salary;
    String experience_year;
    String experience_month;

    public ExperienceModel(String id,String industry_id,String function_id, String companyname, String jobtitle, String industry, String firstday,String lasday,String experience_year,String  experience_month,String salary) {
        this.id = id;
        this.industry_id = industry_id;
        this.function_id = function_id;
        this.companyname = companyname;
        this.jobtitle = jobtitle;
        this.industry = industry;
       this.firstday = firstday;
       this.lasday = lasday;
       this.experience_year = experience_year;
       this.experience_month = experience_month;
       this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getFirstday() {
        return firstday;
    }

    public void setFirstday(String firstday) {
        this.firstday = firstday;
    }

    public String getLasday() {
        return lasday;
    }

    public void setLasday(String lasday) {
        this.lasday = lasday;
    }

    public String getIndustry_id() {
        return industry_id;
    }

    public void setIndustry_id(String industry_id) {
        this.industry_id = industry_id;
    }

    public String getFunction_id() {
        return function_id;
    }

    public void setFunction_id(String function_id) {
        this.function_id = function_id;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getExperience_year() {
        return experience_year;
    }

    public void setExperience_year(String experience_year) {
        this.experience_year = experience_year;
    }

    public String getExperience_month() {
        return experience_month;
    }

    public void setExperience_month(String experience_month) {
        this.experience_month = experience_month;
    }
}
