package com.imv.sucessneeti;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.imv.sucessneeti.signin.SignIn;
import com.imv.sucessneeti.fragment.Applied_Jobs;
import com.imv.sucessneeti.fragment.BlankFragment;
import com.imv.sucessneeti.fragment.Document_List;
import com.imv.sucessneeti.fragment.Education_List;
import com.imv.sucessneeti.fragment.Experience;
import com.imv.sucessneeti.fragment.Expired_jobs;
import com.imv.sucessneeti.fragment.HomeFragment;
import com.imv.sucessneeti.fragment.ParentSetails;
import com.imv.sucessneeti.fragment.Personalinf;
import com.imv.sucessneeti.fragment.PhotoFrag;
import com.imv.sucessneeti.fragment.QueryFrag;
import com.imv.sucessneeti.fragment.Recommended_jobs;
import com.squareup.picasso.Picasso;

import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    NavigationView navigationView;
    Toolbar toolbar;
    LinearLayout adLayout;
    ImageView ivHome, ivfav, ivYoutube, ivPress, editprofile;
    TextView textViewemail, tv_youtube, tvPremium, tvHome, tvName, tvsecond, tvMobile;
    SharedPreferences sharedPreferences;
    LinearLayout header;
    ImageView imageView;
    public boolean doubleBackToExitPressedOnce = false;
    String maes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.statusbar_gradient);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
//        if (Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners").contains(getApplicationContext().getPackageName()))
//        {
//            //service is enabled do something
//        } else {
//            //service is not enabled try to enabled by calling...
//            getApplicationContext().startActivity(new Intent(
//                    "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));


//        }

        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 3);
        Intent intent = getIntent();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        navigationView = findViewById(R.id.navigation_view);

        // header = findViewById(R.id.header);
        View headerView = LayoutInflater.from(this).inflate(R.layout.nav_header_main, navigationView, false);
        navigationView.addHeaderView(headerView);
        drawerLayout = findViewById(R.id.drawer_layout);
        adLayout = findViewById(R.id.adview);
        //Button button = (Button) view.findViewById(R.id.getStarted);
//LinearLayout layout= (LinearLayout) findViewById(R.id.lauiuhi);
        textViewemail = headerView.findViewById(R.id.textViewemail);
        imageView = headerView.findViewById(R.id.imageView);
        textViewemail.setText(sharedPreferences.getString("name", null));
        String imagepath = sharedPreferences.getString("imagepath", null);
        if (imagepath != null) {
            if (imagepath.isEmpty()) {

            } else {
                Picasso.get()
                        .load(imagepath)
                        .placeholder(R.drawable.upload_icon)
                        .into(imageView);
            }
        }
        final HomeFragment homeFragment = new HomeFragment();
        fragmentManager.beginTransaction().replace(R.id.Container, homeFragment).commit();
        if (intent != null) {
            maes = intent.getStringExtra("valueb");
            if (maes != null) {
                if (maes.equals("1")) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("meme", "1");
                    editor.apply();
                    PhotoFrag photoFragm = new PhotoFrag();
                    loadFrag(photoFragm, getString(R.string.photoo), fragmentManager);
                } else if (maes.equals("2")) {
                    Personalinf personalinfm = new Personalinf();
                    loadFrag(personalinfm, getString(R.string.personal_in), fragmentManager);
                } else if (maes.equals("3")) {
                    ParentSetails parentSetailsm = new ParentSetails();
                    loadFrag(parentSetailsm, getString(R.string.parentinfo), fragmentManager);
                } else if (maes.equals("4")) {
                    Education_List education_listm = new Education_List();
                    loadFrag(education_listm, getString(R.string.edulist), fragmentManager);
                } else if (maes.equals("5")) {
                    Experience experiencem = new Experience();
                    loadFrag(experiencem, getString(R.string.experience), fragmentManager);
                } else if (maes.equals("6")) {
                    Document_List document_listm = new Document_List();
                    loadFrag(document_listm, getString(R.string.docum_list), fragmentManager);
                }
            }
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.nav_homeey:
                        HomeFragment homeFragmentw = new HomeFragment();
                        loadFrag(homeFragmentw, getString(R.string.menu_home), fragmentManager);
                        return true;
                    case R.id.nav_changepass:
                        BlankFragment homeFragment = new BlankFragment();
                        loadFrag(homeFragment, getString(R.string.changepass), fragmentManager);
                        return true;
                    case R.id.nav_photo:
                        PhotoFrag photofr = new PhotoFrag();
                        loadFrag(photofr, getString(R.string.photoo), fragmentManager);
                        return true;
                    case R.id.nav_personal_info:
                        Personalinf nnnn = new Personalinf();
                        loadFrag(nnnn, getString(R.string.personal_in), fragmentManager);
                        return true;

                    case R.id.nav_parent_info:
                        ParentSetails yyyy = new ParentSetails();
                        loadFrag(yyyy, getString(R.string.parentinfo), fragmentManager);
                        return true;

                    case R.id.nav_education:
                        Education_List ttttt = new Education_List();
                        loadFrag(ttttt, getString(R.string.edulist), fragmentManager);
                        return true;

                    case R.id.nav_experience:
                        Experience vvvvv = new Experience();
                        loadFrag(vvvvv, getString(R.string.experience), fragmentManager);
                        return true;
                    case R.id.nav_documnet:
                        Document_List rtyy = new Document_List();
                        loadFrag(rtyy, getString(R.string.docum_list), fragmentManager);
                        return true;

                    case R.id.nav_rec_jobs:
                        Recommended_jobs recommended_jobs = new Recommended_jobs();
                        loadFrag(recommended_jobs, getString(R.string.rec_job), fragmentManager);
                        return true;

                    case R.id.nav_app_jobs:
                        Applied_Jobs applied_jobs = new Applied_Jobs();
                        loadFrag(applied_jobs, getString(R.string.myappllied), fragmentManager);
                        return true;
                    case R.id.nav_exp_jobs:
                        Expired_jobs expired_jobs = new Expired_jobs();
                        loadFrag(expired_jobs, getString(R.string.exp_job), fragmentManager);
                        return true;
                    case R.id.nav_queyy:
                        QueryFrag queryFrag = new QueryFrag();
                        loadFrag(queryFrag, getString(R.string.query), fragmentManager);
                        return true;

                    case R.id.nav_logout:
                        SharedPreferences preferences = getSharedPreferences("TEMP", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        Toast.makeText(MainActivity.this, "Successfully logout", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MainActivity.this, SignIn.class);
                        intent.putExtra("finish", true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                        startActivity(intent);
                        finish();

                    default:
                        return true;

                }

            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
            }

        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }


    private void geotowhapp() {

        boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
        Uri uri;
        if (isWhatsappInstalled) {

//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO,   uri = Uri.parse("smsto:" + "9560393886"));
//            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hai Good Morning");
//            sendIntent.setType("text/plain");
//            sendIntent.setPackage("com.whatsapp");
//            startActivity(sendIntent);
            PackageManager packageManager = this.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);

            try {
                String url = "https://api.whatsapp.com/send?phone=" + "+919518818647" + "&text=" + URLEncoder.encode("Welcome to Exam Router ", "UTF-8");
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                if (i.resolveActivity(packageManager) != null) {
                    this.startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "WhatsApp not Installed",
                    Toast.LENGTH_SHORT).show();
            uri = Uri.parse("market://details?id=com.whatsapp");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(goToMarket);

        }
    }

    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }


    public void highLightNavigation(int position) {

        navigationView.getMenu().getItem(position).setChecked(true);
        toolbar.setTitle(getString(R.string.app_name));
    }

    public void loadFrag(Fragment f1, String name, FragmentManager fm) {
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        FragmentTransaction ft = fm.beginTransaction();
        //  ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.Container, f1, name);
        ft.commit();
        setToolbarTitle(name);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (fragmentManager.getBackStackEntryCount() != 0) {
            String tag = fragmentManager.getFragments().get(fragmentManager.getBackStackEntryCount() - 1).getTag();
            setToolbarTitle(tag);
            super.onBackPressed();
        } else {

            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }
            doubleBackToExitPressedOnce = true;

            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 1500);

        }

    }

    public void setToolbarTitle(String Title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(Title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.activity_main_drawer, menu);


//        final MenuItem searchMenuItem = menu.findItem(R.id.search);
//        final SearchView searchView = (SearchView) searchMenuItem.getActionView();
//
//        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                // TODO Auto-generated method stub
//                if (!hasFocus) {
//                    searchMenuItem.collapseActionView();
//                    searchView.setQuery("", false);
//                }
//            }
//        });
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            @Override
//            public boolean onQueryTextSubmit(String arg0) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(MainActivity.this, ActivitySearchVideo.class);
//                intent.putExtra("search", arg0);
//                startActivity(intent);
//                searchView.clearFocus();
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String arg0) {
//                // TODO Auto-generated method stub
//                return false;
//            }
//        });
        return false;
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        finish();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
}
