package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;
import com.imv.sucessneeti.signin.SignIn;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    private EditText cardnumEditTexst, cardnumEditlast, cardnumEditmobile, cardnumEditpass;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LinearLayout svParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        svParent = findViewById(R.id.svParent);
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        init();
        ImageView submit_button = findViewById(R.id.submit_btn);

        submit_button.setOnClickListener(v -> {
            if (cardnumEditTexst.getText().toString().matches("") || cardnumEditlast.getText().toString().matches("") ||
                    cardnumEditpass.getText().toString().matches("") || cardnumEditmobile.getText().toString().matches("")) {
                if (cardnumEditTexst.getText().toString().matches("")) {
                    cardnumEditTexst.setError("First Name is Required");
                }
                if (cardnumEditlast.getText().toString().matches("")) {
                    cardnumEditlast.setError("Last Name is Required");
                }
                if (cardnumEditmobile.getText().toString().matches("")) {
                    cardnumEditmobile.setError("Mobile No. is Required");
                }
                if (cardnumEditpass.getText().toString().matches("")) {
                    cardnumEditpass.setError("Password is Required");
                }
                if (cardnumEditmobile.getText().toString().length() != 10 || cardnumEditlast.getText().toString().length() <= 3
                        || cardnumEditTexst.getText().toString().length() <= 6) {

                }
            } else {
                try {
                    jsondatrat();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Utilities.hideKeyboard(svParent, SignUp.this);

    }

    private void init() {
        cardnumEditTexst = findViewById(R.id.cardnumEditTexst);
        cardnumEditlast = findViewById(R.id.cardnumEditlast);
        cardnumEditmobile = findViewById(R.id.cardnumEditmobile);
        cardnumEditpass = findViewById(R.id.cardnumEditpass);
        cardnumEditmobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditmobile.getText().toString().length() <= 0) {
                    cardnumEditmobile.setError("Enter Mobile No.");
                } else {
                    cardnumEditmobile.setError(null);
                }
            }
        });
        cardnumEditpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpass.getText().toString().length() <= 0) {
                    cardnumEditpass.setError("Enter Password");
                } else if (cardnumEditpass.getText().toString().length() <= 5) {
                    cardnumEditpass.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditpass.setError(null);
                }
            }
        });
        cardnumEditlast.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditlast.getText().toString().length() <= 0) {
                    cardnumEditlast.setError("Enter Last name");
                } else if (cardnumEditlast.getText().toString().length() <= 3) {
                    cardnumEditlast.setError("Enter Valid name");
                } else {
                    cardnumEditlast.setError(null);
                }
            }
        });
        cardnumEditTexst.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditTexst.getText().toString().length() <= 0) {
                    cardnumEditTexst.setError("Enter First name");
                } else if (cardnumEditTexst.getText().toString().length() <= 3) {
                    cardnumEditTexst.setError("Enter Valid name");
                } else {
                    cardnumEditTexst.setError(null);
                }
            }
        });
        TextView loginbtn = findViewById(R.id.loginbtn);
        TextView forgetpass = findViewById(R.id.forgetpass);
        loginbtn.setOnClickListener(v -> {
            Intent intent = new Intent(SignUp.this, SignIn.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        forgetpass.setOnClickListener(v -> {
            Intent intent = new Intent(SignUp.this, Forget_pass.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        JSONObject obj = new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            obj1.put("first_name", cardnumEditTexst.getText().toString());
            obj1.put("last_name", cardnumEditlast.getText().toString());
            obj1.put("mobile", cardnumEditmobile.getText().toString());
            obj1.put("device_id", deviceAppUID);
            obj1.put("password", cardnumEditpass.getText().toString());
            obj.put("transaction_data", obj1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/signup", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(SignUp.this, OtpScren.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("mobile", cardnumEditmobile.getText().toString());
                        startActivity(intent);
                    } else {
                        Toast.makeText(SignUp.this, response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, error -> {
            progressDialog.dismiss();
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ServerError) {
                message = "The server could not be found. Please try again after some time!!";
                Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ParseError) {
                message = "Parsing error! Please try again after some time!!";
                Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection.";
                Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

}
