package com.imv.sucessneeti.Activity.Documents;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;

import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Documentadd extends AppCompatActivity {
    Button button, adddocument;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String TAG = "DOCUMENTADD";
    EditText jobtitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documentadd);
        getSupportActionBar().setTitle("Add Document ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        RequestRunTimePermission();
        AllowRunTimePermission();
        EnableRuntimePermission();
        jobtitle = findViewById(R.id.jobtitle);
        button = (Button) findViewById(R.id.uploadocd);
        adddocument = (Button) findViewById(R.id.adddocument);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setType("application/pdf");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST);
                showImagePopup();

            }
        });
        adddocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jobtitle.getText().toString().matches("")) {
                    jobtitle.setError("Job title is Required");
                } else {
                    uploadImage();
                }
            }
        });
    }

    public void RequestRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(Documentadd.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
        } else {
            ActivityCompat.requestPermissions(Documentadd.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    public String getStringFile(File f) {
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream fileInputStream = new FileInputStream(f);

            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Uri getImageUri2(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(Documentadd.this,
                Manifest.permission.CAMERA)) {

            Toast.makeText(Documentadd.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(Documentadd.this, new String[]{
                    Manifest.permission.CAMERA}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case 1:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(Add_Word.this, "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    // Toast.makeText(Add_Word.this, "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }


//    private void save() {
//        if (!isOnline())
//        {
//            AlertDialog.Builder builder =new AlertDialog.Builder(this);
//            builder.setTitle("No internet Connection");
//            builder.setMessage("Please turn on internet connection to continue");
//            builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//
//                }
//            });
//            AlertDialog alertDialog = builder.create();
//            alertDialog.setCancelable(false);
//            alertDialog.show();
//        }
//        else {
//
//            //  Toast.makeText(this, ""+getStringImage(bitmap), Toast.LENGTH_SHORT).show();
//            if (title.getText().toString().equals("")) {
//                Toast.makeText(this, "Enter Title", Toast.LENGTH_SHORT).show();
//
//            }  else {
//
//                progressDialog = ProgressDialog.show(Add_Word.this, "Please wait...", "saving...", true);
//                progressDialog.setCancelable(true);
//                String url = Base_Url.url+"upload.php";
//                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        Toast.makeText(Add_Word.this, "" + response, Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
//                        title.setText("");
//                        type.setText("");
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(Add_Word.this, "" + error, Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
//
//                    }
//                }) {
//                    @Override
//                    protected Map<String, String> getParams() throws AuthFailureError {
//
//                        Map<String, String> data = new HashMap<>();
//                        String image = getStringImage(bitmap);
//                        data.put("admin_id", admin_id);
//                        data.put("type", type.getText().toString());
//                        data.put("title", title.getText().toString());
//                        data.put("image", image);
//                        data.put("type_id", type_id);
//                        return data;
//                    }
//                };
//
//                requestQueue.add(stringRequest);
//
//            }
//
//        }
//
//    }

    public void AllowRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(Documentadd.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(Documentadd.this, "READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(Documentadd.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }


    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        return encoded;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    //SS HAS VALUE of image

//    private void insertdata() throws JSONException {
//        InputStream iStream = null;
//                  final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setCancelable(false);
//        progressDialog.setTitle("Please wait...");
//        progressDialog.show();
//        final RequestQueue requestQueue = Volley.newRequestQueue(this);
//        final String url ="http://new.successneeti.com/api/user/signup";
//        JSONObject obj=new JSONObject();
//        try {
//            //  String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//
//
//         SharedPreferences sharedPreferences1 = getSharedPreferences("TEMP", MODE_PRIVATE);
//          String iddd = sharedPreferences1.getString("id",null);
//            String doc = getStringFile(file);
//            Log.d("astduyas",doc);
//            JSONObject obj1=new JSONObject();
//            obj1.put("user_id", iddd);
//            obj1.put("title",jobtitle.getText().toString());
//            obj1.put("document",doc);
//            obj.put("transaction_data",obj1);
//            Log.d("asdfjbs,bdf",obj.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        final String requestBody = obj.toString();
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/user/uploaddocuments",new JSONObject(requestBody), new Response.Listener<JSONObject>() {
//
//
//            @Override
//            public void onResponse(JSONObject response) {
//
////                Toast.makeText(getApplicationContext(),
////                        "response-"+response, Toast.LENGTH_LONG).show();
//                Log.d("response",""+response);
//                try {
//                    progressDialog.dismiss();
//                    String ss = response.getString("host_code");
//                    if(ss.matches("0"))
//                    {
//                        Intent intent = new Intent(Documentadd.this, Documentadd.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("valueb","6");
//                        startActivity(intent);
//
//
//                    }
//                    else{
//                        Toast.makeText(Documentadd.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
//                    }
//                }catch (Exception e){
//                    progressDialog.dismiss();
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//progressDialog.dismiss();
//                String message = null;
//                if (error instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                } else if (error instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                } else if (error instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                } else if (error instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                } else if (error instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                } else if (error instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                    Toast.makeText(Documentadd.this, message, Toast.LENGTH_SHORT).show();
//                }
//
//            /*if (error.networkResponse == null) {
//                if (error.getClass().equals(TimeoutError.class)) {
//                    Toast.makeText(getApplicationContext(),
//                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
//                }
//            }*/
//
//            }
//        }){
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                String appid = sharedPreferences.getString("deviceid",null);
//                headers.put("Token",appid);
//
//
//                return headers;
//            }
//        };
//
//        requestQueue.add(jsonObjectRequest);
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            finish();
//        }
//        return super.onOptionsItemSelected(item);
//    }

//
//    private Button btn;
//    private TextView tv;
//    private String upload_URL = "https://demonuts.com/Demonuts/JsonTest/Tennis/uploadfile.php?";
//    private RequestQueue rQueue;
//    private ArrayList<HashMap<String, String>> arraylist;
//    String url = "https://www.google.com";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        btn = findViewById(R.id.btn);
//        tv = findViewById(R.id.tv);
//
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                intent.setType("application/pdf");
//                startActivityForResult(intent,1);
//            }
//        });
//
//        tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                startActivity(browserIntent);
//            }
//        });
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            // Get the Uri of the selected file
//            Uri uri = data.getData();
//            String uriString = uri.toString();
//            File myFile = new File(uriString);
//            String path = myFile.getAbsolutePath();
//            String displayName = null;
//
//            if (uriString.startsWith("content://")) {
//                Cursor cursor = null;
//                try {
//                    cursor = this.getContentResolver().query(uri, null, null, null, null);
//                    if (cursor != null && cursor.moveToFirst()) {
//                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                        Log.d("nameeeee>>>>  ",displayName);
//
//                        uploadPDF(displayName,uri);
//                    }
//                } finally {
//                    cursor.close();
//                }
//            } else if (uriString.startsWith("file://")) {
//                displayName = myFile.getName();
//                Log.d("nameeeee>>>>  ",displayName);
//            }
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//    }

//    private void uploadPDF(final String pdfname, Uri pdffile){
//
//        InputStream iStream = null;
//        try {
//
//            iStream = getContentResolver().openInputStream(pdffile);
//            final byte[] inputData = getBytes(iStream);
//
//            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, upload_URL,
//                    new Response.Listener<NetworkResponse>() {
//                        @Override
//                        public void onResponse(NetworkResponse response) {
//                            Log.d("ressssssoo",new String(response.data));
//                            rQueue.getCache().clear();
//                            try {
//                                JSONObject jsonObject = new JSONObject(new String(response.data));
//                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//
//                                jsonObject.toString().replace("\\\\","");
//
//                                if (jsonObject.getString("status").equals("true")) {
//                                    Log.d("come::: >>>  ","yessssss");
//                                    arraylist = new ArrayList<HashMap<String, String>>();
//                                    JSONArray dataArray = jsonObject.getJSONArray("data");
//
//
//                                    for (int i = 0; i < dataArray.length(); i++) {
//                                        JSONObject dataobj = dataArray.getJSONObject(i);
//                                        url = dataobj.optString("pathToFile");
//                                        tv.setText(url);
//                                    }
//
//
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    }) {
//
//                /*
//                 * If you want to add more parameters with the image
//                 * you can do it here
//                 * here we have only one parameter with the image
//                 * which is tags
//                 * */
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<>();
//                    // params.put("tags", "ccccc");  add string parameters
//                    return params;
//                }
//
//                /*
//                 *pass files using below method
//                 * */
//                @Override
//                protected Map<String, DataPart> getByteData() {
//                    Map<String, DataPart> params = new HashMap<>();
//
//                    params.put("filename", new DataPart(pdfname ,inputData));
//                    return params;
//                }
//            };
//
//
//            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
//                    0,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            rQueue = Volley.newRequestQueue(Documentadd.this);
//            rQueue.add(volleyMultipartRequest);
//
//
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    private static final int FILE_SELECT_CODE = 0;

    public void showImagePopup() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("application/pdf");
            try {
                startActivityForResult(intent, FILE_SELECT_CODE);
            } catch (ActivityNotFoundException e) {
                e.getMessage();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    String filePath = "";
    int fileSize;
    File myFile;
    String displayName = "";
    private String strImage = "";
    String extension;
    Long lastmodified;
    String filenames;
    String path;
    String size;
    String imagePath;
    public static String imageName = "";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Uri uri;
            if (requestCode == 20 && resultCode == RESULT_OK) {
                filePath = Environment.getExternalStorageDirectory().getPath() + "/KormoanCRM/" + imageName;
                File file = new File(filePath);
                uri = Uri.fromFile(file);


                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);


                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

//                Glide.with(Documentadd.this).load(new File(imagePath))
//                        .into(show);


            } else {
                filePath = getPath(Documentadd.this, data.getData());
                uri = data.getData();


            }

            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};


            Log.e("FILE PATH", "---" + filePath);

            String uriString = uri.toString();
            myFile = new File(filePath);
            Log.e("File", "---" + myFile);
            Log.e("File SIZE", "---" + Integer.parseInt(String.valueOf(myFile.length() / 1024)));
            fileSize = Integer.parseInt(String.valueOf(myFile.length() / 1024));
            // Date lastModDate = new Date(myFile.lastModified());
            // Log.i("File last modified @ : "+ lastModDate.toString());


            button.setText(filenames);

            if (fileSize > 200) {
                Toast.makeText(Documentadd.this, "Please select file less than 200 kb.",
                        Toast.LENGTH_SHORT).show();
            } else {
                if (uriString.startsWith("content://")) {
                    Cursor cursor = null;
                    try {
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        if (cursor != null && cursor.moveToFirst()) {
                            displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }
                    } finally {
                        cursor.close();
                    }
                } else if (uriString.startsWith("file://")) {
                    displayName = myFile.getName();
                }
                Log.e("display name ", ">>" + displayName);
                // strImage = convertFileToByteArray(myFile);

                Log.e("Base64", "---" + strImage);
                size = String.valueOf(fileSize);

                extension = displayName.substring(displayName.lastIndexOf("."));
                filenames = displayName;
                filenames = filenames.substring(0, filenames.indexOf("."));

                //   Toast.makeText(this, "" + displayName, Toast.LENGTH_SHORT).show();
                button.setText(displayName);

            }
        } catch (Exception e) {
            Toast.makeText(Documentadd.this, "" + e, Toast.LENGTH_SHORT).show();
            Log.i("FILE_UPLOAD", e + "");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void uploadImage() {

        /**
         * Progressbar to Display if you need
         */
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(Documentadd.this);
        progressDialog.setMessage("PLease Wait");
        progressDialog.show();

        //Create Upload Server Client


        //File creating from selected URL


        File file = new File(filePath);


        // create RequestBody instance from file

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);


        MultipartBody.Part body = MultipartBody.Part.createFormData("pdf", file.getName(), requestFile);
        String iddd = sharedPreferences.getString("id", null);

        RequestBody user_id = RequestBody.create(MultipartBody.FORM, iddd);

        RequestBody name = RequestBody.create(MultipartBody.FORM, jobtitle.getText().toString().trim());


        Call<ReferData> ReferDataCall = APIClient.getInstance().processRefer(user_id, name, body);

        // finally, execute the request
        ReferDataCall.enqueue(new Callback<ReferData>() {
            @Override
            public void onResponse(Call<ReferData> call, retrofit2.Response<ReferData> response) {

                progressDialog.dismiss();

                Log.d("RESPONSE", new Gson().toJson(response.body()));
                // Response Success or Fail
                if (response.code() == 200) {


                    Toast.makeText(Documentadd.this, "Successfully Added", Toast.LENGTH_SHORT).show();
                    //     startActivity(new Intent(Documentadd.this, MainActivity.class));
                    Intent intent = new Intent(Documentadd.this, MainActivity.class);
                    intent.putExtra("valueb", "6");
                    startActivity(intent);

                    finish();
                    // overridePendingTransition(R.anim.enter_from_right,R.anim.exit_out_right);
                }


//                else {
//
//                    Toast.makeText(ReferActivity.this, getString(R.string.unexpected_response), Toast.LENGTH_SHORT).show();
//                }
            }


            @Override
            public void onFailure(Call<ReferData> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
                progressDialog.dismiss();
            }

        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}


