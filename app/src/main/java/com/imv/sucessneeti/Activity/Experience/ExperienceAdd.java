package com.imv.sucessneeti.Activity.Experience;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.Category;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ExperienceAdd extends AppCompatActivity {
    private Spinner spinner, et_funcationarea, et_salary;
    private ArrayList<String> CountryName;
    private ArrayList<String> functarea;
    private ArrayList<String> salaryss;
    private ArrayList<String> idspinej;
    String currentvale, streamval, yearset;
    private EditText jobtitle, companyname, datefirst, datesecond;
    private Button button;
    int subbb = 0;
    final Calendar myCalendar = Calendar.getInstance();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private ArrayList<Category> distrArrL;
    private ArrayList<Category> subjectclass;
    private ArrayList<Category> yearlistmi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experience_add);
        getSupportActionBar().setTitle("Add Experience ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        distrArrL = new ArrayList<>();
        subjectclass = new ArrayList<>();
        yearlistmi = new ArrayList<>();

        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();


        spinner = findViewById(R.id.et_industry);
        et_funcationarea = findViewById(R.id.et_ed_stfunpusr);
        et_salary = findViewById(R.id.et_salaryss);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel2();
            }

        };
        datefirst = findViewById(R.id.datefirst);
        datesecond = findViewById(R.id.datesecond);
        datefirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ExperienceAdd.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                datefirst.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
//                                txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

//                new DatePickerDialog(ExperienceAdd.this, date, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        datesecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ExperienceAdd.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                datesecond.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
//                                txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                try {
                                    //Dates to compare
                                    String CurrentDate = datefirst.getText().toString();
                                    String FinalDate = datesecond.getText().toString();

                                    Date date1;
                                    Date date2;

                                    SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

                                    //Setting dates
                                    date1 = dates.parse(CurrentDate);
                                    date2 = dates.parse(FinalDate);

                                    //Comparing dates
                                    long difference = (date1.getTime() - date2.getTime());
                                    long differenceDates = difference / (24 * 60 * 60 * 1000);

                                    //Convert long to String
                                    int dayDifference = Integer.parseInt(Long.toString(differenceDates));
                                    Log.d("hhhh", String.valueOf(dayDifference));
                                    if (dayDifference <= 0) {
                                        subbb = 1;
                                        Log.d("yyyyy", String.valueOf(dayDifference));
//                                        datesecond.setText(sdf.format(myCalendar.getTime()));
                                        datesecond.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                    } else {
                                        Log.d("mmmmm", String.valueOf(dayDifference));
                                        Toast.makeText(ExperienceAdd.this, "enter a valid date", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception exception) {
                                    Log.e("DIDN'T WORK", "exception " + exception);
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

//                new DatePickerDialog(ExperienceAdd.this, date1, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        CountryName = new ArrayList<>();
        functarea = new ArrayList<>();
        salaryss = new ArrayList<>();
        idspinej = new ArrayList<>();
        init();
        try {
            addgetData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            yeardatta();

        } catch (Exception e) {
            e.printStackTrace();
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //currentvale = (String) parent.getItemAtPosition(position);
                //  currentvale = String.valueOf(position+1);
                //Log.d("sdklhsdflsn", "onItemSelected: "+currentvale);
                //currentvale = String.valueOf(spinner.getId());
                //   currentvale= spinner.getSelectedItem().toString();

                if (position == 0) {

                } else {

                    currentvale = subjectclass.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_funcationarea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {

                } else {

                    streamval = distrArrL.get(position).getId();
                }
                //    streamval = String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_salary.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearset = yearlistmi.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void init() {
        jobtitle = findViewById(R.id.jobtitle);
        companyname = findViewById(R.id.compname);

        button = findViewById(R.id.addeducation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jobtitle.getText().toString().matches("") || datefirst.getText().toString().matches("") || datesecond.getText().toString().matches("") ||
                        companyname.getText().toString().matches("")) {

                    if (jobtitle.getText().toString().matches("")) {
                        jobtitle.setError("Job Title is Required");
                    }
                    if (datefirst.getText().toString().matches("")) {
                        datefirst.setError("Date is Required..");
                    }
                    if (datesecond.getText().toString().matches("")) {
                        datesecond.setError("This Feild is Required");
                    }
                } else if (subbb == 0) {
                    Toast.makeText(ExperienceAdd.this, "Enter a valid experience", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        insertdata();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(ExperienceAdd.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            editor.apply();
            String idd = sharedPreferences.getString("id", null);
            obj1.put("user_id", idd);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("industry_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("asjdsajlkd", response.toString());
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            subjectclass.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < subjectclass.size(); i++) {
                            CountryName.add(subjectclass.get(i).getName().toString());

                        }
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            CountryName.add(country);
//                            idspinej.add(iddd);
//                        }
                        Log.d("datatatta", functarea.toString());
                        ArrayAdapter arrayAdapter = new ArrayAdapter(ExperienceAdd.this, R.layout.simple_spinner_dropdown, CountryName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        spinner.setAdapter(arrayAdapter);


                        spinner.setId(Integer.valueOf(String.valueOf(idspinej)));
                    } else {
                        Toast.makeText(ExperienceAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                }


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String deviceApp = sharedPreferences.getString("deviceid", null);
                headers.put("Token", deviceApp);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void addgetData() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(ExperienceAdd.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("education_id", currentvale);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("responsemeto", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("functional_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            distrArrL.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < distrArrL.size(); i++) {
                            functarea.add(distrArrL.get(i).getName().toString());

                        }
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            functarea.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(ExperienceAdd.this, R.layout.simple_spinner_dropdown, functarea) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_funcationarea.setAdapter(arrayAdapter);


                        et_funcationarea.setId(Integer.valueOf(String.valueOf(idspinej)));
                    } else {
                        Toast.makeText(ExperienceAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String appid = sharedPreferences.getString("deviceid", null);
                headers.put("Token", appid);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
//year_list
    //name

    private void yeardatta() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(ExperienceAdd.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String iddof = sharedPreferences.getString("id", null);
            obj1.put("user_id", iddof);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("salary");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            yearlistmi.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < yearlistmi.size(); i++) {
                            salaryss.add(yearlistmi.get(i).getName().toString());

                        }

//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            salaryss.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(ExperienceAdd.this, R.layout.simple_spinner_dropdown, salaryss);
                        arrayAdapter.notifyDataSetChanged();
                        et_salary.setAdapter(arrayAdapter);


                        et_salary.setId(Integer.valueOf(String.valueOf(idspinej)));
                    } else {
                        Toast.makeText(ExperienceAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apidf = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apidf);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void insertdata() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            //  String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            String idme = sharedPreferences.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("user_id", idme);
            obj1.put("function_id", currentvale);
            obj1.put("industries_id", streamval);
            obj1.put("job_title", jobtitle.getText().toString());
            obj1.put("company_name", companyname.getText().toString());
            obj1.put("from_date", datefirst.getText().toString());
            obj1.put("to_date", datesecond.getText().toString());
            obj1.put("experience_year", "null");
            obj1.put("experience_month", "null");
            obj1.put("salary", yearset);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/addexperience", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(ExperienceAdd.this, "Experience Add Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ExperienceAdd.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb", "5");
                        startActivity(intent);


                    } else {
                        Toast.makeText(ExperienceAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(ExperienceAdd.this, message, Toast.LENGTH_SHORT).show();
                }

            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");

                String apigs = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apigs);


                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updateLabel() {
        String myFormat = "DD/MM/YYYY"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        datefirst.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabel2() {
        String myFormat = "DD/MM/YYYY"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        datesecond.setText(sdf.format(myCalendar.getTime()));
        try {
            //Dates to compare
            String CurrentDate = datefirst.getText().toString();
            String FinalDate = datesecond.getText().toString();

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = (date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            int dayDifference = Integer.parseInt(Long.toString(differenceDates));
            Log.d("hhhh", String.valueOf(dayDifference));
            if (dayDifference >= 0) {
                subbb = 1;
                Log.d("yyyyy", String.valueOf(dayDifference));
                datesecond.setText(sdf.format(myCalendar.getTime()));

            } else {
                Log.d("mmmmm", String.valueOf(dayDifference));
                Toast.makeText(this, "enter a valid date", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
