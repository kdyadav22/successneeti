
package com.imv.sucessneeti.Activity.edumodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionData {

    @SerializedName("id")
    @Expose
    private String Id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("education_id")
    @Expose
    private String educationId;
    @SerializedName("stream_id")
    @Expose
    private String streamId;
    @SerializedName("board_name")
    @Expose
    private String boardName;
    @SerializedName("subject_marks")
    @Expose
    private String subjectMarks;
    @SerializedName("obtain_marks")
    @Expose
    private String obtainMarks;
    @SerializedName("percentage")
    @Expose
    private String percentage;
    @SerializedName("passing_year")
    @Expose
    private String passingYear;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getSubjectMarks() {
        return subjectMarks;
    }

    public void setSubjectMarks(String subjectMarks) {
        this.subjectMarks = subjectMarks;
    }

    public String getObtainMarks() {
        return obtainMarks;
    }

    public void setObtainMarks(String obtainMarks) {
        this.obtainMarks = obtainMarks;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
