package com.imv.sucessneeti.Activity.photoupdate;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.fragment.PhotoFrag;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoUpdate extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_User_Document1 = "doc1";
    ImageView IDProf;
    Button Upload_Btn, adddocument;
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;
    private String Document_img1 = "";
    Uri picUri;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 107;
    private final static int IMAGE_RESULT = 200;
    Button btncam;
    ImageView colotext, changeback;
    ImageView imageViewProfilePic;

    //  ImageView imagestest;
    int position = 999;

    @SuppressLint("RestrictedApi")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_update);
        sharedPreferenceswe = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();


        btncam = (Button) findViewById(R.id.uploadocd);
        adddocument = findViewById(R.id.adddocument);
        adddocument.setOnClickListener(this);

        btncam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT);
            }
        });

        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


        imageViewProfilePic = (ImageView) findViewById(R.id.IdProf);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Choose source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK) {

            ImageView imageView = findViewById(R.id.IdProf);

            if (requestCode == IMAGE_RESULT) {

                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(filePath);
                    imageView.setImageBitmap(bitmap);
//                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
//                    imageView.setImageBitmap(bitmap);
                    Document_img1 = getStringImage(bitmap);
                }
            }

        }

    }


    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("pic_uri", picUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        picUri = savedInstanceState.getParcelable("pic_uri");
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void SendDetail() {
        final ProgressDialog loading = new ProgressDialog(this);
        loading.setMessage("Please Wait...");
        loading.show();
        loading.setCanceledOnTouchOutside(false);
//        RetryPolicy mRetryPolicy = new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConfiURL.Registration_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            loading.dismiss();
//                            Log.d("JSON", response);
//
//                            JSONObject eventObject = new JSONObject(response);
//                            String error_status = eventObject.getString("error");
//                            if (error_status.equals("true")) {
//                                String error_msg = eventObject.getString("msg");
//                                ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                                alertDialogBuilder.setTitle("Vendor Detail");
//                                alertDialogBuilder.setCancelable(false);
//                                alertDialogBuilder.setMessage(error_msg);
//                                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//
//                                    }
//                                });
//                                alertDialogBuilder.show();
//
//                            } else {
//                                String error_msg = eventObject.getString("msg");
//                                ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                                alertDialogBuilder.setTitle("Registration");
//                                alertDialogBuilder.setCancelable(false);
//                                alertDialogBuilder.setMessage(error_msg);
////                                alertDialogBuilder.setIcon(R.drawable.doubletick);
//                                alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        Intent intent=new Intent(Uplode_Reg_Photo.this,Log_In.class);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                });
//                                alertDialogBuilder.show();
//                            }
//                        }catch(Exception e){
//                            Log.d("Tag", e.getMessage());
//
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loading.dismiss();
//                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                            ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                            alertDialogBuilder.setTitle("No connection");
//                            alertDialogBuilder.setMessage(" Connection time out error please try again ");
//                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                }
//                            });
//                            alertDialogBuilder.show();
//                        } else if (error instanceof AuthFailureError) {
//                            ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                            alertDialogBuilder.setTitle("Connection Error");
//                            alertDialogBuilder.setMessage(" Authentication failure connection error please try again ");
//                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                }
//                            });
//                            alertDialogBuilder.show();
//                            //TODO
//                        } else if (error instanceof ServerError) {
//                            ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                            alertDialogBuilder.setTitle("Connection Error");
//                            alertDialogBuilder.setMessage("Connection error please try again");
//                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                }
//                            });
//                            alertDialogBuilder.show();
//                            //TODO
//                        } else if (error instanceof NetworkError) {
//                            ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                            alertDialogBuilder.setTitle("Connection Error");
//                            alertDialogBuilder.setMessage("Network connection error please try again");
//                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                }
//                            });
//                            alertDialogBuilder.show();
//                            //TODO
//                        } else if (error instanceof ParseError) {
//                            ContextThemeWrapper ctw = new ContextThemeWrapper( Uplode_Reg_Photo.this, R.style.Theme_AlertDialog);
//                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
//                            alertDialogBuilder.setTitle("Error");
//                            alertDialogBuilder.setMessage("Parse error");
//                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                }
//                            });
//                            alertDialogBuilder.show();
//                        }
////                        Toast.makeText(Login_Activity.this,error.toString(), Toast.LENGTH_LONG ).show();
//                    }
//                }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> map = new HashMap<String,String>();
//                map.put(KEY_User_Document1,Document_img1);
//                return map;
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        stringRequest.setRetryPolicy(mRetryPolicy);
//        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (Document_img1.equals("")) {
            ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.Theme_AppCompat_Dialog_Alert);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
            alertDialogBuilder.setTitle("Id Proof can't empty ");
            alertDialogBuilder.setMessage("ID Proof can't be empty, please select any document");
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            alertDialogBuilder.show();
        } else {
            try {
                insertdata();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //    private Bitmap bitmap;
//    Bitmap bitmap4;
//    private Bitmap bitmap2;
//    private int PICK_IMAGE_REQUEST = 1;
//    private int PICK_IMAGE_REQUEST2 = 2;
//    ProgressDialog progressDialog;
//    Uri uri;
//    String ss,mm;
//    EditText jobtitle;
//    ImageView IDProf;
//    final int CROP_PIC = 3;
//    Uri imageUri;
//    String spnnval,number;
//    Button button,adddocument;
//    int dsss= 1;
//    SharedPreferences sharedPreferenceswe;
//    SharedPreferences.Editor editor;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_photo_update);
//        getSupportActionBar().setTitle("Update Photo ");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//
//        sharedPreferenceswe = getSharedPreferences("TEMP", MODE_PRIVATE);
//        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();
//
//        AllowRunTimePermission();
//        EnableRuntimePermission();
//        IDProf = findViewById(R.id.idproof);
//        button =(Button) findViewById(R.id.uploadocd);
//        adddocument = (Button) findViewById(R.id.adddocument);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectImage();
//            }
//        });
//        adddocument.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                    try{
//                        insertdata();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//
//        });
//    }
//
//    private void selectImage() {
//
//        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
//        AlertDialog.Builder builder = new AlertDialog.Builder(PhotoUpdate.this);
//        builder.setTitle("Add Photo!");
//        builder.setItems(options, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                int permissionCheck = ContextCompat.checkSelfPermission(PhotoUpdate.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(PhotoUpdate.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                } else {
//                    int permissionCheck1 = ContextCompat.checkSelfPermission(PhotoUpdate.this, Manifest.permission.CAMERA);
//
//                    if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(PhotoUpdate.this, new String[]{Manifest.permission.CAMERA}, 1);
//                    } else {
//                        if (options[item].equals("Take Photo")) {
//
//                            ContentValues values = new ContentValues();
//                            values.put(MediaStore.Images.Media.TITLE, "New Picture");
//                            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
//                            imageUri = getContentResolver().insert(
//                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                            startActivityForResult(intent, PICK_IMAGE_REQUEST2);
////give camera permission
//
//                        } else if (options[item].equals("Choose from Gallery")) {
//                            Intent intent = new Intent();
//                            intent.setType("image/*");
//                            intent.setAction(Intent.ACTION_GET_CONTENT);
//                            startActivityForResult(Intent.createChooser(intent, "pic"), PICK_IMAGE_REQUEST);
//
//                        } else if (options[item].equals("Cancel")) {
//                            dialog.dismiss();
//                        }
//                    }
//                }
//
//            }
//        });
//        builder.show();
//    }
//
//
//
//
//
//    @SuppressLint("MissingSuperCall")
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//            Uri filePath = data.getData();
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//
//                String name = filePath.getPath();
//                //choose_image.setText("Selected");
//                IDProf.setImageBitmap(bitmap);
//                //choose_image.setBackgroundColor(Color.rgb(0,150,0));
//
//                ss=    getStringImage(bitmap);
//                dsss = 2;
//                Log.d("neeeee",ss.toString());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } else if (requestCode == PICK_IMAGE_REQUEST2) {
////================================
///*
//           bitmap = (Bitmap) data.getExtras().get("data");
//            imageView.setImageBitmap(bitmap);
//            uri=getImageUri(getApplicationContext(),bitmap);
//           //performCrop();
// */
////====================================dsss
//
//            Bitmap thumbnail = null;
//            try {
//                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
//                //imageView.setImageBitmap(thumbnail);
//                //uri = getRealPathFromURI(imageUri);
//                uri=getImageUri(getApplicationContext(),thumbnail);
//                performCrop();
//                ss=    getStringImage(thumbnail);
//                dsss = 2;
//                Log.d("neeeee",ss.toString());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//
//            //  ==================
//        }
//        else if (requestCode == CROP_PIC) {
//
//            if(data.getData()==null){
//                bitmap = (Bitmap)data.getExtras().get("data");
//
//                IDProf.setImageBitmap(bitmap);
//
//            }else{
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(PhotoUpdate.this.getContentResolver(), data.getData());
//                    IDProf.setImageBitmap(bitmap);
//
//                } catch (IOException e) {
//
//                    e.printStackTrace();
//
//                }
//            }
//        }
//
//
//
//    }
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
//    public Uri getImageUri2(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
//
//
//    public void EnableRuntimePermission() {
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(PhotoUpdate.this,
//                Manifest.permission.CAMERA)) {
//
//            Toast.makeText(PhotoUpdate.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
//
//        } else {
//
//            ActivityCompat.requestPermissions(PhotoUpdate.this, new String[]{
//                    Manifest.permission.CAMERA}, 1);
//
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
//
//        switch (RC) {
//
//            case 1:
//
//                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    //Toast.makeText(Add_Word.this, "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
//
//                } else {
//
//                    // Toast.makeText(Add_Word.this, "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
//
//                }
//                break;
//        }
//    }
//
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;

    }

    //
//    private void performCrop() {
//        // take care of exceptions
//        try {
//            // call the standard crop action intent (the user device may not
//            // support it)
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            // indicate image type and Uri
//            cropIntent.setDataAndType(uri, "image/*");
//            // set crop properties
//            cropIntent.putExtra("crop", "true");
//            // indicate aspect of desired crop
//            cropIntent.putExtra("aspectX", .8);
//            cropIntent.putExtra("aspectY", .6);
//            // indicate output X and Y
//            cropIntent.putExtra("outputX", 400);
//            cropIntent.putExtra("outputY", 250);
//            // retrieve data on return
//            cropIntent.putExtra("return-data", true);
//            // start the activity - we handle returning in onActivityResult
//            startActivityForResult(cropIntent, CROP_PIC);
//        }
//        // respond to users whose devices do not support the crop action
//        catch (ActivityNotFoundException anfe) {
//            Toast toast = Toast
//                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//
//
////    private void save() {
////        if (!isOnline())
////        {
////            AlertDialog.Builder builder =new AlertDialog.Builder(this);
////            builder.setTitle("No internet Connection");
////            builder.setMessage("Please turn on internet connection to continue");
////            builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
////                @Override
////                public void onClick(DialogInterface dialog, int which) {
////                    dialog.dismiss();
////
////                }
////            });
////            AlertDialog alertDialog = builder.create();
////            alertDialog.setCancelable(false);
////            alertDialog.show();
////        }
////        else {
////
////            //  Toast.makeText(this, ""+getStringImage(bitmap), Toast.LENGTH_SHORT).show();
////            if (title.getText().toString().equals("")) {
////                Toast.makeText(this, "Enter Title", Toast.LENGTH_SHORT).show();
////
////            }  else {
////
////                progressDialog = ProgressDialog.show(Add_Word.this, "Please wait...", "saving...", true);
////                progressDialog.setCancelable(true);
////                String url = Base_Url.url+"upload.php";
////                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
////                    @Override
////                    public void onResponse(String response) {
////
////                        Toast.makeText(Add_Word.this, "" + response, Toast.LENGTH_SHORT).show();
////                        progressDialog.dismiss();
////                        title.setText("");
////                        type.setText("");
////
////                    }
////                }, new Response.ErrorListener() {
////                    @Override
////                    public void onErrorResponse(VolleyError error) {
////                        Toast.makeText(Add_Word.this, "" + error, Toast.LENGTH_SHORT).show();
////                        progressDialog.dismiss();
////
////                    }
////                }) {
////                    @Override
////                    protected Map<String, String> getParams() throws AuthFailureError {
////
////                        Map<String, String> data = new HashMap<>();
////                        String image = getStringImage(bitmap);
////                        data.put("admin_id", admin_id);
////                        data.put("type", type.getText().toString());
////                        data.put("title", title.getText().toString());
////                        data.put("image", image);
////                        data.put("type_id", type_id);
////                        return data;
////                    }
////                };
////
////                requestQueue.add(stringRequest);
////
////            }
////
////        }
////
////    }
//
//    public void AllowRunTimePermission(){
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(PhotoUpdate.this, Manifest.permission.READ_EXTERNAL_STORAGE))
//        {
//
//            Toast.makeText(PhotoUpdate.this,"READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();
//
//        } else {
//
//            ActivityCompat.requestPermissions(PhotoUpdate.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//
//        }
//    }
//
//
//
//
//
//    public String getRealPathFromURI(Uri contentUri) {
//        String[] proj = { MediaStore.Images.Media.DATA };
//        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
//        int column_index = cursor
//                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }
    private void insertdata() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            //  String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            SharedPreferences sharedPreferences1 = getSharedPreferences("CONTT", Context.MODE_PRIVATE);

            String sse = sharedPreferences1.getString("DATA", null);
            String idf = sharedPreferenceswe.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("user_id", idf);
            obj1.put("type", sse);
            obj1.put("image", Document_img1);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/uploadphotos", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(PhotoUpdate.this, "Image saved successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PhotoUpdate.this, MainActivity.class);
                        intent.putExtra("valueb", "1");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    } else {
                        Toast.makeText(PhotoUpdate.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),
                        "error" + error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiuyt);


                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
