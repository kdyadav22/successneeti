package com.imv.sucessneeti.Activity.Documents.test;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.RemoteMessage;
import com.imv.sucessneeti.Activity.Documents.FilePath;
import com.imv.sucessneeti.R;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.UUID;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class Main2Activity extends AppCompatActivity  implements SingleUploadBroadcastReceiver.Delegate {

    private static final String TAG = "AndroidUploadService";

    private final SingleUploadBroadcastReceiver uploadReceiver =
            new SingleUploadBroadcastReceiver();
  //  public static final String PDF_UPLOAD_HTTP_URL = "http://192.168.1.10/pdfnew/homeitr.php";
  public static final String PDF_UPLOAD_HTTP_URL = "http://new.successneeti.com/api/uploadpdf/addpdf";
    public int PDF_REQ_CODE = 1;
    String PdfNameHolder,PdfPathHolder,PdfID;
    Button pan,finsubmit;
    ProgressDialog progressDialog;
    private static final String MYPREF = "MyPref";
    RequestQueue requestQueue;

    int statuspan =1;
    String Vendormail,Phone;
    Uri uri,uri1;
    String iscahna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        AllowRunTimePermission();
        pan = (Button) findViewById(R.id.panbtnattch);
        finsubmit = (Button) findViewById(R.id.finsubmit);

        requestQueue = Volley.newRequestQueue(Main2Activity.this);

        progressDialog = new ProgressDialog(Main2Activity.this);


        pan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent = new Intent();

                intent.setType("application/pdf");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);

            }
        });

        finsubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {

                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "";
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(Main2Activity.this, channelId);
                    Notification notification = notificationBuilder.setOngoing(true)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setPriority(PRIORITY_MIN)
                            .setCategory(NotificationCompat.CATEGORY_SERVICE)
                            .build();



                if(statuspan == 1)
                {
                    Toast.makeText(Main2Activity.this, "Please attach pan card", Toast.LENGTH_SHORT).show();
                }

                else {

                    uploadMultipart(Main2Activity.this);

                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        uploadReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
    }

    public void uploadMultipart(final Context context) {
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        progressDialog.setMessage("Uploading file, please wait.");
//        progressDialog.setMax(100);
//        progressDialog.setCancelable(true);
//        progressDialog.show();
        try {
            PdfPathHolder = FilePath.getFilePathForN(uri,this);
            String uploadId = UUID.randomUUID().toString();
            uploadReceiver.setDelegate(this);
            uploadReceiver.setUploadID(uploadId);
            Log.d("path",PdfPathHolder.toString());
            new MultipartUploadRequest(context, uploadId, PDF_UPLOAD_HTTP_URL)
                    .addFileToUpload(PdfPathHolder, "pdf")
                    .addParameter("name","tata")
                    .addParameter("user_id","9")
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();

        } catch (Exception exc) {
            Log.e(TAG, exc.getMessage(), exc);
        }
    }

    @Override
    public void onProgress(int progress) {
        //your implementation
    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {
        //your implementation
    }

    @Override
    public void onError(Exception exception) {
        //your implementation
    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {
        //your implementation
    }

    @Override
    public void onCancelled() {
        //your implementation
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PDF_REQ_CODE) {
            if (requestCode == PDF_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

                uri = data.getData();

                pan.setText("PDF is Selected");
                statuspan = 2;
            }
        }

    }
//
//    public void PdfUploadFunction() {
//
//        //SharedPreferences sharedPreferences = getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
//        PdfNameHolder = "1";
//        PdfPathHolder = FilePath.getPath(this, uri);
//
//        if (PdfPathHolder == null) {
//
//            Toast.makeText(this, "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
//
//        } else {
//
//            try {
//
//                PdfID = UUID.randomUUID().toString();
//
//                new MultipartUploadRequest(this, PdfID, PDF_UPLOAD_HTTP_URL)
//                        .addFileToUpload(PdfPathHolder, "pdf")
//                        .addParameter("name", PdfNameHolder)
//                        .setMaxRetries(5)
//                        .startUpload();
//                //Toast.makeText(this, "sucess", Toast.LENGTH_SHORT).show();
//            } catch (Exception exception) {
//
//                Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }
//
    public void AllowRunTimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(Main2Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
        {

            Toast.makeText(Main2Activity.this,"READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(Main2Activity.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }
//
//    @Override
//    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {
//
//        switch (RC) {
//
//            case 1:
//
//                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    Toast.makeText(Main2Activity.this,"Permission Granted", Toast.LENGTH_LONG).show();
//
//                } else {
//
//                    Toast.makeText(Main2Activity.this,"Permission Canceled", Toast.LENGTH_LONG).show();
//
//                }
//                break;
//        }
//    }
@RequiresApi(Build.VERSION_CODES.O)
private String createNotificationChannel(NotificationManager notificationManager){
    String channelId = "my_service_channelid";
    String channelName = "My Foreground Service";
    NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
    // omitted the LED color
    channel.setImportance(NotificationManager.IMPORTANCE_NONE);
    channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
    notificationManager.createNotificationChannel(channel);
    return channelId;
}


}