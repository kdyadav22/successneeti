
package com.imv.sucessneeti.Activity.edumodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddEducation {

    @SerializedName("transaction_data")
    @Expose
    private TransactionData transactionData;

    public TransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(TransactionData transactionData) {
        this.transactionData = transactionData;
    }

}
