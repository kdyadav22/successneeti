package com.imv.sucessneeti.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imv.sucessneeti.Activity.edumodel.AddEducation;
import com.imv.sucessneeti.Activity.edumodel.Detail;
import com.imv.sucessneeti.Activity.edumodel.TransactionData;
import com.imv.sucessneeti.Extra.Category;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.AddEducationAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EducationAdd extends AppCompatActivity implements AddEducationAdapter.AddSubjectDetailsCallback {
    private Spinner spinner, et_ed_streamspusr, et_yearpass;
    private ArrayList<String> CountryName;
    private ArrayList<String> streamlist;
    private ArrayList<String> yearlist;
    private ArrayList<String> idspinej;
    String currentvale = "", streamval = "", yearset = "";
    private EditText boardname, subjectmarks, obtaibmarks, percentagef;
    private Button button;
    private Button addMore;
    SharedPreferences sharedPreferences, sharedPreferencesforupdate;
    SharedPreferences.Editor editor;
    private ArrayList<Category> distrArrL;
    private ArrayList<Category> subjectclass;
    private ArrayList<Category> yearlistmi;

    int educationid, streamid, yearid;

    RecyclerView recyclerviewscreennadd;

    List<Detail> detailList = new ArrayList<>();

    float fPercentage;
    float totalMarks = 0;
    float obtainMarks = 0;
    private static final String MYFILE = "MyFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education_add);
        if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
            Objects.requireNonNull(getSupportActionBar()).setTitle("Update Education ");
        }else{
            Objects.requireNonNull(getSupportActionBar()).setTitle("Add Education ");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        distrArrL = new ArrayList<>();
        subjectclass = new ArrayList<>();
        yearlistmi = new ArrayList<>();
        recyclerviewscreennadd = findViewById(R.id.recyclerviewscreennadd);
        recyclerviewscreennadd.hasFixedSize();
        recyclerviewscreennadd.setLayoutManager(new LinearLayoutManager(this));
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        sharedPreferencesforupdate = getSharedPreferences(MYFILE, Context.MODE_PRIVATE);

        spinner = findViewById(R.id.et_ed_streamsp);
        et_ed_streamspusr = findViewById(R.id.et_ed_streamspusr);
        et_yearpass = findViewById(R.id.et_yearpass);

        addMore = findViewById(R.id.addMore);

        CountryName = new ArrayList<>();
        streamlist = new ArrayList<>();
        yearlist = new ArrayList<>();
        idspinej = new ArrayList<>();
        init();

        try {
            if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                int classsid = Integer.parseInt(sharedPreferencesforupdate.getString("selectclas", null));
                int streamidm = Integer.parseInt(sharedPreferencesforupdate.getString("streamid", null));
                int yearidm = Integer.parseInt(sharedPreferencesforupdate.getString("yeardata", null));
                if (classsid != 0) {
                    educationid = classsid;
                }
                if (streamidm != 0) {
                    streamid = streamidm - 1;
                }
                if (yearidm != 0) {
                    yearid = yearidm;
                }

                button.setText("Update");
                updatedatrat();
            } else {
                Detail detail = new Detail();
                detail.setSubjectName("");
                detail.setSubjectMarks(0);
                detail.setObtainMarks(0);

                detailList.add(detail);
                recyclerviewscreennadd.setAdapter(new AddEducationAdapter(detailList, this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            yeardatta();
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    currentvale = subjectclass.get(position).getId();
                    Log.d("sdklhsdflsn", "onItemSelected: " + currentvale);
                    //currentvale = String.valueOf(spinner.getId());
                    //   currentvale= spinner.getSelectedItem().toString();
                    streamlist.clear();
                    distrArrL.clear();
                    try {
                        addgetData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //currentvale = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_ed_streamspusr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    streamval = distrArrL.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_yearpass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    yearset = yearlistmi.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail detail = new Detail();
                detail.setSubjectName("");
                detail.setSubjectMarks(0);
                detail.setObtainMarks(0);

                detailList.add(detail);
                Objects.requireNonNull(recyclerviewscreennadd.getAdapter()).notifyDataSetChanged();
            }
        });
    }

    private void init() {
        boardname = findViewById(R.id.boardname);
        subjectmarks = findViewById(R.id.subjectmarks);
        obtaibmarks = findViewById(R.id.obtaibmarks);
        percentagef = findViewById(R.id.percentagef);
        button = findViewById(R.id.addeducation);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentvale.matches("")) {
                    Toast.makeText(EducationAdd.this, "Current Class is Required", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (streamval.matches("")) {
                    Toast.makeText(EducationAdd.this, "Current Stream is Required", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (boardname.getText().toString().matches("")) {
                    boardname.setError("Board/University Name is Required");
                    return;
                }

                if (yearset.matches("")) {
                    Toast.makeText(EducationAdd.this, "Passing year is Required", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {

                    for (int i = 0; i < detailList.size(); i++) {
                        totalMarks = totalMarks + detailList.get(i).getSubjectMarks();
                        obtainMarks = obtainMarks + detailList.get(i).getObtainMarks();
                    }

                    fPercentage = (obtainMarks / totalMarks) * 100;

                    TransactionData transactionData = new TransactionData();

                    if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                        transactionData.setId(sharedPreferencesforupdate.getString("rowId", null));
                    } else {
                        transactionData.setId("");
                    }

                    transactionData.setUserId(sharedPreferences.getString("id", null));
                    transactionData.setBoardName(boardname.getText().toString().trim());
                    transactionData.setDetails(detailList);
                    transactionData.setEducationId(currentvale);
                    transactionData.setStreamId(streamval);
                    transactionData.setObtainMarks(String.valueOf(obtainMarks));
                    transactionData.setSubjectMarks(String.valueOf(totalMarks));
                    transactionData.setPercentage(String.valueOf(fPercentage));
                    transactionData.setPassingYear(yearset);

                    AddEducation addEducation = new AddEducation();
                    addEducation.setTransactionData(transactionData);

                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(addEducation);

                    JSONObject jsonObj = new JSONObject(json);

                    Log.d("TAG", "" + jsonObj.toString());

                    if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                        updatedata(jsonObj);
                    } else {
                        insertdata(jsonObj);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(EducationAdd.this);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String sssr = sharedPreferences.getString("id", null);

            obj1.put("user_id", sssr);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("education_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            subjectclass.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < subjectclass.size(); i++) {
                            CountryName.add(subjectclass.get(i).getName());

                        }

//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            CountryName.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(EducationAdd.this, R.layout.simple_spinner_dropdown, CountryName) {
                            @Override
                            public boolean isEnabled(int position) {
                                return position != 0;
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        spinner.setAdapter(arrayAdapter);

                        if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                            for (int pos = arrayAdapter.getCount(); pos > 0; pos--) {
                                if (arrayAdapter.getItemId(pos - 1) == educationid) {
                                    spinner.setSelection(pos - 1);
                                    break;
                                }
                            }
                        } else {
                            spinner.setId(Integer.valueOf(String.valueOf(idspinej)));
                        }
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updatedatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(EducationAdd.this);
        JSONObject obj = new JSONObject();
        try {
            JSONObject obj1 = new JSONObject();
            String sssr = sharedPreferencesforupdate.getString("rowId", null);

            obj1.put("id", sssr);
            obj.put("transaction_data", obj1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/geteducationdetails", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        final JSONObject jsonObject = response.getJSONObject("result");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    boardname.setText(jsonObject.optString("board_name"));
                                    spinner.setPrompt(jsonObject.optString("education_name"));
                                    et_ed_streamspusr.setPrompt(jsonObject.optString("stream_name"));

                                    et_yearpass.setId(Integer.valueOf(jsonObject.optString("passing_year")));
                                    spinner.setId(Integer.valueOf(jsonObject.optString("education_id")));
                                    et_ed_streamspusr.setId(Integer.valueOf(jsonObject.optString("stream_id")));
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                            }
                        });

                        JSONArray jsonArray = jsonObject.getJSONArray("details");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Detail detail = new Detail();
                            detail.setSubjectMarks(Integer.parseInt(jsonObject1.optString("subject_marks")));
                            detail.setObtainMarks(Integer.parseInt(jsonObject1.optString("obtain_marks")));
                            detail.setSubjectName(jsonObject1.optString("subject_name"));
                            detailList.add(detail);
                        }
                        recyclerviewscreennadd.setAdapter(new AddEducationAdapter(detailList, EducationAdd.this));
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apiuyt);

                return headers;

            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void addgetData() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(EducationAdd.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("education_id", currentvale);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/streamlist", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("responsemeto", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        //      JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = response.getJSONArray("result");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(TextUtils.isEmpty(dataobj.getString("id")) ? "0" : dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            distrArrL.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < distrArrL.size(); i++) {
                            streamlist.add(distrArrL.get(i).getName());

                        }

//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            streamlist.add(country);
                        // }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(EducationAdd.this, R.layout.simple_spinner_dropdown, streamlist) {
                            @Override
                            public boolean isEnabled(int position) {
                                return position != 0;
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_ed_streamspusr.setAdapter(arrayAdapter);

                        if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                            for (int pos = arrayAdapter.getCount(); pos > 0; pos--) {
                                if (arrayAdapter.getItemId(pos - 1) == streamid) {
                                    et_ed_streamspusr.setSelection(pos-1);
                                    break;
                                }
                            }
                        }
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);


                return headers;

            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void yeardatta() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(EducationAdd.this);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("user_id", "1");


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("year_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

                            yearlistmi.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < yearlistmi.size(); i++) {
                            yearlist.add(yearlistmi.get(i).getName());

                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(EducationAdd.this, R.layout.simple_spinner_dropdown, yearlist) {
                            @Override
                            public boolean isEnabled(int position) {
                                return position != 0;
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_yearpass.setAdapter(arrayAdapter);

                        if (getIntent().getBooleanExtra("isComingToUpdate", false)) {
                            for (int pos = arrayAdapter.getCount(); pos > 0; pos--) {
                                if (Objects.requireNonNull(arrayAdapter.getItem(pos - 1)).equals(String.valueOf(yearid))) {
                                    et_yearpass.setSelection(pos-1);
                                    break;
                                }
                            }
                        } else {
                            et_yearpass.setId(Integer.valueOf(String.valueOf(idspinej)));
                        }
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void insertdata(JSONObject jsonObj) throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/addeducation";
        final String requestBody = jsonObj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(requestBody), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(EducationAdd.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb", "4");
                        startActivity(intent);
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error" + error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", Arrays.toString(networkResponse.data));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apiuyt);
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updatedata(JSONObject jsonObj) throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/updateeducation";
        final String requestBody = jsonObj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(requestBody), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(EducationAdd.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb", "4");
                        startActivity(intent);
                    } else {
                        Toast.makeText(EducationAdd.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error" + error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", Arrays.toString(networkResponse.data));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apiuyt);
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDeleteClicked(int pos) {
        detailList.remove(pos);
        Objects.requireNonNull(recyclerviewscreennadd.getAdapter()).notifyDataSetChanged();
    }

}
