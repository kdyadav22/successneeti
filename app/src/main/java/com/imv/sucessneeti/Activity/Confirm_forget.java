package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.CustomHint;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.signin.SignIn;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Confirm_forget extends AppCompatActivity {
private EditText cardnumEditotp,cardnumEditpass,cardnumEditpasscon;
private ImageView submit_btn;
private TextView loginbtn,registerbtn;
String mobilenki;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_forget);
        init();
        Intent intent = getIntent();
        if(intent != null){
            mobilenki = intent.getStringExtra("mobileno");
        }
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardnumEditotp.getText().toString().matches("")){
                    cardnumEditotp.setError("Otp");
                }else if(cardnumEditpass.getText().toString().matches("")){
                    cardnumEditpass.setError("Password");
                }else if(cardnumEditpasscon.getText().toString().matches("")){
 cardnumEditpasscon.setError("Confirm Password");
                }
                else{
                    try {
                        jsondatrat();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void init() {
        cardnumEditotp = findViewById(R.id.cardnumEditotp);
        cardnumEditpass = findViewById(R.id.cardnumEditpass);
        cardnumEditpasscon = findViewById(R.id.cardnumEditpasscon);
        cardnumEditotp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditotp.getText().toString().length() <=0 ) {
                    cardnumEditotp.setError("Enter Otp");
                }else if(cardnumEditotp.getText().toString().length() <=2){
                    cardnumEditotp.setError("invalid otp");
                }
                else {
                    cardnumEditotp.setError(null);
                }

            }
        });
        cardnumEditpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpass.getText().toString().length() <=0 ) {
                    cardnumEditpass.setError("Enter Password");
                }else if(cardnumEditpass.getText().toString().length() <=5){
                    cardnumEditpass.setError("Password length minimum 6 digits");
                }
                else {
                    cardnumEditpass.setError(null);
                }
            }
        });
        cardnumEditpasscon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpasscon.getText().toString().length() <=0 ) {
                    cardnumEditpasscon.setError("Enter Password");
                }else if(cardnumEditpasscon.getText().toString().length() <=5){
                    cardnumEditpasscon.setError("Password length minimum 6 digits");
                }
                else {
                    cardnumEditpasscon.setError(null);
                }
            }
        });
        Typeface myTypeface = Typeface.createFromAsset(this.getAssets(),
                "fonts/calibri_bold_italic.ttf");
cardnumEditotp.setTypeface(myTypeface);
cardnumEditpass.setTypeface(myTypeface);
cardnumEditpasscon.setTypeface(myTypeface);
        Typeface newTypeface = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");

        CustomHint customHint = new CustomHint(newTypeface, "OTP", Typeface.BOLD_ITALIC, 40f);
        //        CustomHint customHint = new CustomHint(newTypeface, "Enter some text", Typeface.BOLD_ITALIC);
        //        CustomHint customHint = new CustomHint(newTypeface, "Enter some text", 60f);
        //        CustomHint customHint = new CustomHint("Enter some text", Typeface.BOLD_ITALIC, 60f);
        //        CustomHint customHint = new CustomHint("Enter some text", Typeface.BOLD_ITALIC);
        //        CustomHint customHint = new CustomHint("Enter some text", 60f);
        CustomHint customHint1 = new CustomHint(newTypeface, "Password", Typeface.BOLD_ITALIC, 40f);

        CustomHint customHint2 = new CustomHint(newTypeface, "Confirm Password", Typeface.BOLD_ITALIC, 40f);
        cardnumEditpass.setHint(customHint1);
        cardnumEditotp.setHint(customHint);
        cardnumEditpasscon.setHint(customHint2);
        loginbtn = findViewById(R.id.loginbtn);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Confirm_forget.this, SignIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        registerbtn = findViewById(R.id.registerbtn);
        Typeface myTypefacew = Typeface.createFromAsset(this.getAssets(),
                "fonts/calibri_bold_italic.ttf");
loginbtn.setTypeface(myTypefacew);
registerbtn.setTypeface(myTypefacew);
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Confirm_forget.this, SignUp.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url ="http://new.successneeti.com/api/user/signup";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        JSONObject obj=new JSONObject();
        try {
            JSONObject obj1=new JSONObject();

//
//            "password": "123123",
//                    "conf_password": "123123",
//                    "otp": "3161",
//                    "username": "7982063266"
            obj1.put("password", cardnumEditpass.getText().toString());
            obj1.put("conf_password", cardnumEditpasscon.getText().toString());
            obj1.put("otp",cardnumEditotp.getText().toString());
            obj1.put("username",mobilenki);

            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/user/resetpassword",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {
                        Intent intent = new Intent(Confirm_forget.this, SignUp.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);


                    }
                    else{
                        Toast.makeText(Confirm_forget.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Log.d("VolleyError","Volley"+error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                // headers.put("Authorization", "Bearer"+" "+sharedPreferenceConfig.ReadToken(getString(R.string.token_preference)));
                return headers;
            }
            /*@Override
            public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }

            }*/
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }


        };

        requestQueue.add(jsonObjectRequest);
    }

}
