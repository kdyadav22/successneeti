package com.imv.sucessneeti.Activity.Experience;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.Category;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UpdateExperience extends AppCompatActivity {
    private Spinner spinner,et_funcationarea,et_salary;
    private ArrayList<String> CountryName;
    private ArrayList<String> functarea;
    private ArrayList<String> salaryss;
    private ArrayList<String> idspinej;
    String currentvale,streamval,yearset;
    private EditText jobtitle,companyname,datefirst,datesecond;
    private Button button;

    final Calendar myCalendar = Calendar.getInstance();
    private int mYear, mMonth, mDay, mHour, mMinute;

    private static final String MYFILE ="MyFile";
    String valide;
    int subbb =0;
    SharedPreferences sharedPreferenceswe,sharedPreferences;
    SharedPreferences.Editor editor;
    int functionid,roleid,salaryid;
    String fromdateid,todateid;
    private ArrayList<Category> subjectclass;
    private ArrayList<Category> yearlistmi;
    private ArrayList<Category> salarymain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_experience);
        getSupportActionBar().setTitle("Update Experience ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        subjectclass = new ArrayList<>();
        yearlistmi = new ArrayList<>();
        salarymain = new ArrayList<>();
         sharedPreferences = getSharedPreferences(MYFILE, Context.MODE_PRIVATE);
         valide = sharedPreferences.getString("deletevalp",null);
        spinner = findViewById(R.id.et_industry);
        et_funcationarea = findViewById(R.id.et_ed_stfunpusr);
        et_salary = findViewById(R.id.et_salaryss);

        sharedPreferenceswe = getSharedPreferences("TEMP", MODE_PRIVATE);


//        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel();
//            }
//
//        };
//        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel2();
//            }
//
//        };
        datefirst = findViewById(R.id.datefirst);
        datesecond = findViewById(R.id.datesecond);
        datefirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateExperience.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                datefirst.setText(dayOfMonth +"/"+(monthOfYear + 1)+"/"+ year);
//                                txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        datesecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateExperience.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                datesecond.setText(dayOfMonth +"/"+(monthOfYear + 1)+"/"+ year);
//                                txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                try {
                                    //Dates to compare
                                    String CurrentDate=  datefirst.getText().toString();
                                    String FinalDate=  datesecond.getText().toString();

                                    Date date1;
                                    Date date2;

                                    SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

                                    //Setting dates
                                    date1 = dates.parse(CurrentDate);
                                    date2 = dates.parse(FinalDate);

                                    //Comparing dates
                                    long difference = (date1.getTime() - date2.getTime());
                                    long differenceDates = difference / (24 * 60 * 60 * 1000);

                                    //Convert long to String
                                    int dayDifference = Integer.parseInt(Long.toString(differenceDates));
                                    Log.d("hhhh", String.valueOf(dayDifference));
                                    if(dayDifference<=0){
                                        subbb = 1;
                                        Log.d("yyyyy", String.valueOf(dayDifference));
//                                        datesecond.setText(sdf.format(myCalendar.getTime()));
                                        datesecond.setText(dayOfMonth +"/"+(monthOfYear + 1)+"/"+ year);
                                    }else{
                                        Log.d("mmmmm", String.valueOf(dayDifference));
                                        Toast.makeText(UpdateExperience.this, "enter a valid date", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception exception) {
                                    Log.e("DIDN'T WORK", "exception " + exception);
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        CountryName = new ArrayList<>();
        functarea = new ArrayList<>();
        salaryss = new ArrayList<>();
        idspinej = new ArrayList<>();
        init();
        try {
            addgetData();
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            jsondatrat();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        try{
            yeardatta();

        }catch (Exception e){
            e.printStackTrace();
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //currentvale = (String) parent.getItemAtPosition(position);
//                currentvale = String.valueOf(position+1);
//                Log.d("sdklhsdflsn", "onItemSelected: "+currentvale);
                //currentvale = String.valueOf(spinner.getId());
                //   currentvale= spinner.getSelectedItem().toString();

                if(position == 0){

                }else{

                    currentvale =subjectclass.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_funcationarea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
           //     streamval = String.valueOf(position+1);
                if(position == 0){

                }else{

                    streamval =yearlistmi.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_salary.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  yearset = (String) parent.getItemAtPosition(position);
                if(position == 0){

                }else{

                    yearset =salarymain.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void init() {
        jobtitle = findViewById(R.id.jobtitle);
        companyname = findViewById(R.id.compname);
        jobtitle.setText(sharedPreferences.getString("jobtitle",null));
        companyname.setText(sharedPreferences.getString("companynamet",null));
try {
    functionid = Integer.parseInt(sharedPreferences.getString("industry", null));
   roleid = Integer.parseInt(sharedPreferences.getString("roleid", null));
   salaryid = Integer.parseInt(sharedPreferences.getString("salaryid",null));
    fromdateid = sharedPreferences.getString("frmdate",null);
    todateid = sharedPreferences.getString("todate",null);
if(!fromdateid.isEmpty()){
    datefirst.setText(fromdateid);
}
if(!todateid.isEmpty()){
    datesecond.setText(todateid);
}
}catch (Exception e){
    e.printStackTrace();
}

        button = findViewById(R.id.addeducation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(jobtitle.getText().toString().matches("")||companyname.getText().toString().matches("")||datefirst.getText().toString().matches("")||
                datesecond.getText().toString().matches("")){
                    if(jobtitle.getText().toString().matches("")){
                        jobtitle.setError("Job title is Required");
                    }
                    if(companyname.getText().toString().matches("")){
                        companyname.setError("Company Name is Required");
                    }
                }
                else if(subbb == 0){
                    Toast.makeText(UpdateExperience.this, "Enter a valid Experience", Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        insertdata();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateExperience.this);
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();
 String idd =sharedPreferenceswe.getString("id",null);
            obj1.put("user_id", idd);


            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/master/all",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("industry_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("asjdsajlkd",response.toString());
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            subjectclass.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < subjectclass.size(); i++){
                            CountryName.add(subjectclass.get(i).getName().toString());

                        }
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            CountryName.add(country);
//                            idspinej.add(iddd);
//                        }
                        Log.d("datatatta",functarea.toString());
                        ArrayAdapter arrayAdapter = new ArrayAdapter(UpdateExperience.this,R.layout.simple_spinner_dropdown,CountryName){
                            @Override
                            public boolean isEnabled(int position) {
                                if(position ==0){
                                    return  false;
                                }else{
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position,convertView,parent);
                                TextView tv =(TextView) view;
                                if(position == 0){
                                    tv.setTextColor(Color.GRAY);
                                }else{
                                    tv.setTextColor(Color.BLACK);
                                }
                                return  view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        spinner.setAdapter(arrayAdapter);
                        for(int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == functionid) {
                                spinner.setSelection(pos);
                                break;
                            }
                        }


                        spinner.setId(Integer.valueOf(String.valueOf(idspinej)));
                    }


                    else{
                        Toast.makeText(UpdateExperience.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError","Volley"+error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String appid = sharedPreferenceswe.getString("deviceid",null);
                headers.put("Token",appid);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
    private void addgetData() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateExperience.this);
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();

            obj1.put("education_id", currentvale);


            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/master/all",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("responsemeto",""+response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("functional_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            yearlistmi.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < yearlistmi.size(); i++){
                            functarea.add(yearlistmi.get(i).getName().toString());

                        }

//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            functarea.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(UpdateExperience.this,R.layout.simple_spinner_dropdown,functarea){
                            @Override
                            public boolean isEnabled(int position) {
                                if(position ==0){
                                    return  false;
                                }else{
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position,convertView,parent);
                                TextView tv =(TextView) view;
                                if(position == 0){
                                    tv.setTextColor(Color.GRAY);
                                }else{
                                    tv.setTextColor(Color.BLACK);
                                }
                                return  view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_funcationarea.setAdapter(arrayAdapter);
                        for(int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == roleid) {
                                et_funcationarea.setSelection(pos);
                                break;
                            }
                        }

                        et_funcationarea.setId(Integer.valueOf(String.valueOf(idspinej)));
                    }


                    else{
                        Toast.makeText(UpdateExperience.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError","Volley"+error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String appidd = sharedPreferenceswe.getString("deviceid",null);
                headers.put("Token",appidd);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
//year_list
    //name

    private void yeardatta() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateExperience.this);
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();
String idd = sharedPreferenceswe.getString("id",null);
            obj1.put("user_id", idd);


            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/master/all",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("salary");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            salarymain.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < salarymain.size(); i++){
                            salaryss.add(salarymain.get(i).getName().toString());

                        }
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            salaryss.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(UpdateExperience.this,R.layout.simple_spinner_dropdown,salaryss){
                            @Override
                            public boolean isEnabled(int position) {
                                if(position ==0){
                                    return  false;
                                }else{
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position,convertView,parent);
                                TextView tv =(TextView) view;
                                if(position == 0){
                                    tv.setTextColor(Color.GRAY);
                                }else{
                                    tv.setTextColor(Color.BLACK);
                                }
                                return  view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_salary.setAdapter(arrayAdapter);
                        for(int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == salaryid) {
                                et_salary.setSelection(pos);
                                break;
                            }
                        }

                        et_salary.setId(Integer.valueOf(String.valueOf(idspinej)));
                    }


                    else{
                        Toast.makeText(UpdateExperience.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError","Volley"+error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");
String apinh = sharedPreferenceswe.getString("deviceid",null);

                headers.put("Token",apinh);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void insertdata() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            //  String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
String ijh = sharedPreferenceswe.getString("id",null);
            JSONObject obj1=new JSONObject();
            obj1.put("id",valide);
            obj1.put("user_id", ijh);
            obj1.put("function_id",currentvale );
            obj1.put("industries_id", streamval);
            obj1.put("job_title",jobtitle.getText().toString());
            obj1.put("company_name",companyname.getText().toString());
            obj1.put("from_date",datefirst.getText().toString());
            obj1.put("to_date",datesecond.getText().toString());
            obj1.put("experience_year",yearset);
            obj1.put("experience_month","null");
            obj1.put("salary","null");
            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/user/updateexperience",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    progressDialog.dismiss();
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {
                        Intent intent = new Intent(UpdateExperience.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb","5");
                        startActivity(intent);


                    }
                    else{
                        Toast.makeText(UpdateExperience.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(UpdateExperience.this, message, Toast.LENGTH_SHORT).show();
                }

            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
String apiuyt = sharedPreferenceswe.getString("deviceid",null);

                headers.put("Token", apiuyt);


                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updateLabel() {
        String myFormat = "DD/MM/YYYY"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        datefirst.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabel2() {
        String myFormat = "DD/MM/YYYY"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        datesecond.setText(sdf.format(myCalendar.getTime()));
        try {
            //Dates to compare
            String CurrentDate=  datefirst.getText().toString();
            String FinalDate=  datesecond.getText().toString();

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = (date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            int dayDifference = Integer.parseInt(Long.toString(differenceDates));
            Log.d("hhhh", String.valueOf(dayDifference));
            if(dayDifference>=0){
                subbb = 1;
                Log.d("yyyyy", String.valueOf(dayDifference));
                datesecond.setText(sdf.format(myCalendar.getTime()));

            }else{
                Log.d("mmmmm", String.valueOf(dayDifference));
                Toast.makeText(this, "enter a valid date", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
