package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.personal.UpdatePersonal;
import com.imv.sucessneeti.Extra.Category;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Update_Education_Deta extends AppCompatActivity {
    private Spinner spinner, et_ed_streamspusr, et_yearpass;
    private ArrayList<String> CountryName;
    private ArrayList<String> streamlist;
    private ArrayList<String> yearlist;
    private ArrayList<String> idspinej;
    String currentvale = "", streamval = "", yearset = "";
    private EditText boardname, subjectmarks, obtaibmarks, percentagef;
    private Button button;
    private static final String MYFILE = "MyFile";
    String valide;
    SharedPreferences sharedPreferenceswe, sharedPreferences;
    SharedPreferences.Editor editor;
    private ArrayList<Category> distrArrL;
    private ArrayList<Category> subjectclass;
    private ArrayList<Category> yearlistmi;
    int educationid, streamid, yearid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update__education__deta);
        getSupportActionBar().setTitle("Update Education ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        distrArrL = new ArrayList<>();
        subjectclass = new ArrayList<>();
        yearlistmi = new ArrayList<>();

        spinner = findViewById(R.id.et_ed_streamsp);
        sharedPreferenceswe = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        sharedPreferences = getSharedPreferences(MYFILE, Context.MODE_PRIVATE);
        valide = sharedPreferences.getString("deleteval", null);
        et_ed_streamspusr = findViewById(R.id.et_ed_streamspusr);
        et_yearpass = findViewById(R.id.et_yearpass);
        CountryName = new ArrayList<>();
        streamlist = new ArrayList<>();
        yearlist = new ArrayList<>();
        idspinej = new ArrayList<>();
        init();
        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            yeardatta();

        } catch (Exception e) {
            e.printStackTrace();
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    currentvale = subjectclass.get(position).getId();
                    Log.d("sdklhsdflsn", "onItemSelected: " + currentvale);
                    //currentvale = String.valueOf(spinner.getId());
                    //   currentvale= spinner.getSelectedItem().toString();
                    streamlist.clear();
                    distrArrL.clear();
                    try {
                        addgetData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //currentvale = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_ed_streamspusr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {

                    streamval = distrArrL.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_yearpass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {


                    yearset = yearlistmi.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void init() {
        boardname = findViewById(R.id.boardname);

        try {
            int classsid = Integer.parseInt(sharedPreferences.getString("selectclas", null));
            int streamidm = Integer.parseInt(sharedPreferences.getString("streamid", null));
            int yearidm = Integer.parseInt(sharedPreferences.getString("yeardata", null));
            if (classsid != 0) {
                educationid = classsid;
            }
            if (streamidm != 0) {
                streamid = streamidm - 1;
            }
            if (yearidm != 0) {
                yearid = yearidm;
            }
            boardname.setText(sharedPreferences.getString("boardnamey", null));

            subjectmarks = findViewById(R.id.subjectmarks);
            subjectmarks.setText(sharedPreferences.getString("subjectmy", null));
            obtaibmarks = findViewById(R.id.obtaibmarks);
            obtaibmarks.setText(sharedPreferences.getString("obtain_may", null));
            percentagef = findViewById(R.id.percentagef);
            percentagef.setText(sharedPreferences.getString("percenty", null));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        button = findViewById(R.id.addeducation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boardname.getText().toString().matches("") || subjectmarks.getText().toString().matches("") || obtaibmarks.getText().toString().matches("") ||
                        percentagef.getText().toString().matches("")) {
                    if (boardname.getText().toString().matches("")) {
                        boardname.setError("Board/University Name is Required");
                    }
                    if (subjectmarks.getText().toString().matches("")) {
                        subjectmarks.setError("Subject Marks is Required");
                    }
                    if (obtaibmarks.getText().toString().matches("")) {
                        obtaibmarks.setError("Obtain Marks is Required");
                    }
                    if (percentagef.getText().toString().matches("")) {
                        percentagef.setError("Percentage is Required");
                    }
                } else {
                    if (currentvale.matches("")) {
                        Toast.makeText(Update_Education_Deta.this, "Please Select Class", Toast.LENGTH_SHORT).show();
                    } else if (streamval.matches("")) {
                        Toast.makeText(Update_Education_Deta.this, "Please select Stream", Toast.LENGTH_SHORT).show();
                    } else if (yearset.matches("")) {
                        Toast.makeText(Update_Education_Deta.this, "Please Select Passing Out Year", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            insertdata();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(Update_Education_Deta.this);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String iddd = sharedPreferenceswe.getString("id", null);
            obj1.put("user_id", iddd);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("education_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

//ring country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            CountryName.add(country);
//                            idspinej.add(iddd);


                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            subjectclass.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < subjectclass.size(); i++) {
                            CountryName.add(subjectclass.get(i).getName().toString());

                        }
//
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            distlist.add(country);


                        ArrayAdapter arrayAdapter = new ArrayAdapter(Update_Education_Deta.this, R.layout.simple_spinner_dropdown, CountryName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        spinner.setAdapter(arrayAdapter);
                        for (int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == educationid) {
                                spinner.setSelection(pos);
                                break;
                            }
                        }

                    } else {
                        Toast.makeText(Update_Education_Deta.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void addgetData() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(Update_Education_Deta.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("education_id", currentvale);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/streamlist", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("responsemeto", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        //      JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = response.getJSONArray("result");


                        for (int i = 0; i < jsonArray.length(); i++) {

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            distrArrL.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < distrArrL.size(); i++) {
                            streamlist.add(distrArrL.get(i).getName().toString());

                        }
//
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            distlist.add(country);
//                        }


                        ArrayAdapter arrayAdapter = new ArrayAdapter(Update_Education_Deta.this, R.layout.simple_spinner_dropdown, streamlist) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_ed_streamspusr.setAdapter(arrayAdapter);
                        for (int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == streamid) {
                                et_ed_streamspusr.setSelection(pos);
                                break;
                            }
                        }

                    } else {
                        Toast.makeText(Update_Education_Deta.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiyt = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void yeardatta() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(Update_Education_Deta.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String iff = sharedPreferenceswe.getString("id", null);
            obj1.put("user_id", iff);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("year_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            yearlistmi.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < yearlistmi.size(); i++) {
                            yearlist.add(yearlistmi.get(i).getName().toString());

                        }
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            yearlist.add(country);
//                            idspinej.add(iddd);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(Update_Education_Deta.this, R.layout.simple_spinner_dropdown, yearlist) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_yearpass.setAdapter(arrayAdapter);
                        for (int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == yearid) {
                                et_yearpass.setSelection(pos);
                                break;
                            }
                        }

                        et_yearpass.setId(Integer.valueOf(String.valueOf(idspinej)));
                    } else {
                        Toast.makeText(Update_Education_Deta.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyte = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiuyte);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void insertdata() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            //  String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String suyt = sharedPreferenceswe.getString("id", null);
            obj1.put("id", valide);
            obj1.put("user_id", suyt);
            obj1.put("education_id", currentvale);
            obj1.put("stream_id", streamval);
            obj1.put("board_name", boardname.getText().toString());
            obj1.put("subject_marks", subjectmarks.getText().toString());
            obj1.put("obtain_marks", obtaibmarks.getText().toString());
            obj1.put("percentage", percentagef.getText().toString());
            obj1.put("passing_year", yearset);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/updateeducation", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(Update_Education_Deta.this, "Successfully Update", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Update_Education_Deta.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb", "4");
                        startActivity(intent);


                    } else {
                        Toast.makeText(Update_Education_Deta.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),
                        "error" + error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            /*if (error.networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to save. Please try again.", Toast.LENGTH_LONG).show();
                }
            }*/

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");

                String apiuytq = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiuytq);


                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
