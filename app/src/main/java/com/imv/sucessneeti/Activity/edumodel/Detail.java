
package com.imv.sucessneeti.Activity.edumodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("subject_marks")
    @Expose
    private Integer subjectMarks;
    @SerializedName("obtain_marks")
    @Expose
    private Integer obtainMarks;

    private boolean isDeleted;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getSubjectMarks() {
        return subjectMarks;
    }

    public void setSubjectMarks(Integer subjectMarks) {
        this.subjectMarks = subjectMarks;
    }

    public Integer getObtainMarks() {
        return obtainMarks;
    }

    public void setObtainMarks(Integer obtainMarks) {
        this.obtainMarks = obtainMarks;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
