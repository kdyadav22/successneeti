package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.signin.SignIn;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OtpScren extends AppCompatActivity {
    EditText et1, et2, et3, et4;
    public String message, success;
    String fin;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String statusff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_scren);
        Intent intent = getIntent();
        statusff = intent.getStringExtra("mobile");
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        getInti();

    }

    private void getInti() {
        et1 = findViewById(R.id.otp1);
        et2 = findViewById(R.id.otp2);
        et3 = findViewById(R.id.otp3);
        et4 = findViewById(R.id.otp4);
        TextView resendotp = findViewById(R.id.resendotp);

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et2.requestFocus();
                } else if (s.length() == 0) {
                    et1.clearFocus();
                }
            }
        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et3.requestFocus();
                } else if (s.length() == 0) {
                    et1.requestFocus();
                }
            }
        });

        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et4.requestFocus();
                } else if (s.length() == 0) {
                    et2.requestFocus();
                }
            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et4.clearFocus();
                } else if (s.length() == 0) {
                    et3.requestFocus();
                }
            }
        });


        ImageView verfifyy = findViewById(R.id.submit_btn);

        verfifyy.setOnClickListener(v -> {
            if (et1.getText().toString().matches("") || et2.getText().toString().matches("") || et3.getText().toString().matches("")
                    || et4.getText().toString().matches("")) {
                Toast.makeText(OtpScren.this, "Please fill first", Toast.LENGTH_SHORT).show();
            } else {
                matchverify();
            }
        });

        resendotp.setOnClickListener(view -> {
            try {
                resendotpmeh();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void resendotpmeh() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        JSONObject obj = new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("mobile", statusff);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/resentotp", new JSONObject(requestBody), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (!ss.matches("0")) {
                        Toast.makeText(OtpScren.this, response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, error -> {
            progressDialog.dismiss();
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ServerError) {
                message = "The server could not be found. Please try again after some time!!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ParseError) {
                message = "Parsing error! Please try again after some time!!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection.";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        requestQueue.add(jsonObjectRequest);
    }

    private void matchverify() {
        try {
            registerUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerUser() throws JSONException {
        final String on1 = et1.getText().toString();
        final String on2 = et2.getText().toString();
        final String on3 = et3.getText().toString();
        final String on4 = et4.getText().toString();

        fin = on1 + on2 + on3 + on4;

        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        JSONObject obj = new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("mobile", statusff);
            obj1.put("otp", fin);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/verifymobile", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        editor.putBoolean("LOGGEDIN", true);
                        editor.apply();
                        Intent intent = new Intent(OtpScren.this, SignIn.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(OtpScren.this, response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, error -> {
            progressDialog.dismiss();
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ServerError) {
                message = "The server could not be found. Please try again after some time!!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof ParseError) {
                message = "Parsing error! Please try again after some time!!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection.";
                Toast.makeText(OtpScren.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                // headers.put("Authorization", "Bearer"+" "+sharedPreferenceConfig.ReadToken(getString(R.string.token_preference)));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

}
