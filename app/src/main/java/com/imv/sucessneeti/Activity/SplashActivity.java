package com.imv.sucessneeti.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.signin.SignIn;

public class SplashActivity extends AppCompatActivity {
    private boolean mIsBackButtonPressed;
    private static final int SPLASH_DURATION = 2000;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences("TEMP",MODE_PRIVATE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (!mIsBackButtonPressed) {
                    if (!sharedPreferences.getBoolean("LOGGEDIN",false)){
                        Intent intent = new Intent(SplashActivity.this, SignIn.class);
                        startActivity(intent);
                        finish();
                    }else {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    }
                }
            }

        }, SPLASH_DURATION);
    }

    @Override
    public void onBackPressed() {
        // set the flag to true so the next activity won't start up
        mIsBackButtonPressed = true;
        super.onBackPressed();

    }
}
