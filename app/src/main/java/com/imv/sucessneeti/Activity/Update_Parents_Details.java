package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Update_Parents_Details extends AppCompatActivity {
Button btn;
private EditText fathermname,fatheroccupdate,mothername,motheroccupdate,parenteditmobile;
SharedPreferences sharedPreferences;
SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update__parents__details);
        getSupportActionBar().setTitle("Update Parents Details ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        btn = findViewById(R.id.updateparentdetails);
        init();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fathermname.getText().toString().matches("")||fatheroccupdate.getText().toString().matches("")
                ||mothername.getText().toString().matches("")||motheroccupdate.getText().toString().matches("")||
                parenteditmobile.getText().toString().matches("")){
                    if(fathermname.getText().toString().matches("")){
                        fathermname.setError("Father Name is Required");
                    }
                    if(fatheroccupdate.getText().toString().matches("")){
                        fatheroccupdate.setError("This field is Required");
                    }
                    if(mothername.getText().toString().matches("")){
                        mothername.setError("Mother Name is Required");
                    }
                    if(motheroccupdate.getText().toString().matches("")){
                        motheroccupdate.setError("This field is Required");
                    }
                    if(parenteditmobile.getText().toString().matches("")){
                        parenteditmobile.setError("This field is Required");
                    }
                    Toast.makeText(Update_Parents_Details.this, "Please fill all details", Toast.LENGTH_SHORT).show();
                }
                else if(parenteditmobile.getText().toString().length() != 10){
                    Toast.makeText(Update_Parents_Details.this, "Invalid no", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        jsondatrat();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private void init() {
        fathermname = findViewById(R.id.fathereditupdate);
        fatheroccupdate = findViewById(R.id.fatheroccupdate);
        mothername = findViewById(R.id.mothernameupdate);
        motheroccupdate = findViewById(R.id.motheroccupdate);
        parenteditmobile = findViewById(R.id.parenteditmobile);
        fathermname.setText(sharedPreferences.getString("father_name",null));
        fatheroccupdate.setText(sharedPreferences.getString("father_occupation",null));

       mothername.setText(sharedPreferences.getString("mother_name",null));
      motheroccupdate.setText(sharedPreferences.getString("mother_occupation",null));
        parenteditmobile.setText(sharedPreferences.getString("parent_mobile",null));

    }

    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();
String jsss= sharedPreferences.getString("id",null);
            obj1.put("user_id", jsss);
            obj1.put("father_name",fathermname.getText().toString());
            obj1.put("father_occupation",fatheroccupdate.getText().toString());
            obj1.put("mother_name",mothername.getText().toString());

            obj1.put("mother_occupation",motheroccupdate.getText().toString());
            obj1.put("parent_mobile",parenteditmobile.getText().toString());
            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/user/updateProfile",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {
                        Toast.makeText(Update_Parents_Details.this, "Details Update Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Update_Parents_Details.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("valueb","3");
                        startActivity(intent);


                    }
                    else{
                        Toast.makeText(Update_Parents_Details.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),
                        "error"+error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError","Volley"+error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid",null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}


