package com.imv.sucessneeti.Activity.Documents;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    @Multipart
    @POST("uploadpdf/addpdf")
    Call<ReferData> processRefer(@Part("user_id") RequestBody user_id,
                                 @Part("name") RequestBody last_name,
                                 @Part MultipartBody.Part file
    );
}
