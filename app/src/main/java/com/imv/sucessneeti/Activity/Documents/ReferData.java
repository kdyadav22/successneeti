package com.imv.sucessneeti.Activity.Documents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferData {
    @SerializedName("host_code")
    @Expose
    private Integer host_code;

    @SerializedName("host_description")
    @Expose
    private String host_description;

    public Integer getHost_code() {
        return host_code;
    }

    public void setHost_code(Integer host_code) {
        this.host_code = host_code;
    }

    public String getHost_description() {
        return host_description;
    }

    public void setHost_description(String host_description) {
        this.host_description = host_description;
    }
}
