package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;
import com.imv.sucessneeti.signin.SignIn;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Forget_pass extends AppCompatActivity {
    private EditText cardnumEditmobile;
    LinearLayout svParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        inir();
        ImageView getgg = findViewById(R.id.submit_btn);
        svParent = findViewById(R.id.svParent);

        getgg.setOnClickListener(v -> {
            if (cardnumEditmobile.getText().toString().matches("")) {
                cardnumEditmobile.setError("Mobile no is required");
            } else if (cardnumEditmobile.getText().toString().length() != 10) {
                Toast.makeText(Forget_pass.this, "Invalid mobile no", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    jsondatrat();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Utilities.hideKeyboard(svParent, Forget_pass.this);
    }

    private void inir() {
        cardnumEditmobile = findViewById(R.id.cardnumEditmobile);
        cardnumEditmobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditmobile.getText().toString().length() <= 0) {
                    cardnumEditmobile.setError("Enter mobile no.");
                } else if (cardnumEditmobile.getText().toString().length() != 10) {
                    cardnumEditmobile.setError("Mobile No. Length should be 10 digits");
                } else {
                    cardnumEditmobile.setError(null);
                }
            }
        });

        TextView submit_btn = findViewById(R.id.register);
        TextView login_btn = findViewById(R.id.loginbtn);

        submit_btn.setOnClickListener(v -> {
            Intent intent = new Intent(Forget_pass.this, SignUp.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        login_btn.setOnClickListener(v -> {
            Intent intent = new Intent(Forget_pass.this, SignIn.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        JSONObject obj = new JSONObject();
        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            obj1.put("username", cardnumEditmobile.getText().toString());
            obj1.put("device_id", deviceAppUID);

            obj.put("transaction_data", obj1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = obj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/forgotpassword", new JSONObject(requestBody), response -> {
            progressDialog.dismiss();
            Log.d("response", "" + response);
            try {
                String ss = response.getString("host_code");
                if (ss.matches("0")) {
                    Intent intent = new Intent(Forget_pass.this, Confirm_forget.class);
                    intent.putExtra("mobileno", cardnumEditmobile.getText().toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(Forget_pass.this, "No response. Please try again.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> {
            progressDialog.dismiss();
            Log.d("respsd", error.toString());
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

}
