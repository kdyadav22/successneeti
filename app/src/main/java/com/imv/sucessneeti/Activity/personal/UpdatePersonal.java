package com.imv.sucessneeti.Activity.personal;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.EmailVerify;
import com.imv.sucessneeti.Extra.Category;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UpdatePersonal extends AppCompatActivity {
    private TextView mobileno;
    private Spinner et_school, et_free_plan, et_center, et_sp_national, et_sp_category,
            et_sp_cast, et_marital_stat, et_sp_blodd, et_sp_state, et_sp_district;
    private ArrayList<String> schoolarray;
    private ArrayList<String> freeplan;
    private ArrayList<String> centername;
    private ArrayList<String> national;
    private ArrayList<String> castest;
    private ArrayList<String> sp_cast;
    private ArrayList<String> martisla;
    private ArrayList<String> blodded;
    private ArrayList<String> statelis;
    private ArrayList<String> distlist;
    String statedata, schoolData, feePlanData, centerNamedRA, nationalDarta, categData, castDatat, DistDAtab, bloddData;
    private EditText first_name, last_name, email_user, aadhdar_user, dateofbirth,
            et_disability, et_height, et_weight, et_chest, et_body_iddent, et_body_iddent_sec,
            et_police, et_pincode, et_address;
    private RadioGroup radiogrup;
    final Calendar myCalendar = Calendar.getInstance();
    private Button updateperso;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private RadioButton radioButton;
    private ArrayList<Category> goodModelArrayList;
    private ArrayList<Category> schoolArrayCat;
    private ArrayList<Category> basiAarrap;
    private ArrayList<Category> centErArr;
    private ArrayList<Category> natIonArra;
    private ArrayList<Category> cateArrayl;
    private ArrayList<Category> castArrLis;
    private ArrayList<Category> bloodArrL;
    private ArrayList<Category> distrArrL;
    String dataofdender;
    String maritalid = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    int schoolid, centerid, nationalid, categoryidm, castidm, martitalid, bloodgrm, stateidmm, districid;
    int schooltypeset = 0, centernameset = 0, nationalset = 0, cateogoryset = 0, castset = 0, maritalset = 0, bloodgrupset = 0, stateset = 0, districtset = 0;

    private DateFormat resDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    private DateFormat disDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_personal);
        getSupportActionBar().setTitle("Update Basic Info ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();
        radiogrup = findViewById(R.id.radiogrup);
        goodModelArrayList = new ArrayList<>();
        schoolArrayCat = new ArrayList<>();
        basiAarrap = new ArrayList<>();
        centErArr = new ArrayList<>();
        natIonArra = new ArrayList<>();
        cateArrayl = new ArrayList<>();
        castArrLis = new ArrayList<>();
        bloodArrL = new ArrayList<>();
        distrArrL = new ArrayList<>();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        dateofbirth = findViewById(R.id.dateofbirth);
        dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new DatePickerDialog(UpdatePersonal.this, date, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdatePersonal.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String dateString = (monthOfYear < 9 ? "0" + monthOfYear : "" + monthOfYear) + "/" + (dayOfMonth < 9 ? "0" + dayOfMonth : "" + dayOfMonth) + "/" + year;

                                dateofbirth.setText(Utilities.formattedDateString(resDateFormat, disDateFormat, dateString));
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        schoolarray = new ArrayList<>();
        freeplan = new ArrayList<>();
        centername = new ArrayList<>();
        national = new ArrayList<>();
        castest = new ArrayList<>();
        sp_cast = new ArrayList<>();
        martisla = new ArrayList<>();
        blodded = new ArrayList<>();
        statelis = new ArrayList<>();
        distlist = new ArrayList<>();
        initreg();
        spinnerarr();
        initedit();
        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        et_school.post(new Runnable() {
//            @Override
//            public void run() {
//                et_school.setSelection(2);
//            }
//        });
    }

    private void initedit() {
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        email_user = findViewById(R.id.email_user);
        aadhdar_user = findViewById(R.id.aadhdar_user);

        et_disability = findViewById(R.id.et_disability);
        et_height = findViewById(R.id.et_height);
        et_weight = findViewById(R.id.et_weight);
        et_chest = findViewById(R.id.et_chest);
        et_body_iddent = findViewById(R.id.et_body_iddent);
        et_body_iddent_sec = findViewById(R.id.et_body_iddent_sec);
        et_pincode = findViewById(R.id.et_pincode);
        et_police = findViewById(R.id.et_police);
        et_address = findViewById(R.id.et_address);
        updateperso = findViewById(R.id.updateperso);


        try {
            int nationalidv = Integer.parseInt(sharedPreferences.getString("nationalid", null));
            int categoryidmv = Integer.parseInt(sharedPreferences.getString("categid", null));
            int castidmv = Integer.parseInt(sharedPreferences.getString("castid", null));
            int centeridv = Integer.parseInt(sharedPreferences.getString("centerid", null));
            int schoolidv = Integer.parseInt(sharedPreferences.getString("schoolid", null));
            int bloodgrmv = Integer.parseInt(sharedPreferences.getString("bloodid", null));
            int stateidmmv = Integer.parseInt(sharedPreferences.getString("stateidm", null));
            int districidv = Integer.parseInt(sharedPreferences.getString("districid", null));
            int martitalidv = Integer.parseInt(sharedPreferences.getString("maritalid", null));


            if (nationalidv != 0) {
                nationalid = nationalidv;
            }
            if (categoryidmv != 0) {
                categoryidm = categoryidmv;
            }
            if (castidmv != 0) {
                castidm = castidmv;
            }
            if (centeridv != 0) {
                centerid = centeridv;
            }
            if (schoolidv != 0) {
                schoolid = schoolidv;
            }
            if (bloodgrmv != 0) {
                bloodgrm = bloodgrmv;
            }
            if (stateidmmv != 0) {
                stateidmm = stateidmmv;
            }
            if (districidv != 0) {
                districid = districidv;
            }
            if (martitalidv != 0) {
                martitalid = martitalidv;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String namef = sharedPreferences.getString("firstname", null);
        String lanam = sharedPreferences.getString("lastname", null);
        String emast = sharedPreferences.getString("email", null);
        String aadhamo = sharedPreferences.getString("aadharno", null);
        String heist = sharedPreferences.getString("height", null);
        String weist = sharedPreferences.getString("weight", null);
        String pistc = sharedPreferences.getString("pincode", null);
        String aadrs = sharedPreferences.getString("aadress", null);
        String datebb = sharedPreferences.getString("dateofb", null);
        String disablity = sharedPreferences.getString("disabili", null);
        String chest = sharedPreferences.getString("chestf", null);
        String polics = sharedPreferences.getString("policesta", null);

        String bodyiden1 = sharedPreferences.getString("bodyident1", null);
        String bodyiden2 = sharedPreferences.getString("bodyident2", null);
        if (namef != null) {
            first_name.setText(namef);
        }
        if (lanam != null) {
            last_name.setText(lanam);
        }
        if (emast != null) {
            email_user.setText(emast);
        }
        if (aadhamo != null) {
            aadhdar_user.setText(aadhamo);
        }
        if (heist != null) {
            et_height.setText(heist);
        }
        if (weist != null) {
            et_weight.setText(weist);
        }
        if (pistc != null) {
            et_pincode.setText(pistc);
        }
        if (aadrs != null) {
            et_address.setText(aadrs);
        }
        if (datebb != null) {
            dateofbirth.setText(datebb);
        }
        if (disablity != null) {
            et_disability.setText(disablity);
        }
        if (chest != null) {
            et_chest.setText(chest);
        }
        if (polics != null) {
            et_police.setText(polics);
        }
        if (bodyiden1 != null) {
            et_body_iddent.setText(bodyiden1);
        }
        if (bodyiden2 != null) {
            et_body_iddent_sec.setText(bodyiden2);
        }
        mobileno.setText(sharedPreferences.getString("mobile", null));
        updateperso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (first_name.getText().toString().matches("") || last_name.getText().toString().matches("") || email_user.getText().toString().matches("") ||
                        et_disability.getText().toString().matches("") ||
                        et_height.getText().toString().matches("") || et_pincode.getText().toString().matches("") ||
                        et_police.getText().toString().matches("") || et_address.getText().toString().matches("")) {
                    if (first_name.getText().toString().matches("")) {
                        first_name.setError("Name is Required");
                    }
                    if (last_name.getText().toString().matches("")) {
                        last_name.setError("Last Name is Required");
                    }
                    if (email_user.getText().toString().matches("")) {
                        email_user.setError("Email is Required");
                    }
                    if (et_disability.getText().toString().matches("")) {
                        et_disability.setError("Disability is Required");
                    }
                    if (et_height.getText().toString().matches("")) {
                        et_height.setError("Height is required");
                    }
                    if (et_pincode.getText().toString().matches("")) {
                        et_pincode.setError("Pincode is Required");
                    }
                    if (et_police.getText().toString().matches("")) {
                        et_police.setError("Police Station is Required");
                    }
                    if (et_address.getText().toString().matches("")) {
                        et_address.setError("Address is Required");
                    }
                } else if (et_pincode.getText().toString().length() != 6) {
                    Toast.makeText(UpdatePersonal.this, "Invalid pincode", Toast.LENGTH_SHORT).show();
                } else if (aadhdar_user.getText().toString().length() != 12) {
                    Toast.makeText(UpdatePersonal.this, "Wrong Aadhaar No.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        updateprof();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void spinnerarr() {
        et_sp_national = findViewById(R.id.et_sp_national);
        et_sp_national.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                nationalDarta = natIonArra.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_sp_category = findViewById(R.id.et_sp_category);
        et_sp_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                categData = cateArrayl.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_sp_cast = findViewById(R.id.et_sp_cast);
        et_sp_cast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                castDatat = castArrLis.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_marital_stat = findViewById(R.id.et_marital_stat);
        et_marital_stat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maritalid = String.valueOf(position + 1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_sp_blodd = findViewById(R.id.et_sp_blodd);
        et_sp_blodd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bloddData = basiAarrap.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_sp_state = findViewById(R.id.et_sp_state);
        et_sp_district = findViewById(R.id.et_sp_district);
        et_sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DistDAtab = distrArrL.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //currentvale = (String) parent.getItemAtPosition(position);
                statedata = goodModelArrayList.get(position).getId();

                Log.d("sdklhsdflsn", "onItemSelected: " + statedata);
                //currentvale = String.valueOf(spinner.getId());
                //   currentvale= spinner.getSelectedItem().toString();
                distlist.clear();
                distrArrL.clear();
                try {
                    addgetData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initreg() {
        mobileno = findViewById(R.id.mobileno);
        et_school = findViewById(R.id.et_school);
        et_school.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //  et_school.setSelection(schoolArrayCat.get(position).getName());
                schoolData = schoolArrayCat.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_free_plan = findViewById(R.id.et_free_plan);
        et_free_plan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                feePlanData = basiAarrap.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        et_center = findViewById(R.id.et_center);
        et_center.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                centerNamedRA = centErArr.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(UpdatePersonal.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("user_id", "1");


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("school_list");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            //JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            schoolArrayCat.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < schoolArrayCat.size(); i++) {
                            schoolarray.add(schoolArrayCat.get(i).getName().toString());
                        }
//
//                            String country = jsonObject1.getString("name");
//                            String iddd = jsonObject1.getString("id");
//                            schoolarray.add(country);
//                          }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, schoolarray) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_school.setAdapter(arrayAdapter);
                        for (int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == schoolid) {
                                et_school.setSelection(pos);
                                break;
                            }
                        }

                        JSONArray jsonArray1 = jsonObject.getJSONArray("plan_list");
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray1.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            basiAarrap.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < basiAarrap.size(); i++) {
                            freeplan.add(basiAarrap.get(i).getName().toString());
                        }

//                            JSONObject jsonObject2 = jsonArray1.getJSONObject(i);
//
//
//                            String country = jsonObject2.getString("name");
//                            String iddd = jsonObject2.getString("id");
//                            freeplan.add(country);
//                        }
                        ArrayAdapter arrayAdapter2 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, freeplan) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_free_plan.setAdapter(arrayAdapter2);


                        JSONArray jsonArray2 = jsonObject.getJSONArray("center_list");
                        for (int i = 0; i < jsonArray2.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray2.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            centErArr.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < centErArr.size(); i++) {
                            centername.add(centErArr.get(i).getName().toString());
                        }

//                            JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
//
//
//                            String country = jsonObject3.getString("name");
//                            String iddd = jsonObject3.getString("id");
//                            centername.add(country);
//                        }
                        ArrayAdapter arrayAdapter3 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, centername) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_center.setAdapter(arrayAdapter3);
                        for (int pos = arrayAdapter3.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter3.getItemId(pos) == centerid) {
                                et_center.setSelection(pos);
                                break;
                            }
                        }

                        JSONArray jsonArray3 = jsonObject.getJSONArray("nationality");
                        for (int i = 0; i < jsonArray3.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray3.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            natIonArra.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < natIonArra.size(); i++) {
                            national.add(natIonArra.get(i).getName().toString());
                        }

//                            JSONObject jsonObject4 = jsonArray3.getJSONObject(i);
//
//
//                            String country = jsonObject4.getString("name");
//                            String iddd = jsonObject4.getString("id");
//                            national.add(country);
//                        }
                        ArrayAdapter arrayAdapter4 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, national) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_national.setAdapter(arrayAdapter4);
                        for (int pos = arrayAdapter4.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter4.getItemId(pos) == nationalid) {
                                et_sp_national.setSelection(pos);
                                break;
                            }
                        }

                        JSONArray jsonArray4 = jsonObject.getJSONArray("category_list");
                        for (int i = 0; i < jsonArray4.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray4.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            cateArrayl.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < cateArrayl.size(); i++) {
                            castest.add(cateArrayl.get(i).getName().toString());

                        }
//                            JSONObject jsonObject5 = jsonArray4.getJSONObject(i);
//
//
//                            String country = jsonObject5.getString("name");
//                            String iddd = jsonObject5.getString("id");
//                            castest.add(country);
//                        }
                        ArrayAdapter arrayAdapter5 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, castest) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_category.setAdapter(arrayAdapter5);
                        for (int pos = arrayAdapter5.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter5.getItemId(pos) == categoryidm) {
                                et_sp_category.setSelection(pos);
                                break;
                            }
                        }

                        JSONArray jsonArray5 = jsonObject.getJSONArray("cast_list");
                        for (int i = 0; i < jsonArray5.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray5.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            castArrLis.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < castArrLis.size(); i++) {
                            sp_cast.add(castArrLis.get(i).getName().toString());

                        }

                        //                            JSONObject jsonObject6 = jsonArray5.getJSONObject(i);
//
//
//                            String country = jsonObject6.getString("name");
//                            String iddd = jsonObject6.getString("id");
//                            sp_cast.add(country);
//                        }
                        ArrayAdapter arrayAdapter6 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, sp_cast) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_cast.setAdapter(arrayAdapter6);
                        for (int pos = arrayAdapter6.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter6.getItemId(pos) == castidm - 1) {
                                et_sp_cast.setSelection(pos);
                                break;
                            }
                        }


                        JSONArray jsonArray6 = jsonObject.getJSONArray("marital_status");
                        for (int i = 0; i < jsonArray6.length(); i++) {
                            JSONObject jsonObject7 = jsonArray6.getJSONObject(i);


                            String country = jsonObject7.getString("name");
                            String iddd = jsonObject7.getString("id");
                            martisla.add(country);
                        }
                        ArrayAdapter arrayAdapter7 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, martisla);
                        arrayAdapter.notifyDataSetChanged();
                        et_marital_stat.setAdapter(arrayAdapter7);

                        JSONArray jsonArray7 = jsonObject.getJSONArray("blood_group");
                        for (int i = 0; i < jsonArray7.length(); i++) {
                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray7.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            bloodArrL.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < bloodArrL.size(); i++) {
                            blodded.add(bloodArrL.get(i).getName().toString());

                        }

//                            JSONObject jsonObject8 = jsonArray7.getJSONObject(i);
//
//
//                            String country = jsonObject8.getString("name");
//                            String iddd = jsonObject8.getString("id");
//                            blodded.add(country);
//                        }
                        ArrayAdapter arrayAdapter9 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, blodded) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_blodd.setAdapter(arrayAdapter9);
                        for (int pos = arrayAdapter9.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter9.getItemId(pos) == bloodgrm) {
                                et_sp_blodd.setSelection(pos);
                                break;
                            }
                        }

                        JSONArray jsonArray8 = jsonObject.getJSONArray("state_list");
                        for (int i = 0; i < jsonArray8.length(); i++) {

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray8.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            goodModelArrayList.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < goodModelArrayList.size(); i++) {
                            statelis.add(goodModelArrayList.get(i).getName().toString());
                        }
                        ArrayAdapter arrayAdapter10 = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, statelis) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_state.setAdapter(arrayAdapter10);
                        for (int pos = arrayAdapter10.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter10.getItemId(pos) == stateidmm) {
                                et_sp_state.setSelection(pos);
                                break;
                            }
                        }

                    } else {
                        //  Toast.makeText(UpdatePersonal.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);
                return headers;

            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void addgetData() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(UpdatePersonal.this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();

            obj1.put("state_id", statedata);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/districtlist", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                Log.d("responsemeto", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        //      JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = response.getJSONArray("result");


                        for (int i = 0; i < jsonArray.length(); i++) {

                            Category playerModel = new Category();
                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            playerModel.setId(dataobj.getString("id"));
                            playerModel.setName(dataobj.getString("name"));

//                            JSONObject jsonObject9 = jsonArray8.getJSONObject(i);
//
//
//                            String country = jsonObject9.getString("name");
//                            String iddd = jsonObject9.getString("id");
                            distrArrL.add(playerModel);
                            //  statelis.add(country);
                        }
                        for (int i = 0; i < distrArrL.size(); i++) {
                            distlist.add(distrArrL.get(i).getName().toString());

                        }
//
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//
//                            String country = jsonObject1.getString("name");
//                            distlist.add(country);
//                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(UpdatePersonal.this, R.layout.simple_spinner_dropdown, distlist) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        arrayAdapter.notifyDataSetChanged();
                        et_sp_district.setAdapter(arrayAdapter);
                        for (int pos = arrayAdapter.getCount(); pos >= 0; pos--) {
                            if (arrayAdapter.getItemId(pos) == districid) {
                                et_sp_district.setSelection(pos);
                                break;
                            }
                        }
                    } else {
                        //   Toast.makeText(UpdatePersonal.this, ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuy = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuy);
                return headers;

            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updateprof() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            int selectedId = radiogrup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioButton = (RadioButton) findViewById(selectedId);


            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String isss = sharedPreferences.getString("id", null);

            obj1.put("user_id", isss);
            obj1.put("first_name", first_name.getText().toString());
            obj1.put("last_name", last_name.getText().toString());
            obj1.put("email", email_user.getText().toString());
            if (radioButton.getText().toString().equalsIgnoreCase("Male")) {
                dataofdender = "1";
            } else if (radioButton.getText().toString().equalsIgnoreCase("Female")) {
                dataofdender = "2";
            }
            obj1.put("dob", dateofbirth.getText().toString());
            obj1.put("gender", dataofdender);
            obj1.put("nationality", nationalDarta);
            obj1.put("category", categData);
            obj1.put("cast", castDatat);
            obj1.put("marital_status", maritalid);
            obj1.put("center_name", centerNamedRA);

            obj1.put("aadhar_no", aadhdar_user.getText().toString());
            obj1.put("state_id", statedata);
            obj1.put("district_id", DistDAtab);
            obj1.put("address", et_address.getText().toString());
            obj1.put("pin_code", et_pincode.getText().toString());

            obj1.put("police_station", et_police.getText().toString());
            obj1.put("school_id", schoolData);
            obj1.put("blood_group", bloddData);
            obj1.put("body_identification", et_body_iddent.getText().toString());
            obj1.put("body_identification2", et_body_iddent_sec.getText().toString());

            obj1.put("disability", et_disability.getText().toString());
            obj1.put("height", et_height.getText().toString());
            obj1.put("weight", et_weight.getText().toString());
            obj1.put("chest", et_chest.getText().toString());
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/updateBasicInfo", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject arr = response.getJSONObject("result");
                        String mm = arr.getString("email_verify");
                        if (mm.matches("1")) {
                            Toast.makeText(UpdatePersonal.this, "Details Update Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdatePersonal.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("valueb", "2");
                            startActivity(intent);
                        } else {
                            Toast.makeText(UpdatePersonal.this, "Otp Sent to email", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdatePersonal.this, EmailVerify.class);

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("email", email_user.getText().toString());
                            startActivity(intent);
                        }

                    } else {
                        Toast.makeText(UpdatePersonal.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyty = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyty);
                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

    private void updateLabel() {
        String myFormat = "DD-MM-YYYY"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        //dateofbirth.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
