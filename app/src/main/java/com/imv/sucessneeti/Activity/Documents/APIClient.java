package com.imv.sucessneeti.Activity.Documents;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {


    private  static  ApiInterface apiRequests;

    // public  static  String Base_Url="http://growsack.in/";
    // public  static  String Base_Url="http://ksbminfotech.com/";
    public  static  String Base_Url="http://new.successneeti.com/api/";


    private  static ApiInterface api_;

    public  static ApiInterface getInstance()
    {
        if(api_ ==null)
        {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient= new OkHttpClient().newBuilder()
                    .addInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60,TimeUnit.SECONDS)
                    .writeTimeout(60,TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit retrofit= new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(Base_Url)
                    .client(okHttpClient)
                    .build();
            ApiInterface api_ =  retrofit.create(ApiInterface.class);
            return api_;

        }
        else
            return api_;
    }



}
