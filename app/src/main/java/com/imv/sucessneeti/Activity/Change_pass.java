package com.imv.sucessneeti.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.CustomHint;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Change_pass extends AppCompatActivity {
    private EditText cardnumEditold, cardnumEditpass, cardnumEditpasscon;
    private ImageView submit_btn;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ScrollView svParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        init();
        submit_btn = findViewById(R.id.submit_btn);
        //svParent = findViewById(R.id.svParent);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cardnumEditold.getText().toString().matches("") || cardnumEditpass.getText().toString().matches("")
                        || cardnumEditpasscon.getText().toString().matches("")) {
                    Toast.makeText(Change_pass.this, "fill all first", Toast.LENGTH_SHORT).show();
                } else if (cardnumEditold.getText().toString().length() <= 5 || cardnumEditpass.getText().toString().length() <= 5 ||
                        cardnumEditpasscon.getText().toString().length() <= 5) {
                    Toast.makeText(Change_pass.this, "Invalid", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        jsondatrat();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //Utilities.hideKeyboard(svParent, Change_pass.this);
    }

    private void init() {
        cardnumEditold = findViewById(R.id.cardnumEditold);
        cardnumEditpass = findViewById(R.id.cardnumEditpass);
        cardnumEditpasscon = findViewById(R.id.cardnumEditpasscon);
        cardnumEditold.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditold.getText().toString().length() <= 0) {
                    cardnumEditold.setError("Enter Password");
                } else if (cardnumEditold.getText().toString().length() <= 5) {
                    cardnumEditold.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditold.setError(null);
                }
            }
        });
        cardnumEditpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpass.getText().toString().length() <= 0) {
                    cardnumEditpass.setError("Enter Password");
                } else if (cardnumEditpass.getText().toString().length() <= 5) {
                    cardnumEditpass.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditpass.setError(null);
                }
            }
        });
        cardnumEditpasscon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpasscon.getText().toString().length() <= 0) {
                    cardnumEditpasscon.setError("Enter Password");
                } else if (cardnumEditpasscon.getText().toString().length() <= 5) {
                    cardnumEditpasscon.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditpasscon.setError(null);
                }
            }
        });


        Typeface myTypeface = Typeface.createFromAsset(this.getAssets(),
                "fonts/calibri_bold_italic.ttf");
        cardnumEditold.setTypeface(myTypeface);
        cardnumEditpass.setTypeface(myTypeface);
        cardnumEditpasscon.setTypeface(myTypeface);
        Typeface newTypeface = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        CustomHint customHint = new CustomHint(newTypeface, "Old Password", Typeface.BOLD_ITALIC, 40f);
        CustomHint customHint1 = new CustomHint(newTypeface, "New Password", Typeface.BOLD_ITALIC, 40f);

        CustomHint customHint2 = new CustomHint(newTypeface, "Confirm Password", Typeface.BOLD_ITALIC, 40f);
        cardnumEditpass.setHint(customHint1);
        cardnumEditold.setHint(customHint);
        cardnumEditpasscon.setHint(customHint2);

    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        try {
            String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String idd = sharedPreferences.getString("id", null);
            obj1.put("user_id", idd);
            obj1.put("old_password", cardnumEditold.getText().toString());
            obj1.put("new_password", cardnumEditpass.getText().toString());
            obj1.put("conf_password", cardnumEditpasscon.getText().toString());

            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/changepassword", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(Change_pass.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(Change_pass.this, "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "error"+error.toString(), Toast.LENGTH_LONG).show();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

}

