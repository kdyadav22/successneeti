package com.imv.sucessneeti.networking;


import com.imv.sucessneeti.signin.LoginModel;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiClient {

    //1
    @Headers("Content-Type: application/json")
    @POST("login")
    Call<ResponseBody> signIn(@Body JSONObject request);

}