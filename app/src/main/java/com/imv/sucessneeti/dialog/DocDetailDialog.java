package com.imv.sucessneeti.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imv.sucessneeti.Extra.DownloadTaskWithCallback;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.DocumentListAdapter;

import org.json.JSONArray;

import java.io.File;
import java.util.Objects;

public class DocDetailDialog extends Dialog implements DocumentListAdapter.DownloadDocCallback, DownloadTaskWithCallback.DidFileDownload{
    private Activity activity;
    private JSONArray jsonArray;
    private ProgressDialog pDialog;

    public DocDetailDialog(Activity activity, JSONArray jsonArray) {
        super(activity);
        this.activity = activity;
        this.jsonArray = jsonArray;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window w = getWindow();
        Objects.requireNonNull(w).addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.document_view_dialog);

        ImageView closeImageView = findViewById(R.id.closePreview);
        RecyclerView recyclerView = findViewById(R.id.docDisplay);
        initRecyclerViews(recyclerView);

        recyclerView.setAdapter(new DocumentListAdapter(jsonArray, this));

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        //window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        window.setAttributes(wlp);

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocDetailDialog.this.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    private void showProgress() {
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Please wait..");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        Objects.requireNonNull(activity).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.show();
            }
        });
    }

    private void hideProgress() {
        if (pDialog != null) {
            Objects.requireNonNull(activity).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                }
            });
        }
    }

    private void initRecyclerViews(RecyclerView recyclerView) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onDocClicked(String docUrl) {
        String fileName = docUrl.substring(docUrl.lastIndexOf('/') + 1);
        new DownloadTaskWithCallback(activity, docUrl, fileName, this);

    }

    @Override
    public void onFileDownload(File file, Boolean isFailed) {
        if (!isFailed)
        Toast.makeText(activity, "File Successfully Downloaded", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(activity, "File Couldn't be Downloaded", Toast.LENGTH_LONG).show();

    }
}