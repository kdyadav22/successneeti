package com.imv.sucessneeti.Extra;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloadTaskWithCallback {

    private Context context;
    private String downloadUrl = "", downloadFileName = "";
    private DidFileDownload fileDownloadCallback;

    public DownloadTaskWithCallback(Context context, String downloadUrl, String downloadFileName, DidFileDownload fileDownloadCallback) {
        this.context = context;
        this.downloadUrl = downloadUrl;
        this.downloadFileName = downloadFileName;
        this.fileDownloadCallback = fileDownloadCallback;
        //Start Downloading Task
        new DownloadingTask().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    //If Download completed then change button text
                    fileDownloadCallback.onFileDownload(outputFile, false);
                } else {
                    //If download failed change button text
                    fileDownloadCallback.onFileDownload(outputFile, true);
                    Log.e("TAG", "Download Failed");
                }
            } catch (Exception e) {
                //Change button text if exception occurs
                Log.e("TAG", "Download Failed with Exception - %s" + e.getLocalizedMessage());
            }

            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("TAG", "Server returned HTTP " + c.getResponseCode() + " " + c.getResponseMessage());
                }

                //Get File if SD card is present
                apkStorage = new File(String.format("%s/%s", Environment.getExternalStorageDirectory(), "Success Neeti"));

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e("TAG", "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("TAG", "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {
                //Read exception if something went wrong
                e.getMessage();
                outputFile = null;
                Log.e("TAG", "Download Error Exception %s" + e.getMessage());
            }

            return null;
        }
    }

    public interface DidFileDownload {
        void onFileDownload(File file, Boolean isFailed);

    }
}