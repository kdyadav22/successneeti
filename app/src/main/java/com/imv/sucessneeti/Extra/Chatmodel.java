package com.imv.sucessneeti.Extra;

public class Chatmodel {
    String type;
    String name;
    String title;
    String description;
    String ts;

    public Chatmodel(String type, String name, String title, String description, String ts) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.description = description;
        this.ts = ts;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
