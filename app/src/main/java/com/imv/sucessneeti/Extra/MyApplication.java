package com.imv.sucessneeti.Extra;

import android.app.Application;
import android.content.ContextWrapper;

import com.imv.sucessneeti.paymentgaet.AppEnvironment;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MyApplication extends Application {

    private static MyApplication mInstance;
    AppEnvironment appEnvironment;
    public MyApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
        mInstance = this;
        appEnvironment = AppEnvironment.SANDBOX;

        List<Locale> locales = new ArrayList<>();
        locales.add(Locale.ENGLISH);
    }
    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }
    public static synchronized MyApplication getInstance() {
        return mInstance;
    }



}