package com.imv.sucessneeti.Extra;

public class Recmon_jobsm {
    String id;
     String user_state;
     String job_title;
     String job_state;
     String last_date;
     String fee;
     int applied;

    public Recmon_jobsm(String id, String user_state, String job_title, String job_state, String last_date, String fee, int applied) {
        this.id = id;
        this.user_state = user_state;
        this.job_title = job_title;
        this.job_state = job_state;
        this.last_date = last_date;
        this.fee = fee;
        this.applied = applied;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_state() {
        return user_state;
    }

    public void setUser_state(String user_state) {
        this.user_state = user_state;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_state() {
        return job_state;
    }

    public void setJob_state(String job_state) {
        this.job_state = job_state;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public int getApplied() {
        return applied;
    }

    public void setApplied(int applied) {
        this.applied = applied;
    }
}
