package com.imv.sucessneeti.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.personal.UpdatePersonal;
import com.imv.sucessneeti.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Personalinf extends Fragment {

    TextView mobileno, schooluser, paymenttype, centername, firstnameus, lastnameus, castuser;
    TextView email_user, aadhdar_user, dobuser, gender_user, nationality, category_user, merritstatus;
    TextView disability_user, blood_groupmm, height_user, weight_user, chest_user, bodyidentificat1, bodyidentificat2;
    TextView staate_user, district_user, pincode_user, address_user, policestat;
    Button editpresonal;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personalinf, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        mobileno = view.findViewById(R.id.mobileno);
        schooluser = view.findViewById(R.id.schooluser);
        paymenttype = view.findViewById(R.id.paymenttype);
        centername = view.findViewById(R.id.centername);
        firstnameus = view.findViewById(R.id.firstname);
        lastnameus = view.findViewById(R.id.lastname);
        email_user = view.findViewById(R.id.email_user);
        aadhdar_user = view.findViewById(R.id.aadhdar_user);
        dobuser = view.findViewById(R.id.dobuser);
        gender_user = view.findViewById(R.id.gender_user);
        nationality = view.findViewById(R.id.nationality);
        category_user = view.findViewById(R.id.category_user);
        castuser = view.findViewById(R.id.castuser);
        merritstatus = view.findViewById(R.id.merritstatus);
        disability_user = view.findViewById(R.id.disability_user);
        blood_groupmm = view.findViewById(R.id.blood_group);
        height_user = view.findViewById(R.id.height_user);
        weight_user = view.findViewById(R.id.weight_user);
        chest_user = view.findViewById(R.id.chest_user);
        bodyidentificat1 = view.findViewById(R.id.bodyidentificat1);
        bodyidentificat2 = view.findViewById(R.id.bodyidentificat2);
        staate_user = view.findViewById(R.id.staate_user);
        district_user = view.findViewById(R.id.district_user);
        pincode_user = view.findViewById(R.id.pincode_user);
        address_user = view.findViewById(R.id.address_user);
        policestat = view.findViewById(R.id.policestat);
        editpresonal = view.findViewById(R.id.editpresonal);
        editpresonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UpdatePersonal.class);
                getContext().startActivity(intent);
            }
        });
        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String apis = sharedPreferences.getString("id", null);
            obj1.put("user_id", apis);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/profile", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        String firstname = jsonObject.getString("first_name");
                        firstnameus.setText(firstname);
                        String lastname = jsonObject.getString("last_name");
                        lastnameus.setText(lastname);
                        String email = jsonObject.getString("email");
                        email_user.setText(email);
                        String mobile = jsonObject.getString("mobile");
                        mobileno.setText(mobile);
                        String dob = jsonObject.getString("dob");
                        dobuser.setText(dob);
                        String gender = jsonObject.getString("gender");
                        if (gender.matches("1")) {
                            gender_user.setText("Male");
                        } else if (gender.matches("2")) {
                            gender_user.setText("Female");
                        }

                        String nationallty = jsonObject.getString("nationality_name");
                        nationality.setText(nationallty);
                        String category = jsonObject.getString("category_name");
                        category_user.setText(category);
                        String cast = jsonObject.getString("cast_name");
                        castuser.setText(cast);
                        String free_plan = jsonObject.getString("fee_plan");
                        paymenttype.setText(free_plan);
                        String marital_stust = jsonObject.getString("marital_status_name");
                        merritstatus.setText(marital_stust);
                        String cenetr_name = jsonObject.getString("center_id_name");
                        centername.setText(cenetr_name);
                        String adharno = jsonObject.getString("aadhar_no");
                        aadhdar_user.setText(adharno);
                        String state_id = jsonObject.getString("state_name");
                        staate_user.setText(state_id);
                        String district = jsonObject.getString("district_name");
                        district_user.setText(district);
                        String address = jsonObject.getString("address");
                        address_user.setText(address);
                        String pin_code = jsonObject.getString("pin_code");
                        pincode_user.setText(pin_code);
                        String police_station = jsonObject.getString("police_station");
                        policestat.setText(police_station);
                        String school_id = jsonObject.getString("school_name");
                        schooluser.setText(school_id);
                        String payment_status = jsonObject.getString("payment_status");
                        String blood_group = jsonObject.getString("blood_group_name");
                        blood_groupmm.setText(blood_group);
                        String body_identif = jsonObject.getString("body_identification");
                        bodyidentificat1.setText(body_identif);
                        String body_identif2 = jsonObject.getString("body_identification2");
                        bodyidentificat2.setText(body_identif2);
                        String disable = jsonObject.getString("disability");
                        disability_user.setText(disable);
                        String height = jsonObject.getString("height");
                        height_user.setText(height);
                        String weight = jsonObject.getString("weight");
                        weight_user.setText(weight);
                        String chest = jsonObject.getString("chest");
                        chest_user.setText(chest);
                        String nationlid = jsonObject.getString("nationality");
                        String categid = jsonObject.getString("category");
                        String castid = jsonObject.getString("cast");
                        String centerid = jsonObject.getString("center_name");
                        String schoid = jsonObject.getString("school_id");
                        String bloodId = jsonObject.getString("blood_group");
                        String stateidm = jsonObject.getString("state_id");
                        String distritid = jsonObject.getString("district_id");
                        String maritalid = jsonObject.getString("marital_status");
                        String satate_anem = jsonObject.getString("state_name");
                        String distrct_nn = jsonObject.getString("district_name");
                        String school_n = jsonObject.getString("school_name");
                        String blood_grio = jsonObject.getString("blood_group_name");

                        editor.putString("firstname", firstname);
                        editor.putString("lastname", lastname);
                        editor.apply();
                        editor.putString("nationalid", nationlid);
                        editor.putString("categid", categid);
                        editor.apply();
                        editor.putString("castid", castid);
                        editor.putString("centerid", centerid);
                        editor.putString("schoolid", schoid);
                        editor.putString("bloodid", bloodId);
                        editor.putString("stateidm", stateidm);
                        editor.putString("districid", distritid);
                        editor.putString("maritalid", maritalid);
                        editor.apply();

                        editor.putString("email", email);
                        editor.putString("aadharno", adharno);
                        editor.putString("height", height);
                        editor.putString("weight", weight);
                        editor.putString("pincode", pin_code);
                        editor.putString("aadress", address);
                        editor.putString("dateofb", dob);
                        editor.putString("disabili", disable);
                        editor.putString("chestf", chest);
                        editor.putString("policesta", police_station);
                        editor.putString("bodyident1", body_identif);
                        editor.putString("bodyident2", body_identif2);
                        editor.apply();


                    } else {
                        Toast.makeText(getContext(), "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

}
