package com.imv.sucessneeti.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.EducationAdd;
import com.imv.sucessneeti.Model.EducationModel;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.EducationAddedAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;


public class Education_List extends Fragment implements EducationAddedAdapter.EducationClickCallback {
    private RecyclerView recylereducat;

    private List<EducationModel> GetDataAdapter1;
    private EducationAddedAdapter recyclerViewadapter;
    private ImageView imageaddeducation;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String MYFILE = "MyFile";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_education__list, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        recylereducat = view.findViewById(R.id.recylereducat);
        imageaddeducation = view.findViewById(R.id.imageaddeducation);
        imageaddeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EducationAdd.class);
                getActivity().startActivity(intent);
            }
        });
        GetDataAdapter1 = new ArrayList<>();
        recylereducat.setHasFixedSize(true);
        recylereducat.setLayoutManager(new LinearLayoutManager(getActivity()));

        try {
            jsondatrat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String apiid = sharedPreferences.getString("id", null);
            obj1.put("user_id", apiid);
            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/geteducation", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {


                        JSONArray jsonArray = response.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            EducationModel GetDataAdapter2 = new EducationModel(
                                    jsonObject1.getString("id"),
                                    jsonObject1.getString("education_id"),
                                    jsonObject1.getString("education_name"),
                                    jsonObject1.getString("stream_id"),
                                    jsonObject1.getString("stream_name"),
                                    jsonObject1.getString("board_name"),
                                    jsonObject1.getString("subject_marks"),
                                    jsonObject1.getString("obtain_marks"),
                                    jsonObject1.getString("percentage"),
                                    jsonObject1.getString("passing_year")
                            );
                            GetDataAdapter1.add(GetDataAdapter2);
                            recyclerViewadapter = new EducationAddedAdapter(GetDataAdapter1, getActivity(), Education_List.this);

                            recylereducat.setAdapter(recyclerViewadapter);

                        }
                    } else {
                        Toast.makeText(getActivity(), "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String apiuyt = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apiuyt);
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onDeleteClick(EducationModel educationModel, int pos) {
        try {
            getDeleteData(educationModel, pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEditClick(EducationModel educationModel, int pos) {
        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(MYFILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("rowId", educationModel.getId());
        editor.putString("deleteval", educationModel.getId());
        editor.putString("boardnamey", educationModel.getBoard_name());
        editor.putString("subjectmy", educationModel.getSujbjectmark());
        editor.putString("obtain_may", educationModel.getObtainmar());
        editor.putString("percenty", educationModel.getPerctengae_name());
        editor.putString("selectclas", educationModel.getEducationid());
        editor.putString("streamid", educationModel.getStreamid());
        editor.putString("yeardata", educationModel.getYearofpassding());
        editor.apply();
        Intent intent = new Intent(getActivity(), EducationAdd.class);
        intent.putExtra("isComingToUpdate", true);
        getActivity().startActivity(intent);
    }

    private void getDeleteData(EducationModel educationModel, final int pos) throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            String aiid = sharedPreferences.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("id", educationModel.getId());
            obj1.put("user_id", aiid);

            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/deleteeducation", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        GetDataAdapter1.remove(pos);
                        Objects.requireNonNull(recylereducat.getAdapter()).notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Successfully Deleted", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String apiuyt = sharedPreferences.getString("deviceid", null);
                headers.put("Token", apiuyt);

                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);

    }
}