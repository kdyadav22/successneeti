package com.imv.sucessneeti.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.CustomHint;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    private EditText cardnumEditold, cardnumEditpass, cardnumEditpasscon;
    private ImageView submit_btn;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LinearLayout svParent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();
        cardnumEditold = view.findViewById(R.id.cardnumEditold);
        cardnumEditpass = view.findViewById(R.id.cardnumEditpass);
        cardnumEditpasscon = view.findViewById(R.id.cardnumEditpasscon);
        svParent = view.findViewById(R.id.svParent);

        cardnumEditold.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditold.getText().toString().length() <= 0) {
                    cardnumEditold.setError("Enter Password");
                } else if (cardnumEditold.getText().toString().length() <= 5) {
                    cardnumEditold.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditold.setError(null);
                }
            }
        });
        cardnumEditpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpass.getText().toString().length() <= 0) {
                    cardnumEditpass.setError("Enter Password");
                } else if (cardnumEditpass.getText().toString().length() <= 5) {
                    cardnumEditpass.setError("Password length minimum 6 digits");
                } else {
                    cardnumEditpass.setError(null);
                }
            }
        });
        cardnumEditpasscon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpasscon.getText().toString().length() <= 0) {
                    cardnumEditpasscon.setError("Enter Password");
                } else if (cardnumEditpasscon.getText().toString().length() <= 5) {
                    cardnumEditpasscon.setError("Password length minimum 6 digits");
                } else {
                    String passwrd = cardnumEditpass.getText().toString();
                    if (!cardnumEditpasscon.equals(passwrd)) {
                        cardnumEditpasscon.setError("Password mismatch");
                    }


                    cardnumEditpasscon.setError(null);
                }
            }
        });


        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/calibri_bold_italic.ttf");
        cardnumEditold.setTypeface(myTypeface);
        cardnumEditpass.setTypeface(myTypeface);
        cardnumEditpasscon.setTypeface(myTypeface);
        Typeface newTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/calibri_bold_italic.ttf");
        CustomHint customHint = new CustomHint(newTypeface, "Old Password", Typeface.BOLD_ITALIC, 40f);
        CustomHint customHint1 = new CustomHint(newTypeface, "New Password", Typeface.BOLD_ITALIC, 40f);

        CustomHint customHint2 = new CustomHint(newTypeface, "Confirm Password", Typeface.BOLD_ITALIC, 40f);
        cardnumEditpass.setHint(customHint1);
        cardnumEditold.setHint(customHint);
        cardnumEditpasscon.setHint(customHint2);
        submit_btn = view.findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cardnumEditold.getText().toString().matches("") ||
                        cardnumEditpass.getText().toString().matches("") || cardnumEditpasscon.getText().toString().matches("")) {
                    if (cardnumEditold.getText().toString().matches("")) {
                        cardnumEditold.setError("Old Password is required");
                    }
                    if (cardnumEditpass.getText().toString().matches("")) {
                        cardnumEditpass.setError("New Password is required");
                    }
                    if (cardnumEditpasscon.getText().toString().matches("")) {
                        cardnumEditpasscon.setError("Confirm Password is required");
                    }
                    if (cardnumEditold.getText().toString().length() <= 5 || cardnumEditpass.getText().toString().length() <= 5 ||
                            cardnumEditpasscon.getText().toString().length() <= 5) {
                        Toast.makeText(getContext(), "Invalid", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    String mm = cardnumEditpass.getText().toString();
                    if (mm.matches(cardnumEditpasscon.getText().toString())) {
                        try {
                            jsondatrat();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "Password not Match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        Utilities.hideKeyboard(svParent, getActivity());

        return view;
    }

    private void jsondatrat() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
        final String url = "http://new.successneeti.com/api/user/signup";
        JSONObject obj = new JSONObject();
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        try {
            String deviceAppUID = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String idd = sharedPreferences.getString("id", null);
            obj1.put("user_id", idd);
            obj1.put("old_password", cardnumEditold.getText().toString());
            obj1.put("new_password", cardnumEditpass.getText().toString());
            obj1.put("conf_password", cardnumEditpasscon.getText().toString());

            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/changepassword", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(getContext(), "Successfully Changed", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);


                    } else {
                        Toast.makeText(getContext(), "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            } };

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //hideSoftKeyboard(Objects.requireNonNull(getActivity()));

    }

    @Override
    public void onPause() {
        super.onPause();
        //hideSoftKeyboard(Objects.requireNonNull(getActivity()));
    }

    private static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
