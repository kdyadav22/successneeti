package com.imv.sucessneeti.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Model.MainPhoto;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.PhotoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class PhotoFrag extends Fragment {

    private RecyclerView recyclerphoto;
    private ProgressDialog progressDialog;
    private List<MainPhoto> GetDataAdapter1;
    private PhotoAdapter recyclerViewadapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();


        recyclerphoto = view.findViewById(R.id.recyclerphoto);
        GetDataAdapter1 = new ArrayList<>();
        recyclerphoto.setHasFixedSize(true);
        recyclerphoto.setLayoutManager(new LinearLayoutManager(getActivity()));

        try {
            jsondatrat();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    public void JSON_DATA_WEB_CALL() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading..."); // Setting Message
        progressDialog.setTitle("Please wait..."); // Setting Title
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getActivity()));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //  Toast.makeText(getContext(), "right", Toast.LENGTH_SHORT).show();

                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray array = jsonObject.getJSONArray("image_type");
                            Log.d("Response", array.toString());
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonObject1 = array.getJSONObject(i);

                                MainPhoto GetDataAdapter2 = new MainPhoto(
                                        jsonObject1.getString("id"),
                                        jsonObject1.getString("name")
                                );
                                GetDataAdapter1.add(GetDataAdapter2);
                                recyclerViewadapter = new PhotoAdapter(GetDataAdapter1, getContext());

                                recyclerphoto.setAdapter(recyclerViewadapter);

                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //   Toast.makeText(getActivity(), "wrong", Toast.LENGTH_SHORT).show();
                Log.d("Error.Response", error.toString());
            }
        });

        queue.add(stringRequest);
    }

    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String spoid = sharedPreferences.getString("id", null);
            obj1.put("user_id", spoid);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/photoslist

/* "host_code": 0,
    "host_description": "Success",
    "result": {
        "id": "8",
        "profile_photo": "5e11da8b43578.jpeg",
        "left_thumb": "",
        "right_thumb": "",
        "student_signature": "",
        "father_signature": "",
        "mother_signature": "",
        "image_path": "http://new.successneeti.com/UPLOADS/student_images/9"
* */

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/master/all", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        JSONArray jsonArray = jsonObject.getJSONArray("image_type");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            MainPhoto GetDataAdapter2 = new MainPhoto(
                                    jsonObject1.getString("name"),
                                    jsonObject1.getString("id")
                            );
                            GetDataAdapter1.add(GetDataAdapter2);
                            recyclerViewadapter = new PhotoAdapter(GetDataAdapter1, getContext());

                            recyclerphoto.setAdapter(recyclerViewadapter);
                        }
                        String mm = sharedPreferences.getString("meme", null);

                        if (mm != null) {
                            if (mm.matches("1")) {
                                editor.putString("meme", "2");
                                editor.apply();
                                FragmentTransaction ft = Objects.requireNonNull(getFragmentManager()).beginTransaction();
                                if (Build.VERSION.SDK_INT >= 26) {
                                    ft.setReorderingAllowed(false);
                                }
                                ft.detach(PhotoFrag.this).attach(PhotoFrag.this).commit();
                            }
                        }

                        try {
                            jsonImage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);
                Log.d("dslkhadf", apiuyt);
                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
    //http://new.successneeti.com/api/user/photoslist


    private void jsonImage() throws JSONException {
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String spih = sharedPreferences.getString("id", null);
            obj1.put("user_id", spih);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/photoslist", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject = response.getJSONObject("result");
                        String ssm = jsonObject.getString("profile_photo");
                        String leftThum = jsonObject.getString("left_thumb");
                        String righttuhmbm = jsonObject.getString("right_thumb");
                        String studentSign = jsonObject.getString("student_signature");
                        String fathersig = jsonObject.getString("father_signature");
                        String mothersign = jsonObject.getString("mother_signature");
                        String imagepath = jsonObject.getString("image_path");
                        Log.d("sdjb", imagepath + ssm);
                        String profil = imagepath + "\\" + ssm;
                        Log.d("yyyyy", profil);
                        String lefthum = imagepath + "\\" + leftThum;
                        Log.d("ttttt", lefthum);
                        String rigtuhm = imagepath + "\\" + righttuhmbm;
                        Log.d("ttttt", righttuhmbm);
                        String stusign = imagepath + "\\" + studentSign;
                        Log.d("ttttt", stusign);
                        String fathers = imagepath + "\\" + fathersig;
                        Log.d("ttttt", fathers);
                        String moths = imagepath + "\\" + mothersign;
                        Log.d("ttttt", moths);
                        String[] styy = {profil, lefthum, rigtuhm, stusign, fathers, moths};
//
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("TEMPP", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("profile", profil);
                        editor.putString("lefthum", lefthum);
                        editor.putString("rightumg", rigtuhm);
                        editor.putString("stusign", stusign);
                        editor.putString("fathers", fathers);
                        editor.putString("mothers", moths);
                        editor.apply();
////                            recyclerViewadapter = new PhotoAdapter(GetDataAdapter1, getContext());
////
////                                  recyclerphoto.setAdapter(recyclerViewadapter);
//                        }

                    } else {
                        //  Toast.makeText(getContext(), ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");
                String apiuyt = sharedPreferences.getString("deviceid", null);
                Log.d("ksdas", apiuyt);
                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
}

