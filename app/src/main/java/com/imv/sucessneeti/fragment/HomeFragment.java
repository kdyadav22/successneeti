package com.imv.sucessneeti.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imv.sucessneeti.R;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment {

TextView textView;
SharedPreferences sharedPreferences;
SharedPreferences.Editor editor;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);

        textView = (TextView) view.findViewById(R.id.username);
    String name  = sharedPreferences.getString("name",null);
    textView.setText(name);
        return view;
    }
}
