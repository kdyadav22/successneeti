package com.imv.sucessneeti.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.Update_Parents_Details;
import com.imv.sucessneeti.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class ParentSetails extends Fragment {

    TextView fathername,fatheroccupation,mothername,motheroccupation,parentmobile;
  Button editparent;
SharedPreferences sharedPreferences;
SharedPreferences.Editor editor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parent_setails, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        fathername = view.findViewById(R.id.fathername);
        fatheroccupation = view.findViewById(R.id.fatheroccupation);
        mothername = view.findViewById(R.id.mothername);
        motheroccupation = view.findViewById(R.id.motheroccupation);
        parentmobile = view.findViewById(R.id.parentmobile);
        editparent = view.findViewById(R.id.editparent);
        try {
            jsondatrat();
        }catch (Exception e){
            e.printStackTrace();
        }
        editparent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Update_Parents_Details.class);
                getContext().startActivity(intent);
            }
        });
        return  view;
    }
    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String url ="http://new.successneeti.com/api/user/signup";
        JSONObject obj=new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();
String  apid = sharedPreferences.getString("id",null);
            obj1.put("user_id", apid);


            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/user/profile",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {

                        JSONObject jsonObject = response.getJSONObject("result");
//                        String firstname = jsonObject.getString("first_name");
//                        firstnameus.setText(firstname);
//                        String lastname = jsonObject.getString("last_name");
//                        lastnameus.setText(lastname);
//                        String email = jsonObject.getString("email");
//                        email_user.setText(email);
//                        String mobile = jsonObject.getString("mobile");
//                        mobileno.setText(mobile);
//                        String dob = jsonObject.getString("dob");
//                        dobuser.setText(dob);
//                        String gender = jsonObject.getString("gender");
//                        gender_user.setText(gender);
//                        String nationallty = jsonObject.getString("nationality");
//                        nationality.setText(nationallty);
//                        String category = jsonObject.getString("category");
//                        category_user.setText(category);
//                        String cast = jsonObject.getString("cast");
//                        castuser.setText(cast);
//                        String free_plan = jsonObject.getString("fee_plan");
//                        paymenttype.setText(free_plan);
//                        String marital_stust = jsonObject.getString("marital_status");
//                        merritstatus.setText(marital_stust);
//                        String cenetr_name = jsonObject.getString("center_name");
//                        centername.setText(cenetr_name);
//                        String adharno = jsonObject.getString("aadhar_no");
//                        aadhdar_user.setText(adharno);
//                        String state_id = jsonObject.getString("state_id");
//                        staate_user.setText(state_id);
//                        String district = jsonObject.getString("district_id");
//                        district_user.setText(district);
//                        String address = jsonObject.getString("address");
//                        address_user.setText(address);
//                        String pin_code = jsonObject.getString("pin_code");
//                        pincode_user.setText(pin_code);
//                        String police_station = jsonObject.getString("police_station");
//                        policestat.setText(police_station);
//                        String school_id = jsonObject.getString("school_id");
//                        schooluser.setText(school_id);
//                        String payment_status = jsonObject.getString("payment_status");
//                        String blood_group = jsonObject.getString("blood_group");
//                        blood_groupmm.setText(blood_group);
//                        String body_identif = jsonObject.getString("body_identification");
//                        bodyidentificat1.setText(body_identif);
//                        String  body_identif2 = jsonObject.getString("body_identification2");
//                        bodyidentificat2.setText(body_identif2);
//                        String disable = jsonObject.getString("disability");
//                        disability_user.setText(disable);
//                        String height = jsonObject.getString("height");
//                        height_user.setText(height);
//                        String weight = jsonObject.getString("weight");
//                        weight_user.setText(weight);
//                        String chest = jsonObject.getString("chest");
//                        chest_user.setText(chest);
                        String father_named = jsonObject.getString("father_name");
                        fathername.setText(father_named);
                        editor.putString("father_name",father_named);
                        String father_occupation = jsonObject.getString("father_occupation");
                        fatheroccupation.setText(father_occupation);
                        editor.putString("father_occupation",father_occupation);
                        String mother_name = jsonObject.getString("mother_name");
                        mothername.setText(mother_name);
                        editor.putString("mother_name",mother_name);
                        String mother_occupation = jsonObject.getString("mother_occupation");
                        motheroccupation.setText(mother_occupation);
                        editor.putString("mother_occupation",mother_occupation);
                        String parent_mobile = jsonObject.getString("parent_mobile");
                        parentmobile.setText(parent_mobile);
                        editor.putString("parent_mobile",parent_mobile);
                        editor.apply();
//                        String national = jsonObject.getString("nationality_name");
//                        String category_name = jsonObject.getString("category_name");
//                        String  cast_anme = jsonObject.getString("cast_name");
//                        String cenetr_idname = jsonObject.getString("center_id_name");
//                        String  satate_anem = jsonObject.getString("state_name");
//                        String distrct_nn = jsonObject.getString("district_name");
//                        String school_n = jsonObject.getString("school_name");
//                        String blood_grio = jsonObject.getString("blood_group_name");
//

                    }
                    else{
                        Toast.makeText(getContext(), ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid",null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }

}
