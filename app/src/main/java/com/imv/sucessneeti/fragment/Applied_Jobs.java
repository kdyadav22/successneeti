package com.imv.sucessneeti.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.Recmon_jobsm;
import com.imv.sucessneeti.Model.AppliedjobMode;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.Applied_job_ad;
import com.imv.sucessneeti.adapter.Exp_jobAda;
import com.imv.sucessneeti.dialog.DocDetailDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Applied_Jobs extends Fragment implements Applied_job_ad.AppliedDocCallback {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    RecyclerView recyclerviewrec;

    private List<AppliedjobMode> GetDataAdapter1;
    private Applied_job_ad recyclerViewadapter;

    //   jobs/appliedjobs
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_applied__jobs, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        recyclerviewrec = view.findViewById(R.id.recucyaplied);

        GetDataAdapter1 = new ArrayList<>();
        recyclerviewrec.setHasFixedSize(true);
        recyclerviewrec.setLayoutManager(new LinearLayoutManager(getActivity()));

        try {
            jsondatratu();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void jsondatratu() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String apis = sharedPreferences.getString("id", null);
            obj1.put("user_id", apis);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/jobs/appliedjobs", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {


                        JSONArray jsonArray = response.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            AppliedjobMode GetDataAdapter2 = new AppliedjobMode(
                                    // "reg_receipt":"http:\/\/new.successneeti.com\/UPLOADS\/student_images\/47\/1579958071.jpg",
                                    //      "fee_receipt":"http:\/\/new.successneeti.com\/UPLOADS\/student_images\/47\/1579958250.png",
                                    //      "admit_card":"",
                                    //      "result":"",

                                    jsonObject1.getString("id"),
                                    jsonObject1.getString("title"),
                                    jsonObject1.getString("state_name"),
                                    jsonObject1.getString("last_date"),
                                    jsonObject1.getString("fee"),
                                    jsonObject1.getString("admit_card"),
                                    jsonObject1.getString("result"),
                                    jsonObject1.getString("fee_receipt"),
                                    jsonObject1.getString("reg_receipt")

                            );
                            GetDataAdapter1.add(GetDataAdapter2);
                            recyclerViewadapter = new Applied_job_ad(GetDataAdapter1, getContext(), Applied_Jobs.this);

                            recyclerviewrec.setAdapter(recyclerViewadapter);

                        }

                    } else {
                        Toast.makeText(getContext(), "" + response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                //    headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onAppliedDocClicked(AppliedjobMode getDataAdapter1) {
        try {
            JSONArray jsonArray = new JSONArray();

            if (!TextUtils.isEmpty(getDataAdapter1.getAdmit_card())) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("DocTitle", "AdmitCard");
                jsonObject1.put("DocUrl", getDataAdapter1.getAdmit_card());
                jsonArray.put(jsonObject1);
            }

            if (!TextUtils.isEmpty(getDataAdapter1.getFee_rec())) {
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("DocTitle", "Fee Receipt");
                jsonObject2.put("DocUrl", getDataAdapter1.getFee_rec());
                jsonArray.put(jsonObject2);
            }

            if (!TextUtils.isEmpty(getDataAdapter1.getResult())) {
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("DocTitle", "Result");
                jsonObject3.put("DocUrl", getDataAdapter1.getResult());
                jsonArray.put(jsonObject3);
            }

            if (!TextUtils.isEmpty(getDataAdapter1.getReg_rec())) {
                JSONObject jsonObject4 = new JSONObject();
                jsonObject4.put("DocTitle", "Reg Receipt");
                jsonObject4.put("DocUrl", getDataAdapter1.getReg_rec());
                jsonArray.put(jsonObject4);
            }

            openDocDialog(jsonArray);

        } catch (Exception e) {
            e.getMessage();
        }

    }

    private void openDocDialog(JSONArray jsonArray) {
        try {
            DocDetailDialog uploadPictureDialog;
            uploadPictureDialog = new DocDetailDialog(getActivity(), jsonArray);
            uploadPictureDialog.setCancelable(true);
            uploadPictureDialog.setCanceledOnTouchOutside(true);
            uploadPictureDialog.show();

        } catch (Exception e) {
            Log.d("TAG", e.getMessage());
        }
    }
}
