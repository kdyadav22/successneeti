package com.imv.sucessneeti.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Extra.Recmon_jobsm;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.adapter.Exp_jobAda;
import com.imv.sucessneeti.adapter.Rec_jobsAda;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Expired_jobs extends Fragment {

    RecyclerView recyclerviewrec;

    private List<Recmon_jobsm> GetDataAdapter1;
    private Exp_jobAda recyclerViewadapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public Expired_jobs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expired_jobs, container, false);
        sharedPreferences = getContext().getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getContext().getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        recyclerviewrec = view.findViewById(R.id.recyexpijob);

        GetDataAdapter1 = new ArrayList<>();
        recyclerviewrec.setHasFixedSize(true);
        recyclerviewrec.setLayoutManager(new LinearLayoutManager(getActivity()));

        try {
            jsondatrat();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }
    private void jsondatrat() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JSONObject obj=new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1=new JSONObject();
            String apis = sharedPreferences.getString("id",null);
            obj1.put("user_id", apis);


            obj.put("transaction_data",obj1);
            Log.d("asdfjbs,bdf",obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://new.successneeti.com/api/jobs/expiredjobs",new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response",""+response);
                try {
                    String ss = response.getString("host_code");
                    if(ss.matches("0"))
                    {


                        JSONArray jsonArray  = response.getJSONArray("result");
                        for(int i = 0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            Recmon_jobsm GetDataAdapter2 = new Recmon_jobsm(
                                    jsonObject1.getString("id"),
                                    jsonObject1.getString("user_state"),
                                    jsonObject1.getString("title"),
                                    jsonObject1.getString("job_state_name"),
                                    jsonObject1.getString("last_date"),
                                    jsonObject1.getString("fee"),
                                    jsonObject1.getInt("is_applied")
                            );
                            GetDataAdapter1.add(GetDataAdapter2);
                            recyclerViewadapter = new Exp_jobAda(GetDataAdapter1, getContext());

                            recyclerviewrec.setAdapter(recyclerViewadapter);

                        }

                    }
                    else{
                        Toast.makeText(getContext(), ""+response.getString("host_description"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                //    headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid",null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }



}