package com.imv.sucessneeti.signin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.imv.sucessneeti.Activity.Forget_pass;
import com.imv.sucessneeti.Activity.OtpScren;
import com.imv.sucessneeti.Activity.SignUp;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.common.Utilities;
import com.imv.sucessneeti.networking.ApiClient;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignIn extends AppCompatActivity {
    private EditText cardnumEditmobile;
    private EditText cardnumEditpass;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String deviceAppUID;
    LinearLayout svParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Task<InstanceIdResult> task = FirebaseInstanceId.getInstance().getInstanceId();
        task.addOnSuccessListener(authResult -> {
            // Task completed successfully
            // ...
            deviceAppUID = authResult.getToken();

        });
        svParent = findViewById(R.id.svParent);

        task.addOnFailureListener(e -> {
            // Task failed with an exception
            // ...
        });
        sharedPreferences = getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = getSharedPreferences("TEMP", MODE_PRIVATE).edit();
        init();
        ImageView submit_btn = findViewById(R.id.submit_btn);
        cardnumEditmobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditmobile.getText().toString().length() <= 0) {
                    cardnumEditmobile.setError("Enter mobile no");
                } else {
                    cardnumEditmobile.setError(null);
                }
            }
        });
        cardnumEditpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (cardnumEditpass.getText().toString().length() <= 0) {
                    cardnumEditpass.setError("Enter password");
                } else if (cardnumEditpass.getText().toString().length() <= 5) {
                    cardnumEditpass.setError("Password length must be of minimum 6 digits");
                } else {
                    cardnumEditpass.setError(null);
                }
            }
        });
        submit_btn.setOnClickListener(v -> {
            if (cardnumEditmobile.getText().toString().matches("")) {
                cardnumEditmobile.setError("Mobile no empty");
            }
            if (cardnumEditpass.getText().toString().matches("")) {
                cardnumEditpass.setError("Password empty");
            }
            if (cardnumEditmobile.getText().toString().length() != 10) {
                cardnumEditmobile.setError("Enter valid mobile no");
            } else {
                try {
                    jsondatrat();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Utilities.hideKeyboard(svParent, SignIn.this);

    }

    private void jsondatrat() {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject obj = new JSONObject();
        try {
            JSONObject obj1 = new JSONObject();
            obj1.put("username", cardnumEditmobile.getText().toString());
            obj1.put("device_id", deviceAppUID);
            obj1.put("password", cardnumEditpass.getText().toString());
            obj.put("transaction_data", obj1);

            final String requestBody = obj.toString();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/login", new JSONObject(requestBody), response -> {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {

                        JSONObject jsonObject1 = response.getJSONObject("result");
                        String id = jsonObject1.getString("id");
                        String name = jsonObject1.getString("name");
                        String email = jsonObject1.getString("email");
                        String mobile = jsonObject1.getString("mobile");
                        String token = jsonObject1.getString("token");
                        String subscription = jsonObject1.getString("subscription");
                        String first_name = jsonObject1.getString("first_name");
                        String last_name = jsonObject1.getString("last_name");
                        String imagepath = jsonObject1.getString("image");
                        int subscription_fee = jsonObject1.getInt("subscription_fee");
                        Log.d("idddd", id);
                        Log.d("resss", token);
                        Prefs.putString("deviceid", token);

                        editor.putString("id", id);
                        editor.putString("name", name);
                        editor.putString("email", email);
                        editor.putString("mobile", mobile);
                        editor.putString("firstname", first_name);
                        editor.putString("lastname", last_name);
                        editor.putString("deviceid", token);
                        editor.putString("subscription", subscription);
                        editor.putString("subscription_fee", String.valueOf(subscription_fee));
                        editor.putBoolean("LOGGEDIN", true);
                        editor.apply();
                        editor.putString("imagepath", imagepath);
                        editor.apply();
                        Toast.makeText(SignIn.this, "Successfully login", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignIn.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (response.getInt("host_code") == 2) {
                        Intent intent = new Intent(SignIn.this, OtpScren.class);
                        intent.putExtra("mobile", cardnumEditmobile.getText().toString());
                        startActivity(intent);
                    } else {
                        Toast.makeText(SignIn.this, "Invalid Mobile No or Password", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

            }, error -> {
                progressDialog.dismiss();
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(SignIn.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    Toast.makeText(SignIn.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    Toast.makeText(SignIn.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    Toast.makeText(SignIn.this, message, Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    Toast.makeText(SignIn.this, message, Toast.LENGTH_SHORT).show();
                }

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "applications/json");
                    return headers;
                }
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            requestQueue.add(jsonObjectRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView register = findViewById(R.id.register);
        TextView forget = findViewById(R.id.forgetpass);
        cardnumEditmobile = findViewById(R.id.cardnumEditmobile);
        cardnumEditpass = findViewById(R.id.cardnumEditpass);
        register.setOnClickListener(v -> {
            Intent intent = new Intent(SignIn.this, SignUp.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        forget.setOnClickListener(v -> {
            Intent intent = new Intent(SignIn.this, Forget_pass.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }
}
