package com.imv.sucessneeti.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.R;
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    NotificationCompat.Builder notificationBuilder;

    Bitmap image;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("msg", "onMessageReceived: " + remoteMessage.getData().get("message"));

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Default";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody()).setAutoCancel(true).setContentIntent(pendingIntent);;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, builder.build());
    }
}
//        super.onMessageReceived(remoteMessage);
//        if(DEBUG_MODE) {
//            Log.d(TAG, "From : " + remoteMessage.getFrom());
//            Log.d(TAG, "FCM Data : " + remoteMessage.getData());
//        }
//
//        String tag = "text";
//        String title = remoteMessage.getData().get("title");
//        String msg = remoteMessage.getData().get("message");
//        String img = remoteMessage.getData().get("image");
//        String type = remoteMessage.getData().get("type");
//
//        if (img.length() > 5) { tag ="image"; image = getBitmapFromURL(img);}
//        if(type == null){ type = "none"; }
//
//        switch(title){
//
//            case "credit":
//
//                title = getResources().getString(R.string.congratulations);
//                msg = msg+ " " + getResources().getString(R.string.app_currency) + " " + getResources().getString(R.string.successfull_received);
//                type = "transactions";
//
//                break;
//
//            case "redeemed":
//
//                title = getResources().getString(R.string.redeem_recevied)+" "+msg;
//                msg = getResources().getString(R.string.redeem_succes_message);
//                type = "transactions";
//
//                break;
//
//            case "referer":
//
//                title = getResources().getString(R.string.congratulations);
//                msg = msg+ " " + getResources().getString(R.string.app_currency) + " " + getResources().getString(R.string.refer_bonus_received);
//
//                break;
//
//            case "invite":
//
//                title = getResources().getString(R.string.congratulations);
//                msg = msg+ " " + getResources().getString(R.string.app_currency) + " " + getResources().getString(R.string.invitation_bonus_received);
//                type = "transactions";
//
//                break;
//        }
//
//        sendNotification(tag, title, msg, type, image);
//    }
//
//    /**
//     * Show the notification received
//     */
//    private void sendNotification(String tag, String title, String messageBody, String type, Bitmap img) {
//
//		Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        if (tag.equalsIgnoreCase("image")) {
//            notificationBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle(title)
//                    .setContentText(messageBody)
//                    .setStyle(new NotificationCompat.BigPictureStyle()
//                            .bigPicture(img))
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri);
//
//        } else {
//
//            notificationBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle(title)
//                    .setContentText(messageBody)
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri);
//        }
//
//        if(type.equals("transactions")){
//
//			Intent transactionsintent = new Intent(this, FragmentsActivity.class);
//            transactionsintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            transactionsintent.putExtra("show","transactions");
//			PendingIntent transactionsIntent = PendingIntent.getActivity(this, 0, transactionsintent,PendingIntent.FLAG_ONE_SHOT);
//            notificationBuilder.setContentIntent(transactionsIntent);
//
//        }else{
//
//			Intent appintent = new Intent(this, AppActivity.class);
//            appintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			PendingIntent appIntent = PendingIntent.getActivity(this, 0, appintent,PendingIntent.FLAG_ONE_SHOT);
//            notificationBuilder.setContentIntent(appIntent);
//
//		}
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(App.getInstance().get("noid",10)+1 , notificationBuilder.build());
//    }
//
//    public static Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//
//            return BitmapFactory.decodeStream(input);
//
//        } catch (IOException e) { /* e.printStackTrace() */; return null; }
//    }
//}
