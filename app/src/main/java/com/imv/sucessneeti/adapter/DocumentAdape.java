package com.imv.sucessneeti.adapter;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.Model.DocumentList;
import com.imv.sucessneeti.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class DocumentAdape extends RecyclerView.Adapter<DocumentAdape.ViewHolder> {
    Context context;
    String mmm;
    List<DocumentList> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String MYFILE = "MyFile";
    DownloadManager downloadManager;
    String urlimagebb = "";
    String namebb = "";

    public DocumentAdape(List<DocumentList> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardesigndocu, parent, false);
        sharedPreferences = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.imagedeletedey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getDeleteData();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        viewHolder.downloadid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = sharedPreferences.getString("image_path", null);
                String yy = name + "\\" + mmm;
//                downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
//                Uri uri = Uri.parse(yy);
//                DownloadManager.Request request = new DownloadManager.Request(uri);
//
//                request.setTitle(viewHolder.title_doc.getText().toString());
////                request.setDestinationInExternalPublicDir("/Downloads/", viewHolder.title_doc.getText().toString()+".jpg");
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                Long reference = downloadManager.enqueue(request);
                MyAsyncTask asyncTask = new MyAsyncTask();
                asyncTask.execute();
                urlimagebb = yy;
                namebb = viewHolder.title_doc.getText().toString();
            }
        });

        return viewHolder;
    }

    private void getDeleteData() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            JSONObject obj1 = new JSONObject();
            String iddd = sharedPreferences.getString("id", null);
            obj1.put("document_id", subject);
            obj1.put("user_id", iddd);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/deletedocument", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(context, "Delete Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("valueb", "6");
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                //    headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferences.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);

    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final DocumentList getDataAdapter1 = getDataAdapter.get(position);


        Viewholder.title_doc.setText(getDataAdapter1.getTitle());
        Viewholder.datedoc.setText(getDataAdapter1.getDate());
        mmm = getDataAdapter1.getNamei();
//        Viewholder.idofview.setText(getDataAdapter1.getId());

        Viewholder.idofdocument.setText(getDataAdapter1.getId());
        subject = Viewholder.idofdocument.getText().toString();
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title_doc, datedoc, downloadid, idofdocument;
        public TextView percentageuse, yearofpassing, idofview;
        public ImageView imagedeletedey, updateeducation;

        public ViewHolder(View itemView) {

            super(itemView);

            title_doc = (TextView) itemView.findViewById(R.id.title_doc);
            datedoc = (TextView) itemView.findViewById(R.id.datedoc);
            imagedeletedey = (ImageView) itemView.findViewById(R.id.imagedeletedey);
            downloadid = (TextView) itemView.findViewById(R.id.downloadid);
            idofdocument = (TextView) itemView.findViewById(R.id.idofdocument);
        }


    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String tata = urlimagebb;
            try {
                String folder_main = "Success Neeti";


                URL url = new URL(tata);
                //create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //set up some things on the connection
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                //and connect!
                urlConnection.connect();
                //set the path where we want to save the file in this case, going to save it on the root directory of the sd card.
                File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!f.exists()) {
                    f.mkdirs();
                }
                //  File SDCardRoot = Environment.getExternalStoragePublicDirectory("Download");
                //create a new file, specifying the path, and the filename which we want to save the file as.
                File file = new File(f, namebb + ".pdf");
                //this will be used to write the downloaded data into the file we created
                FileOutputStream fileOutput = new FileOutputStream(file);
                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();
                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer
                //now, read through the input buffer and write the contents to the file
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    //this is where you would do something to report the prgress, like this maybe
                    //updateProgress(downloadedSize, totalSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(context, "Pdf Successfully Downloaded", Toast.LENGTH_SHORT).show();
        }
    }
}
