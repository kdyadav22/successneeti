package com.imv.sucessneeti.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.imv.sucessneeti.R;
import com.imv.sucessneeti.databinding.ItemDocLayoutBinding;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.DocViewHolder> {
    private JSONArray docJsonArray;
    private DownloadDocCallback downloadDocCallback;

    public DocumentListAdapter(JSONArray docJsonArray, DownloadDocCallback downloadDocCallback) {
        this.docJsonArray = docJsonArray;
        this.downloadDocCallback = downloadDocCallback;
    }

    @NonNull
    @Override
    public DocumentListAdapter.DocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDocLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_doc_layout, parent, false);
        return new DocViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentListAdapter.DocViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.setIsRecyclable(true);
        JSONObject jsonObject ;
        try {
             jsonObject = docJsonArray.getJSONObject(position);
             holder.itemRowBinding.docName.setText(jsonObject.getString("DocTitle"));
            Picasso.get()
                    .load(jsonObject.getString("DocUrl"))
                    .placeholder(R.drawable.background)
                    .into(holder.itemRowBinding.docImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.itemRowBinding.downloadDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    downloadDocCallback.onDocClicked(docJsonArray.getJSONObject(position).getString("DocUrl"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return docJsonArray.length();
    }


    class DocViewHolder extends RecyclerView.ViewHolder {

        ItemDocLayoutBinding itemRowBinding;

        DocViewHolder(ItemDocLayoutBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public interface DownloadDocCallback {
        void onDocClicked(String docUrl);
    }

}
