package com.imv.sucessneeti.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.imv.sucessneeti.Extra.Recmon_jobsm;
import com.imv.sucessneeti.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Exp_jobAda extends  RecyclerView.Adapter<Exp_jobAda.ViewHolder> {
    Context context;
    float finauu;
    String yut;
    List<Recmon_jobsm> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    private static final String MYFILE ="MyFile";
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;

    public Exp_jobAda(List<Recmon_jobsm> getDataAdapter, Context context){

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardesignexpire, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();

//        viewHolder.linearyiu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, OpenPayment_Gatway.class);
//                intent.putExtra("value",yut);
//                context.startActivity(intent);
//            }
//        });
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final Recmon_jobsm getDataAdapter1 =  getDataAdapter.get(position);



        Viewholder.titlename.setText(getDataAdapter1.getJob_title());
        Viewholder.jobstate.setText(getDataAdapter1.getJob_state());

        String ss = getDataAdapter1.getFee();
        Log.d("fy", "onBindViewHolder: "+ss);
        float mm = Float.parseFloat(ss);
        finauu = (mm *18)/100;
        float ssss =finauu + mm;
        Log.d("fy", "onBindViewHolder: "+finauu);
        yut = String.valueOf(ssss);

        Log.d("fy", "onBindViewHolder: "+yut);
        Viewholder.jobfee.setText(yut);
        // Viewholder.experuser.setText(getCountOfDays(getDataAdapter1.getFirstday(),getDataAdapter1.getLasday()));
        Viewholder.lastdate.setText(getDataAdapter1.getLast_date());
//        subject = getDataAdapter1.getId();
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView titlename,jobstate,jobfee;
        public TextView lastdate,idofview;
        LinearLayout linearyiu;
        public ViewHolder(View itemView) {

            super(itemView);

            titlename = (TextView) itemView.findViewById(R.id.titlename);
            jobstate = (TextView) itemView.findViewById(R.id.jobstate);
            jobfee = (TextView) itemView.findViewById(R.id.jobfee);
            lastdate = (TextView) itemView.findViewById(R.id.lastdate);
            linearyiu = (LinearLayout) itemView.findViewById(R.id.linearyiu);
        }


    }
}
