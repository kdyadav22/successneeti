package com.imv.sucessneeti.adapter;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.imv.sucessneeti.Activity.edumodel.Detail;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.databinding.AddsubBinding;
import com.imv.sucessneeti.databinding.ItemDocLayoutBinding;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class AddEducationAdapter extends RecyclerView.Adapter<AddEducationAdapter.DocViewHolder> {
    private List<Detail> detailList;
    private AddSubjectDetailsCallback addSubjectDetailsCallback;

    public AddEducationAdapter(List<Detail> detailList, AddSubjectDetailsCallback addSubjectDetailsCallback) {
        this.detailList = detailList;
        this.addSubjectDetailsCallback = addSubjectDetailsCallback;
    }

    @NonNull
    @Override
    public AddEducationAdapter.DocViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AddsubBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.addsub, parent, false);
        return new DocViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AddEducationAdapter.DocViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.setIsRecyclable(true);
        try {
            final Detail object = detailList.get(position);

            holder.itemRowBinding.subjectName.setText(object.getSubjectName());

            int sMarks = object.getSubjectMarks();
            holder.itemRowBinding.subjectMark.setText(sMarks > 0 ? "" + sMarks : "");

            int oMarks = object.getObtainMarks();
            holder.itemRowBinding.obtainMark.setText(oMarks > 0 ? "" + oMarks : "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.itemRowBinding.deleteEdu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addSubjectDetailsCallback.onDeleteClicked(position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }


    class DocViewHolder extends RecyclerView.ViewHolder {

        AddsubBinding itemRowBinding;

        DocViewHolder(final AddsubBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;

            itemRowBinding.subjectName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable.toString()))
                        return;

                    detailList.get(getAdapterPosition()).setSubjectName(itemRowBinding.subjectName.getText().toString());
                }
            });

            itemRowBinding.subjectMark.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable.toString()))
                        return;
                    detailList.get(getAdapterPosition()).setSubjectMarks(Integer.parseInt(itemRowBinding.subjectMark.getText().toString()));
                }
            });

            itemRowBinding.obtainMark.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable.toString()))
                        return;
                    detailList.get(getAdapterPosition()).setObtainMarks(Integer.parseInt(itemRowBinding.obtainMark.getText().toString()));
                }
            });
        }
    }

    public interface AddSubjectDetailsCallback {
        void onDeleteClicked(int pos);
    }

}
