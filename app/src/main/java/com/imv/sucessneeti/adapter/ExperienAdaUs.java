package com.imv.sucessneeti.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.Experience.UpdateExperience;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.Model.ExperienceModel;
import com.imv.sucessneeti.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class ExperienAdaUs extends RecyclerView.Adapter<ExperienAdaUs.ViewHolder> {
    Context context;

    List<ExperienceModel> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    private static final String MYFILE = "MyFile";
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;

    public ExperienAdaUs(List<ExperienceModel> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardesignexper, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);
        sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        viewHolder.imagedeletedey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getDeleteData();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        viewHolder.updateeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(MYFILE, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("deletevalp", subject);
                editor.putString("companynamet", viewHolder.compname.getText().toString());
                editor.putString("jobtitle", viewHolder.jobtitleus.getText().toString());
                editor.putString("industry", viewHolder.industry_id.getText().toString());
                editor.putString("roleid", viewHolder.function_id.getText().toString());
                editor.putString("salaryid", viewHolder.salary.getText().toString());
                editor.putString("frmdate", viewHolder.fromdate.getText().toString());
                editor.putString("todate", viewHolder.todate.getText().toString());
                editor.apply();
                Intent intent = new Intent(context, UpdateExperience.class);

                context.startActivity(intent);
            }
        });

        return viewHolder;
    }

    private void getDeleteData() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            String apiid = sharedPreferenceswe.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("id", subject);
            obj1.put("user_id", apiid);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
//        http://new.successneeti.com/api/user/profile
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/deleteexperience", new JSONObject(requestBody), new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),
//                        "response-"+response, Toast.LENGTH_LONG).show();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("valueb", "5");
                        context.startActivity(intent);

                    } else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "applications/json");
                // headers.put("Accept", "application/json");

                //    headers.put("Content-Type", "application/json");

                String apiuyt = sharedPreferenceswe.getString("deviceid", null);

                headers.put("Token", apiuyt);

                return headers;

            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }


        };

        requestQueue.add(jsonObjectRequest);
    }
//    public String getCountOfDays(String createdDateString, String expireDateString) {
//        Log.d("sldjkfskf",createdDateString+"\n"+expireDateString);
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
//
//        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
//        try {
//            createdConvertedDate = dateFormat.parse(createdDateString);
//            expireCovertedDate = dateFormat.parse(expireDateString);
//
//            Date today = new Date();
//
//            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        int cYear = 0, cMonth = 0, cDay = 0;
//
//        if (createdConvertedDate.after(todayWithZeroTime)) {
//            Calendar cCal = Calendar.getInstance();
//            cCal.setTime(createdConvertedDate);
//            cYear = cCal.get(Calendar.YEAR);
//            cMonth = cCal.get(Calendar.MONTH);
//            cDay = cCal.get(Calendar.DAY_OF_MONTH);
//
//        } else {
//            Calendar cCal = Calendar.getInstance();
//            cCal.setTime(todayWithZeroTime);
//            cYear = cCal.get(Calendar.YEAR);
//            cMonth = cCal.get(Calendar.MONTH);
//            cDay = cCal.get(Calendar.DAY_OF_MONTH);
//        }
//
//
//    /*Calendar todayCal = Calendar.getInstance();
//    int todayYear = todayCal.get(Calendar.YEAR);
//    int today = todayCal.get(Calendar.MONTH);
//    int todayDay = todayCal.get(Calendar.DAY_OF_MONTH);
//    */
//
//        Calendar eCal = Calendar.getInstance();
//        eCal.setTime(expireCovertedDate);
//
//        int eYear = eCal.get(Calendar.YEAR);
//        int eMonth = eCal.get(Calendar.MONTH);
//        int eDay = eCal.get(Calendar.DAY_OF_MONTH);
//
//        Calendar date1 = Calendar.getInstance();
//        Calendar date2 = Calendar.getInstance();
//
//        date1.clear();
//        date1.set(cYear, cMonth, cDay);
//        date2.clear();
//        date2.set(eYear, eMonth, eDay);
//
//        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();
//
//        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
//
//        return ("" + (int) dayCount + " Days");
//    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final ExperienceModel getDataAdapter1 = getDataAdapter.get(position);


        Viewholder.compname.setText(getDataAdapter1.getCompanyname());
        Viewholder.jobtitleus.setText(getDataAdapter1.getJobtitle());
        Viewholder.industryus.setText(getDataAdapter1.getIndustry());
        editor.putString("companynamet", getDataAdapter1.getCompanyname());
        editor.putString("jobtitlemtr", getDataAdapter1.getJobtitle());
        editor.apply();

        try {
            //Dates to compare
            String CurrentDate = getDataAdapter1.getFirstday();
            String FinalDate = getDataAdapter1.getLasday();

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            Log.e("HERE", "HERE: " + dayDifference);
        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
        Viewholder.experuser.setText(getDataAdapter1.getExperience_year() + " Year " + getDataAdapter1.getExperience_month() + " Months");
        Viewholder.idofview.setText(getDataAdapter1.getId());
        Viewholder.function_id.setText(getDataAdapter1.getFunction_id());
        Viewholder.industry_id.setText(getDataAdapter1.getIndustry_id());
        Viewholder.salary.setText(getDataAdapter1.getSalary());
        Viewholder.fromdate.setText(getDataAdapter1.getFirstday());
        Viewholder.todate.setText(getDataAdapter1.getLasday());
        subject = getDataAdapter1.getId();
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView compname, jobtitleus, industryus, salary;
        public TextView experuser, idofview, function_id, industry_id, fromdate, todate;
        public ImageView imagedeletedey, updateeducation;

        public ViewHolder(View itemView) {

            super(itemView);

            compname = (TextView) itemView.findViewById(R.id.compname);
            jobtitleus = (TextView) itemView.findViewById(R.id.jobtitleus);
            imagedeletedey = (ImageView) itemView.findViewById(R.id.imagedeletedey);
            industryus = (TextView) itemView.findViewById(R.id.industryus);
            experuser = (TextView) itemView.findViewById(R.id.experuser);
            idofview = (TextView) itemView.findViewById(R.id.idofview);
            updateeducation = (ImageView) itemView.findViewById(R.id.updateeducation);
            function_id = (TextView) itemView.findViewById(R.id.function_id);
            industry_id = (TextView) itemView.findViewById(R.id.industry_id);
            salary = (TextView) itemView.findViewById(R.id.salary);
            fromdate = (TextView) itemView.findViewById(R.id.fromdate);
            todate = (TextView) itemView.findViewById(R.id.todate);
        }


    }
}
