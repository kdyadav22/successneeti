package com.imv.sucessneeti.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.imv.sucessneeti.Model.AppliedjobMode;
import com.imv.sucessneeti.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Applied_job_ad extends RecyclerView.Adapter<Applied_job_ad.ViewHolder> {
    Context context;
    float finauu;
    String yut;
    List<AppliedjobMode> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    private static final String MYFILE = "MyFile";
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;
    AppliedDocCallback appliedDocCallback;

    public Applied_job_ad(List<AppliedjobMode> getDataAdapter, Context context, AppliedDocCallback appliedDocCallback) {
        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
        this.appliedDocCallback = appliedDocCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardesignexpire, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {
        final AppliedjobMode getDataAdapter1 = getDataAdapter.get(position);

        Viewholder.titlename.setText(getDataAdapter1.getTitle());
        Viewholder.jobstate.setText(getDataAdapter1.getJob_state());

        Viewholder.jobfee.setText("\u20B9 " + getDataAdapter1.getFee());
        // Viewholder.experuser.setText(getCountOfDays(getDataAdapter1.getFirstday(),getDataAdapter1.getLasday()));
        Viewholder.lastdate.setText(getDataAdapter1.getLast_date());
        Viewholder.linearyiu.setBackgroundColor(Color.parseColor("#4900F0"));
        Viewholder.idofview.setBackgroundColor(Color.parseColor("#4900F0"));
        Viewholder.idofview.setText("Document");
        Viewholder.idofview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appliedDocCallback.onAppliedDocClicked(getDataAdapter1);
            }
        });

//        subject = getDataAdapter1.getId();
    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titlename, jobstate, jobfee;
        TextView lastdate, idofview;
        LinearLayout linearyiu;

        ViewHolder(View itemView) {
            super(itemView);
            titlename = itemView.findViewById(R.id.titlename);
            jobstate = itemView.findViewById(R.id.jobstate);
            jobfee = itemView.findViewById(R.id.jobfee);
            lastdate = itemView.findViewById(R.id.lastdate);
            linearyiu = itemView.findViewById(R.id.linearyiu);
            idofview = itemView.findViewById(R.id.industryus);
        }
    }

    public interface AppliedDocCallback {
        void onAppliedDocClicked(AppliedjobMode getDataAdapter1);
    }
}
