package com.imv.sucessneeti.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imv.sucessneeti.Activity.EducationAdd;
import com.imv.sucessneeti.Activity.Update_Education_Deta;
import com.imv.sucessneeti.MainActivity;
import com.imv.sucessneeti.Model.EducationModel;
import com.imv.sucessneeti.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class EducationAdaUs extends RecyclerView.Adapter<EducationAdaUs.ViewHolder> {
    Context context;

    List<EducationModel> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;
    private static final String MYFILE = "MyFile";

    public EducationAdaUs(List<EducationModel> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carddesigneducation, parent, false);
        sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();

        final ViewHolder viewHolder = new ViewHolder(v);

        /*viewHolder.imagedeletedey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    subject = viewHolder.educationid.getText().toString();
                    getDeleteData();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        viewHolder.updateeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(MYFILE, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("deleteval", viewHolder.idofview.getText().toString());
                editor.putString("boardnamey", viewHolder.boardexamuser.getText().toString());
                editor.putString("subjectmy", viewHolder.subjectmarks.getText().toString());
                editor.putString("obtain_may", viewHolder.obtainmark.getText().toString());
                editor.putString("percenty", viewHolder.percentageuse.getText().toString());
                editor.putString("selectclas", viewHolder.educationid.getText().toString());
                editor.putString("streamid", viewHolder.streamid.getText().toString());
                editor.putString("yeardata", viewHolder.yearofpassing.getText().toString());
                editor.apply();
                Intent intent = new Intent(context, EducationAdd.class);
                intent.putExtra("isComingToUpdate", true);
                context.startActivity(intent);
            }
        });*/

        return viewHolder;
    }

    private void getDeleteData() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            String aiid = sharedPreferenceswe.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("id", subject);
            obj1.put("user_id", aiid);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/deleteeducation", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("valueb", "4");
                        context.startActivity(intent);
                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String apiuyt = sharedPreferenceswe.getString("deviceid", null);
                headers.put("Token", apiuyt);

                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);

    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final EducationModel getDataAdapter1 = getDataAdapter.get(position);


        Viewholder.education_name.setText(getDataAdapter1.getEducation_name());
        Viewholder.streamuser.setText(getDataAdapter1.getStream_name());
        Viewholder.boardexamuser.setText(getDataAdapter1.getBoard_name());
        Viewholder.percentageuse.setText(getDataAdapter1.getPerctengae_name());
        Viewholder.yearofpassing.setText(getDataAdapter1.getYearofpassding());
        Viewholder.idofview.setText(getDataAdapter1.getId());
        Viewholder.subjectmarks.setText(getDataAdapter1.getSujbjectmark());
        Viewholder.obtainmark.setText(getDataAdapter1.getObtainmar());
        Viewholder.educationid.setText(getDataAdapter1.getEducationid());
        Viewholder.streamid.setText(getDataAdapter1.getStreamid());
        Log.d("hlddusa", Viewholder.boardexamuser.getText().toString());

    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView education_name, streamuser, boardexamuser;
        TextView percentageuse, yearofpassing, idofview, obtainmark, subjectmarks, streamid, educationid;
        ImageView imagedeletedey, updateeducation;

        public ViewHolder(View itemView) {

            super(itemView);

            education_name = itemView.findViewById(R.id.education_name);
            streamuser = itemView.findViewById(R.id.streamuser);
            imagedeletedey = itemView.findViewById(R.id.imagedeletedey);
            boardexamuser = itemView.findViewById(R.id.boardexamuser);
            percentageuse = itemView.findViewById(R.id.percentageuse);
            yearofpassing = itemView.findViewById(R.id.yearofpassing);
            idofview = itemView.findViewById(R.id.idofview);
            updateeducation = itemView.findViewById(R.id.updateeducation);
            obtainmark = itemView.findViewById(R.id.obtainmark);
            subjectmarks = itemView.findViewById(R.id.subjectmarks);
            streamid = itemView.findViewById(R.id.streamid);
            educationid = itemView.findViewById(R.id.educationid);
        }


    }
}
