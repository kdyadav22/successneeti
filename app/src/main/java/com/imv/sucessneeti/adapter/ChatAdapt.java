package com.imv.sucessneeti.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.imv.sucessneeti.Extra.Chatmodel;
import com.imv.sucessneeti.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ChatAdapt extends RecyclerView.Adapter<ChatAdapt.ViewHolder> {
    Context context;
    float finauu;
    String yut, uty, ssssi;
    List<Chatmodel> getDataAdapter;

    String subject;
    private static final String MYFILE = "MyFile";
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;

    public ChatAdapt(List<Chatmodel> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.designforchat, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();
        ssssi = sharedPreferenceswe.getString("subscription", null);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final Chatmodel getDataAdapter1 = getDataAdapter.get(position);


        String type;
        type = getDataAdapter1.getType();
        Log.d("meyia", type);

        Viewholder.adminshow.setVisibility(View.GONE);
        Viewholder.usershow.setVisibility(View.GONE);
        if (type.matches("2")) {
            if (getDataAdapter1.getName().matches("Admin")) {
                Viewholder.adminshow.setVisibility(View.VISIBLE);
                Viewholder.adminshow.setText(getDataAdapter1.getTitle() + "\n" + getDataAdapter1.getDescription());
                Log.d("alldata", getDataAdapter1.getTitle() + getDataAdapter1.getDescription());
            } else {
                Viewholder.adminshow.setVisibility(View.GONE);

            }
        } else if (type.matches("1")) {
            Viewholder.usershow.setVisibility(View.VISIBLE);
            Viewholder.usershow.setText(getDataAdapter1.getTitle() + "\n" + getDataAdapter1.getDescription());
        }
    }


    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titlename, adminshow, usershow;

        public ViewHolder(View itemView) {

            super(itemView);

            titlename = (TextView) itemView.findViewById(R.id.titlename);
            adminshow = (TextView) itemView.findViewById(R.id.adminshow);
            usershow = (TextView) itemView.findViewById(R.id.usershow);
        }


    }
}
