package com.imv.sucessneeti.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.imv.sucessneeti.Activity.photoupdate.PhotoUpdate;
import com.imv.sucessneeti.Model.MainPhoto;
import com.imv.sucessneeti.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
    Context context;

    List<MainPhoto> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    ImageView networkImageView;
    DownloadManager downloadManager;
    private long downloadid;
    String urlimagebb = "";
    String namebb = "";

    public PhotoAdapter(List<MainPhoto> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carddesignhome, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, final int position) {

        final MainPhoto getDataAdapter1 = getDataAdapter.get(position);

        final SharedPreferences sharedPreferences = context.getSharedPreferences("TEMPP", Context.MODE_PRIVATE);
        String profil = sharedPreferences.getString("profile", null);
//      Log.d("yyyyyyy",profil);
        String leftthu = sharedPreferences.getString("lefthum", null);
        String righttum = sharedPreferences.getString("rightumg", null);
        String stusign = sharedPreferences.getString("stusign", null);
        String fathers = sharedPreferences.getString("fathers", null);
        String mothers = sharedPreferences.getString("mothers", null);
        if (position == 0) {
//            Glide.with(context)
//                    .load(profil)
//                    .placeholder(R.drawable.upload_icon)
//                    .into(Viewholder.networkImageView);
            Picasso.get()
                    .load(profil)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        } else if (position == 1) {
            Picasso.get()
                    .load(leftthu)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        } else if (position == 2) {
            Picasso.get()
                    .load(righttum)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        } else if (position == 3) {
            Picasso.get()
                    .load(stusign)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        } else if (position == 4) {
            Picasso.get()
                    .load(fathers)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        } else if (position == 5) {
            Picasso.get()
                    .load(mothers)
                    .placeholder(R.drawable.upload_icon)
                    .into(Viewholder.networkImageView);
        }


        Viewholder.imagename.setText(getDataAdapter1.getName());
        Viewholder.idofimage.setText(getDataAdapter1.getId());
        Viewholder.uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhotoUpdate.class);
                SharedPreferences sharedPreferences1 = context.getSharedPreferences("CONTT", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences1.edit();
                editor.putString("DATA", getDataAdapter1.getId());
                editor.apply();
                context.startActivity(intent);
            }
        });
        Viewholder.downlaodimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedPreferences = context.getSharedPreferences("TEMPP", Context.MODE_PRIVATE);
                final String profil = sharedPreferences.getString("profile", null);
                Log.d("yyyyyyy", profil);
                String leftthu = sharedPreferences.getString("lefthum", null);
                String righttum = sharedPreferences.getString("rightumg", null);
                String stusign = sharedPreferences.getString("stusign", null);
                String fathers = sharedPreferences.getString("fathers", null);
                String mothers = sharedPreferences.getString("mothers", null);
                if (position == 0) {
                    // Uri  uri = Uri.parse("http://new.successneeti.com/UPLOADS/student_images/9/5e21d05843e0b.jpeg");
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = profil;
                        namebb = "My_Profile";
                    }


//                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(profil);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }

                } else if (position == 1) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = leftthu;
                        namebb = "Left Thumb";
                    }

//                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//
//                        // this will request for permission when user has not granted permission for the app
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                    }
//
//                    else{
//                        //Download Script
//
//                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(leftthu);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//                        request.setDestinationInExternalPublicDir("/Downloads/", Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }
                } else if (position == 2) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = righttum;
                        namebb = "Right Thumb";
                    }
//                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//
//                        // this will request for permission when user has not granted permission for the app
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                    }
//
//                    else{
//                        //Download Script
//
//                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(righttum);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//                        request.setDestinationInExternalPublicDir("/Downloads/", Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }
                } else if (position == 3) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = stusign;
                        namebb = "Student Signature";
                    }
//                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//
//                        // this will request for permission when user has not granted permission for the app
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                    }
//
//                    else{
//                        //Download Script
//
//                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(stusign);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//                        request.setDestinationInExternalPublicDir("/Downloads/", Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }
                } else if (position == 4) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = fathers;
                        namebb = "Father Signature";
                    }
//                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//
//                        // this will request for permission when user has not granted permission for the app
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                    }
//
//                    else{
//                        //Download Script
//
//                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(fathers);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//                        request.setDestinationInExternalPublicDir("/Downloads/", Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }
                } else if (position == 5) {
//                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//
//                        // this will request for permission when user has not granted permission for the app
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                    }
//
//                    else{
//                        //Download Script
//
//                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(mothers);
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setVisibleInDownloadsUi(true);
//                        request.setDestinationInExternalPublicDir("/Downloads/", Viewholder.imagename.toString()+".jpg");
//
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
//                        downloadManager.enqueue(request);
//                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        // this will request for permission when user has not granted permission for the app
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                        }
                    }
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "No Permission", Toast.LENGTH_SHORT).show();
                    } else {
                        MyAsyncTask asyncTask = new MyAsyncTask();
                        asyncTask.execute();
                        urlimagebb = mothers;
                        namebb = "Mother Signature";
                    }
                }

            }
        });


    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView imagename, idofimage;
        public ImageView networkImageView;
        public Button downlaodimage, uploadimage;

        public ViewHolder(View itemView) {
            super(itemView);

            imagename = itemView.findViewById(R.id.imagename);
            networkImageView = itemView.findViewById(R.id.networkimage);
            idofimage = itemView.findViewById(R.id.idofimage);
            downlaodimage = itemView.findViewById(R.id.downloadimage);
            uploadimage = itemView.findViewById(R.id.uploadimage);
        }
    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String tata = urlimagebb;
            Log.d("sdfs", urlimagebb);
            try {
                String folder_main = "Success Neeti";


                URL url = new URL(tata);
                //create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //set up some things on the connection
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                //and connect!
                urlConnection.connect();
                //set the path where we want to save the file in this case, going to save it on the root directory of the sd card.
                File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!f.exists()) {
                    f.mkdirs();
                }
                //  File SDCardRoot = Environment.getExternalStoragePublicDirectory("Download");
                //create a new file, specifying the path, and the filename which we want to save the file as.
                File file = new File(f, namebb + ".jpg");
                //this will be used to write the downloaded data into the file we created
                FileOutputStream fileOutput = new FileOutputStream(file);
                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();
                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer
                //now, read through the input buffer and write the contents to the file
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    //this is where you would do something to report the prgress, like this maybe
                    //updateProgress(downloadedSize, totalSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(context, "Image Successfully Downloaded", Toast.LENGTH_SHORT).show();
        }
    }
}
  /*  add label in last of photo
    upload profile photo
    photo size 50 kb upload
    pdf 100 kb
    add photo image show
    sucess neeti

    in persinal
    chest optional
    all mendateuy
    fee plan
    show ttype in all like mother and mother
    mother name when update and set text below of update button

    download in image
    upload pdf */
