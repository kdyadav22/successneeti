package com.imv.sucessneeti.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.imv.sucessneeti.Extra.Recmon_jobsm;
import com.imv.sucessneeti.R;
import com.imv.sucessneeti.paymentgaet.MyActivity;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Rec_jobsAda extends RecyclerView.Adapter<Rec_jobsAda.ViewHolder> {
    Context context;
    float finauu;
    String yut, uty, ssssi;
    List<Recmon_jobsm> getDataAdapter;

    ImageLoader imageLoader1;
    String subject;
    private static final String MYFILE = "MyFile";
    SharedPreferences sharedPreferenceswe;
    SharedPreferences.Editor editor;

    public Rec_jobsAda(List<Recmon_jobsm> getDataAdapter, Context context) {

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardesignjobrec, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, final int position) {

        final Recmon_jobsm getDataAdapter1 = getDataAdapter.get(position);


        Viewholder.titlename.setText(getDataAdapter1.getJob_title());
        Viewholder.jobstate.setText(getDataAdapter1.getJob_state());

        String ss = getDataAdapter1.getFee();

        Log.d("fy", "onBindViewHolder: " + finauu);
        yut = String.valueOf(ss);

        Log.d("fy", "onBindViewHolder: " + yut);
        Viewholder.jobfee.setText("\u20B9 " + getDataAdapter1.getFee());
        SharedPreferences sharedPreferencesmm = context.getSharedPreferences("TEMP", MODE_PRIVATE);
        String idva = sharedPreferencesmm.getString("subscription_fee", null);
        uty = idva;
        Viewholder.rupess.setText("\u20B9 " + idva);
        // Viewholder.experuser.setText(getCountOfDays(getDataAdapter1.getFirstday(),getDataAdapter1.getLasday()));
        Viewholder.lastdate.setText(getDataAdapter1.getLast_date());
        Viewholder.toalapp.setText(String.valueOf(getDataAdapter1.getApplied()));
        int ssbyid = getDataAdapter1.getApplied();
        if (ssbyid == 0) {


//        subject = getDataAdapter1.getId();

            sharedPreferenceswe = context.getSharedPreferences("TEMP", MODE_PRIVATE);
            editor = context.getSharedPreferences("TEMP", MODE_PRIVATE).edit();
            ssssi = sharedPreferenceswe.getString("subscription", null);
            if (ssssi.matches("pending")) {
                Viewholder.linearyiuapply.setVisibility(View.INVISIBLE);
            } else {
                Viewholder.linearyiu.setVisibility(View.INVISIBLE);
                Viewholder.linearyiuapply.setVisibility(View.VISIBLE);
            }
            Viewholder.linearyiu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MyActivity.class);
                    intent.putExtra("typedata", uty);
                    intent.putExtra("jobid", getDataAdapter.get(position).getId());
                    intent.putExtra("type", "1");
                    context.startActivity(intent);
                }
            });
            Viewholder.linearyiuapply.setOnClickListener(new View.OnClickListener() {
                                                             @Override
                                                             public void onClick(View v) {
                                                                 Intent intent = new Intent(context, MyActivity.class);
                                                                 intent.putExtra("typedata", getDataAdapter.get(position).getFee());
                                                                 intent.putExtra("jobid", getDataAdapter.get(position).getId());
                                                                 intent.putExtra("type", "2");
                                                                 context.startActivity(intent);
                                                             }
                                                         }
            );
            Viewholder.linearyiuapplyed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Already Applied", Toast.LENGTH_SHORT).show();
                }
            });
            if (ssssi.matches("pending")) {
                Viewholder.linearyiuapply.setVisibility(View.INVISIBLE);
            } else {

                Viewholder.linearyiuapplyed.setVisibility(View.INVISIBLE);
                Viewholder.linearyiu.setVisibility(View.INVISIBLE);
                Viewholder.linearyiuapply.setVisibility(View.VISIBLE);
            }
        } else {

            Viewholder.linearyiu.setVisibility(View.INVISIBLE);
            Viewholder.linearyiuapply.setVisibility(View.INVISIBLE);
            Viewholder.linearyiuapplyed.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titlename, jobstate, jobfee;
        public TextView lastdate, idofview, toalapp, rupess;
        LinearLayout linearyiu, linearyiuapply, linearyiuapplyed;

        public ViewHolder(View itemView) {

            super(itemView);

            titlename = (TextView) itemView.findViewById(R.id.titlename);
            jobstate = (TextView) itemView.findViewById(R.id.jobstate);
            jobfee = (TextView) itemView.findViewById(R.id.jobfee);
            lastdate = (TextView) itemView.findViewById(R.id.lastdate);
            linearyiu = (LinearLayout) itemView.findViewById(R.id.linearyiu);
            toalapp = (TextView) itemView.findViewById(R.id.toalapp);
            rupess = (TextView) itemView.findViewById(R.id.rupess);
            linearyiuapply = (LinearLayout) itemView.findViewById(R.id.linearyiuapply);
            linearyiuapplyed = (LinearLayout) itemView.findViewById(R.id.linearyiuapplyed);
        }


    }
}
