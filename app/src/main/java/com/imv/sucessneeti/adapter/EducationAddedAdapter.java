package com.imv.sucessneeti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imv.sucessneeti.Model.EducationModel;
import com.imv.sucessneeti.R;

import java.util.List;

public class EducationAddedAdapter extends RecyclerView.Adapter<EducationAddedAdapter.ViewHolder> {
    Context context;
    private List<EducationModel> educationModelList;
    private EducationClickCallback educationClickCallback;

    public EducationAddedAdapter(List<EducationModel> educationModelList, Context context, EducationClickCallback educationClickCallback) {
        super();
        this.educationModelList = educationModelList;
        this.context = context;
        this.educationClickCallback = educationClickCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carddesigneducation, parent, false);
        return new ViewHolder(v);
    }

    /*private void getDeleteData() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.show();
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject obj = new JSONObject();
        try {
            // String deviceAppUID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            String aiid = sharedPreferenceswe.getString("id", null);
            JSONObject obj1 = new JSONObject();
            obj1.put("id", subject);
            obj1.put("user_id", aiid);


            obj.put("transaction_data", obj1);
            Log.d("asdfjbs,bdf", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = obj.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://new.successneeti.com/api/user/deleteeducation", new JSONObject(requestBody), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("response", "" + response);
                try {
                    String ss = response.getString("host_code");
                    if (ss.matches("0")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("valueb", "4");
                        context.startActivity(intent);
                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Log.d("VolleyError", "Volley" + error);
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    Log.e("Status code", String.valueOf(networkResponse.data));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String apiuyt = sharedPreferenceswe.getString("deviceid", null);
                headers.put("Token", apiuyt);

                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);

    }*/

    @Override
    public void onBindViewHolder(final ViewHolder viewholder, final int position) {
        final EducationModel educationModel = educationModelList.get(position);

        viewholder.education_name.setText(educationModel.getEducation_name());
        viewholder.streamuser.setText(educationModel.getStream_name());
        viewholder.boardexamuser.setText(educationModel.getBoard_name());
        viewholder.percentageuse.setText(educationModel.getPerctengae_name());
        viewholder.yearofpassing.setText(educationModel.getYearofpassding());
        viewholder.idofview.setText(educationModel.getId());
        viewholder.subjectmarks.setText(educationModel.getSujbjectmark());
        viewholder.obtainmark.setText(educationModel.getObtainmar());
        viewholder.educationid.setText(educationModel.getEducationid());
        viewholder.streamid.setText(educationModel.getStreamid());

        viewholder.updateeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                educationClickCallback.onEditClick(educationModel, position);
            }
        });

        viewholder.imagedeletedey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                educationClickCallback.onDeleteClick(educationModel, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return educationModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView education_name, streamuser, boardexamuser;
        TextView percentageuse, yearofpassing, idofview, obtainmark, subjectmarks, streamid, educationid;
        ImageView imagedeletedey, updateeducation;

        public ViewHolder(View itemView) {
            super(itemView);

            education_name = itemView.findViewById(R.id.education_name);
            streamuser = itemView.findViewById(R.id.streamuser);
            imagedeletedey = itemView.findViewById(R.id.imagedeletedey);
            boardexamuser = itemView.findViewById(R.id.boardexamuser);
            percentageuse = itemView.findViewById(R.id.percentageuse);
            yearofpassing = itemView.findViewById(R.id.yearofpassing);
            idofview = itemView.findViewById(R.id.idofview);
            updateeducation = itemView.findViewById(R.id.updateeducation);
            obtainmark = itemView.findViewById(R.id.obtainmark);
            subjectmarks = itemView.findViewById(R.id.subjectmarks);
            streamid = itemView.findViewById(R.id.streamid);
            educationid = itemView.findViewById(R.id.educationid);
        }

    }

    public interface EducationClickCallback {
        void onDeleteClick(EducationModel educationModel, int pos);

        void onEditClick(EducationModel educationModel, int pos);
    }
}
